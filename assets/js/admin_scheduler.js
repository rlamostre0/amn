
$(document).ready(function() {
var smallCalendar=$('#small-calendar').fullCalendar({
	header: {
		left: 'title',
		center: '',
		right: 'prev,next'
	},
	aspectRatio: 1,
	height: "auto",
	defaultView: 'month',
	selectable: true,
	selectHelper: true,
	select: function(start, end, allDay) {
		selectedDate=start.format("YYYY-MM-DD");
		$('.submitForm').submit();
		$.ajax(
			{
				type: "POST",
				url: "/admin/schedule_client/",
				data: {
					selected_date: selectedDate,
					therapist_id: window.therapist_id,
				},
		  	error : function(XMLHttpRequest, textStatus, errorThrown) {
			
			}
		})
		.done(function( result ) {

			res=jQuery.parseJSON(result);
			$('.tableData').html('');
			html='';
			$.each(res, function( index, value ) {
				s_str = value.start.split(" ");
				e_str = value.end.split(" ");

				html+='<tr>';
				html+='	<td>'+s_str[1].substr(0, 5)+' - '+e_str[1].substr(0, 5)+'</td>';
				html+='	<td>'+value.name+'</td>';
				html+='	<td>';
				html+='<a href="/admin/view_intake_form/'+value.intake_form_id+'" class="btn btn-warning">view intake form</a>';
				html+=' <a href="#" class="btn btn-info">change schedule</a>';
				html+=' <a href="#myModal" role="button" class="btn btn-danger btnClientCancel" data-toggle="modal" data-clientStart="'+value.start+'" data-clientEnd="'+value.end+'" data-intakeId="'+value.intake_form_id+'" data-clientName="'+value.name+'" data-clientSched="'+window.the_date+', '+s_str[1].substr(0, 5)+' - '+e_str[1].substr(0, 5)+'">cancel</a>';
				html+='	</td>';
				html+='</tr>';
			});

			if (res.length==0) {
				html+='<tr>';
				html+='<td colspan="3"><p>There are currently no reservations for this date.</p></td>';
				html+='</tr>';
			}
			console.log(html);
			$('.tableData').html(html);

		});
		}
// 	editable: false,

	// aspectRatio: 1,
	// height: "auto",
	// defaultView: 'month',
// 	snapDuration:"01:00",
// 		type:'POST',
// 		data: function() { // a function that returns an object
// 				franchiseNo: clinic['franchiseno'],
// 				therapist_id:therapist['id'],
// 			};
		// data: function() { // a function that returns an object
		// 	return {
		// 		franchiseNo: window.clinic['franchiseno'],
		// 		therapist_id: window.therapists['id'],
		// 		start: start.format(),
		// 		end: end.format(),
		// 	};
		// }
	});

});