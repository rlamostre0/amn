
function searchMaps(searchType, zipCode, radius, city, state, mapId, mapResultId, mapStatusId) {
    var serviceFilter = new Array(8);
    if (!serviceFilter[0] && !serviceFilter[1] && !serviceFilter[2] && !serviceFilter[3]
            && !serviceFilter[4] && !serviceFilter[5] && !serviceFilter[6] && !serviceFilter[7]) {
        serviceFilter = null;
    }
    if (searchType === 1) {
        $.ajax({
            url: 'http://www.mapquestapi.com/search/v2/radius',
            dataType: 'jsonp', crossDomain: true,
            data: {
                key: decodeURIComponent(window.mapquestkey),
                inFormat: "json",
                json: JSON.stringify({
                    origin: zipCode,
                    hostedDataList: [
                        {
                            tableName: "mqap.142301_locations",
                            extraCriteria: "zip_code ILIKE ?",
                            parameters: [zipCode]
                        }
                    ]
                })
            },
            success: function (data) {
                window.mapResultData = data;
                if (typeof data.searchResults !== 'undefined') {
                    showMaps(data, serviceFilter, mapId, mapResultId, mapStatusId);
                } else {
                    $('#' + mapId).html("");
                    $('#' + mapResultId).html("");
                    $('#' + mapStatusId).text("No location found");
                    $('#' + mapId + "-border").hide();
                }
            }
        });
    } else if (searchType === 2) {
        $.ajax({
            url: 'http://www.mapquestapi.com/search/v2/radius',
            dataType: 'jsonp', crossDomain: true,
            data: {
                key: decodeURIComponent(window.mapquestkey),
                inFormat: "json",
                json: JSON.stringify({
                    origin: "Atlanta, GA",
                    hostedDataList: [{tableName: "mqap.142301_locations"}],
                    options: {radius: radius}
                })
            },
            success: function (data) {
                window.mapResultData = data;
                if (typeof data.searchResults !== 'undefined') {
                    showMaps(data, serviceFilter, mapId, mapResultId, mapStatusId);
                } else {
                    $('#' + mapId).html("");
                    $('#' + mapResultId).html("");
                    $('#' + mapStatusId).text("No location found");
                    $('#' + mapId + "-border").hide();
                }
            }
        });
    } else if (searchType === 3) {
        $.ajax({
            url: 'http://www.mapquestapi.com/search/v2/radius',
            dataType: 'jsonp', crossDomain: true,
            data: {
                key: decodeURIComponent(window.mapquestkey),
                inFormat: "json",
                json: JSON.stringify({
                    origin: city + ", " + state + ", US",
                    hostedDataList: [
                        {
                            tableName: "mqap.142301_locations",
                            extraCriteria: "city ILIKE ? and state ILIKE ?",
                            parameters: [city, state]
                        }
                    ]
                })
            },
            success: function (data) {
                window.mapResultData = data;
                if (typeof data.searchResults !== 'undefined') {
                    showMaps(data, serviceFilter, mapId, mapResultId, mapStatusId);
                } else {
                    $('#' + mapId).html("");
                    $('#' + mapResultId).html("");
                    $('#' + mapStatusId).text("No location found");
                    $('#' + mapId + "-border").hide();
                }
            }
        });
    }
}

function searchMapsByCity(city, state, mapId, mapResultId, mapStatusId) {
    var serviceFilter = new Array(8);
    if (!serviceFilter[0] && !serviceFilter[1] && !serviceFilter[2] && !serviceFilter[3]
            && !serviceFilter[4] && !serviceFilter[5] && !serviceFilter[6] && !serviceFilter[7]) {
        serviceFilter = null;
    }
    $.ajax({
        url: 'http://www.mapquestapi.com/search/v2/radius',
        dataType: 'jsonp', crossDomain: true,
        data: {
            key: decodeURIComponent(window.mapquestkey),
            inFormat: "json",
            json: JSON.stringify({
                origin: city + ", " + state + ", US",
                hostedDataList: [
                    {
                        tableName: "mqap.142301_locations",
                        extraCriteria: "city ILIKE ? and state ILIKE ?",
                        parameters: [city, state]
                    }
                ]
            })
        },
        success: function (data) {
            window.mapResultData = data;
            if (typeof data.searchResults !== 'undefined') {
                showMaps(data, serviceFilter, mapId, mapResultId, mapStatusId);
            } else {
                $('#' + mapId).html("");
                $('#' + mapResultId).html("");
                $('#' + mapStatusId).text("No location found");
                $('#' + mapId + "-border").hide();
            }
        }
    });
}

function showMaps(data, filter, mapId, mapResultId, mapStatusId) {
    var pois = new MQA.ShapeCollection();
    var html = "";
    var locationCount = 0;

    $('#' + mapId).html("");
    $('#' + mapResultId).html("");
    for (var i = 0; i < data.searchResults.length; i++) {
        var mapData = data.searchResults[i];
        var isValid = true;
        if (filter !== null) {
            var c = 0;
            for (var m = 0; m < filter.length; m++) {
                if (filter[m]) {
                    ++c;
                }
            }
            if (c > 0) {
                isValid = ((mapData.fields.is_medical_massage && filter[0])
                        || (mapData.fields.is_spa_massage && filter[1])
                        || (mapData.fields.is_fascia_manipulation && filter[2])
                        || (mapData.fields.is_trigger_point_therapy && filter[3])
                        || (mapData.fields.is_lymphatic_system && filter[4])
                        || (mapData.fields.is_energy_work && filter[5])
                        || (mapData.fields.is_Insurance && filter[6])
                        || (mapData.fields.is_couples_massage && filter[7]));
            } else {
                isValid = true;
            }
        }
        if (isValid) {
            var location = new MQA.Poi({lat: mapData.shapePoints[0], lng: mapData.shapePoints[1]});
            var numberedPoi = new MQA.Icon('http://www.mapquestapi.com/staticmap/geticon?uri=poi-' + (i + 1) + '.png', 20, 29);
            location.setIcon(numberedPoi);
            location.setRolloverContent('<span class="poi-title">' + mapData.name + '</span>');
            location.setInfoContentHTML('<div class="poi-body"><div class="poi-title">' +
                    mapData.name + '</div>' + mapData.fields.street + "</br>" + mapData.fields.city + ", " + mapData.fields.state + ", " + mapData.fields.zip_code + '<br /></div>');
            pois.add(location);

            var h = '<div class="columns map-info"><div class="small-6 columns">'
                    + '<h3><b>' + mapData.name + '</b><br/></h3>';
            if (mapData.fields.location !== null && mapData.fields.location !== "") {
                h = h + '<b>' + mapData.fields.location + '</b><br/>';
            }
            h = h + 'Franchise #.: ' + (mapData.fields.franchise_no !== null ? mapData.fields.franchise_no : "") + '<br/>';
            h = h + 'License #.: ' + (mapData.fields.license_no !== null ? mapData.fields.license_no : "") + '<br/><br/>';

            if (mapData.fields.therapist_name !== null && mapData.fields.therapist_name !== "") {
                h = h + '<b>' + mapData.fields.therapist_name + '</b><br/>';
            }
            h = h + mapData.fields.street + "</br>" + mapData.fields.city + ", " + mapData.fields.state + " " + mapData.fields.zip_code
                    + '<br/>';
            if (mapData.fields.phone_no !== null && mapData.fields.phone_no !== "") {
                h = h + mapData.fields.phone_no + '<br/>';
            }
            if (mapData.fields.website_url !== null && mapData.fields.website_url !== "") {
                h = h + 'Website: <a href="' + mapData.fields.website_url + '" target="_blank">' + mapData.fields.website_url + '</a><br/>';
            }
            h = h + '</div><div class="small-6 columns"><img class="right" src="/assets/img/franchise/main-img.png"></div>';
            h = h + '<div class="small-12 columns"><b><br/>Modalities This Location Practices<br/></b>';
            if (mapData.fields.is_medical_massage) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-red" title="Medical Massage/Remedial Massage"/></a>';
            }
            if (mapData.fields.is_spa_massage) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-green" title="Spa/Swedish/Deep Tissue"/></a>';
            }
            if (mapData.fields.is_fascia_manipulation) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-orange" title="Fascial Manipulation/Myofascial Release"/></a>';
            }
            if (mapData.fields.is_trigger_point_therapy) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-blue" title="Trigger Point Therapy/Neuromuscular Therapy"/></a>';
            }
            if (mapData.fields.is_lymphatic_system) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-yellow" title="Lympathic Massage/Lympathic Drainage"/></a>';
            }
            if (mapData.fields.is_energy_work) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-darkblue" title="Energy Work/Reiki"/></a>';
            }
            if (mapData.fields.is_couples_massage) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-violet" title="Eastern Medicine"/></a>';
            }
            if (mapData.fields.is_Insurance) {
                h = h + '<a href="#" class="service-offered"><img class="modal-btn-location mi-black" title="Insurance Approved Provider"/></a>';
            }

            h = h + '<br/><div class="the_btn"><a href="#" >Reviews<img style="margin-left:3px" src="/assets/img/star.png" /></a></div>' + '<div class="the_btn"><a href="#">Schedule Now<img width="20px" style="position:absolute;margin-top:-3px;margin-left:3px" src="/assets/img/book-gn.png" /></a></div>' + '<div class="the_btn"><a href="/location/indiv/' + mapData.fields.sql_id + '">View this Location<img width="15px" style="position:absolute;margin-top:-3px;margin-left:3px" src="/assets/img/userid.png" /></a></div>'
                    // + '<a href="'+window.site_url+'book/'+mapData.fields.franchise_no+'" class="map-schedulenow" data-clinic-id="'+mapData.fields.franchise_no+'"></a>'
										'<a href="'+window.site_url+'book/'+mapData.fields.franchise_no+'" class="map-schedulenow" data-clinic-id="'+mapData.fields.franchise_no+'"></a>'
                    + '<a href="' + window.site_url + 'franchise?lat=' + mapData.fields.mqap_geography.latLng.lat
                    + '&lng=' + mapData.fields.mqap_geography.latLng.lng + '" class="map-viewlocation"></a>' + '<a href="#"></a>';

            h = h + '<br/><br/><br/>Location Description:<br/>' + (mapData.fields.location_description !== null ? mapData.fields.location_description : "") + '<br/><br/>';

            h = h + 'Distance From You: <a href="http://www.mapquest.com/?saddr=&amp;daddr='
                    + mapData.fields.street + ", " + mapData.fields.city + ", " + mapData.fields.state + " " + mapData.fields.zip_code
                    + '" class="map-direction"></a><br/><br/>';
            
            h = h + '<a href="#" class="map-twitter"></a>';
            h = h + '<a href="#" class="map-google"></a>&nbsp;&nbsp;&nbsp;';
            h = h + '<a href="'+window.site_url+'schedule/'+mapData.fields.franchise_no+'">Book a Session</a>&nbsp;&nbsp;';
            h = h + '<a href="#">Affiliate Blogger</a><br/>';
            h = h + '</div></div>';

            html = html + h;
            locationCount = locationCount + 1;
        }
    }
    if (html !== "") {
        $('#' + mapResultId).html(html);
        window.map = new MQA.TileMap({
            elt: document.getElementById(mapId),
            zoom: 10,
            collection: pois,
            bestFitMargin: 0
        });
        MQA.withModule('smallzoom', 'mousewheel', function () {
            map.addControl(new MQA.SmallZoom());
            map.enableMouseWheelZoom();
        });
    }
    if (locationCount > 0) {
        $('#' + mapId + "-border").show();
        $('#' + mapStatusId).text(locationCount + " location" + (locationCount > 1 ? "s" : "") + " found");
    } else {
        $('#' + mapId + "-border").hide();
        $('#' + mapStatusId).text("No location found");
    }
}
