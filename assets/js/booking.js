// JavaScript Document

$(function() {

    $('#set-pain').click(function(e) {
        html = $('#pain-selector-wrap').html();
        id = 'id-key-' + parseInt(Math.random() * (999 - 1) + 1);
        html = '<div class="pain-selector" id=' + id + '><input type="hidden" class="pain-selector-left" name="pain_selector[' + id + '][left]" ><input type="hidden" class="pain-selector-top" name="pain_selector[' + id + '][top]" ></div>';
        $('#anatomical-figure').append(html);
        $('#anatomical-figure .pain-selector#' + id).draggable({cursor: 'move', container: '#anatomical-figure',
            stop: function() {
                var cont = $('#anatomical-figure').offset();
                var img = $(this).offset();
                // $('#drg1').text('x-axis :' + (img.left - cont.left) + ', y-axis :' + (img.top - cont.top));	
                $($(this).find('.pain-selector-left')).val(img.left - cont.left);
                $($(this).find('.pain-selector-top')).val(img.left - cont.top);
            }
        });
    });
});