function pointInPolygon(poly, x, y, cornerx, cornery) {
    var i;
    var j = poly.length - 1;
    var oddNodes = false;

    for (var i = 0; i < poly.length; i++) {
        var m = poly[i][1] + cornery;
        var n = poly[j][1] + cornery;
        var o = poly[i][0] + cornerx;
        var p = poly[j][0] + cornerx;
        if ((m < y && n >= y || n < y && m >= y)
                && (o <= x || p <= x)) {
            oddNodes ^= (o + (y - m) / (n - m) * (p - o) < x);
        }
        j = i;
    }
    return oddNodes;
}