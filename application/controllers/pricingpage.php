<?php

class Pricingpage extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('testimonials_model');
	}
	
	function index(){

		$data_seo = array(
			'meta_title' => 'Pricing Page',
			'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
			'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
			'is_hide_header_slide' => true
			);


		$this->load->view('templates/header', $data_seo);
		$this->load->view('pricingpage/index');
		$this->load->view('templates/footer');
	}

	function about_us(){
		$this->load->view('pricingpage/about_us');
	}

	function membership(){
		$this->load->view('pricingpage/membership');
	}

	function discounts(){
		$this->load->view('pricingpage/discounts');
	}

	function health_insurance(){
		$this->load->view('pricingpage/health_insurance');
	}

	function pricing(){
		$this->load->view('pricingpage/pricing');
	}	

	function vip_point_system(){
		$this->load->view('pricingpage/vip_point_system');
	}

	function your_first_visit(){
		$this->load->view('pricingpage/your_first_visit');
	}

	function chair_massage(){
		$this->load->view('pricingpage/chair_massage');
	}

	function become_a_chair_massage_affiliate(){
		$this->load->view('pricingpage/become_a_chair_massage_affiliate');
	}

	function problems_and_solutions(){
		$this->load->view('pricingpage/problems_and_solutions');
	}

	function faqs(){
		$this->load->view('faq');
	}

	function health_care_practitioners(){
		$this->load->view('pricingpage/health_care_practitioners');
	}

	function therapist(){
		$this->load->view('pricingpage/therapist');
	}

	function terms_of_use(){
		$this->load->view('pricingpage/terms_of_use');
	}

	function privacy_policy(){
		$this->load->view('pricingpage/privacy_policy');
	}

	function modalities(){
		$this->load->view('modalities');
	}

	function testimonial(){
		$data_seo = array(
            'meta_title' => 'Testimonial',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $data = array();
        $data["recordlist"] = $this->testimonials_model->get_all_testimonials();

        $testimonials = Array();
        $reviews = $this->testimonials_model->get_all_testimonials();
        foreach ($reviews as $p) {
            $comments = $this->testimonials_model->get_all_comments($p["id"]);
            array_push($testimonials, Array($p, $comments));
        }
        $data["recordlist"] = $testimonials;
        $data["mode"] = "";
		$this->load->view('testimonial', $data);
	}




	// EXTERNAL LINK HANDLERS
	// The functions below are melerey containers for the index function, this was made to compromise the MVC 

	function click_about_us(){
		$this->index();
	}

	function click_membership(){
		$this->index();
	}

	function click_discounts(){
		$this->index();
	}

	function click_health_insurance(){
		$this->index();
	}

	function click_vip_point_system(){
		$this->index();
	}

	function click_your_first_visit(){
		$this->index();
	}

	function click_chair_massage(){
		$this->index();
	}

	function click_become_a_chair_massage_affiliate(){
		$this->index();
	}

	function click_problems_and_solutions(){
		$this->index();
	}

	function click_faqs(){
		$this->index();
	}

	function click_health_care_practitioners(){
		$this->index();
	}

	function click_therapist(){
		$this->index();
	}

	function click_terms_of_use(){
		$this->index();
	}

	function click_privacy_policy(){
		$this->index();
	}

	function click_pricing(){
		$this->index();
	}

}

?>