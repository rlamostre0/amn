<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blog extends CI_Controller {

    private $view_path = "admin/blog/";

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('admin/login');
        }
        $this->load->model('blogs_model');
    }

    public function index($page = 0) {
        $per_page = 10;
        $blog_count = count($this->blogs_model->get_blogs());
        $data['blogs'] = $this->blogs_model->get_limited_blogs($page, $per_page);

        $config['full_tag_open'] = '<div class="pagination"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="disabled"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&lt;';
        $config['next_link'] = '&gt;';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['base_url'] = site_url('admin/blog/index');
        $config['uri_segment'] = 4;
        $config['total_rows'] = $blog_count;
        $config['per_page'] = $per_page; 

        $this->pagination->initialize($config);

        $data['main_content'] = 'admin/blog/list';
        $this->load->view('includes/template', $data);
    }

    public function add() {
        $this->form_validation->set_error_delimiters('<li>', '</li>');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run()) {
          $this->blogs_model->store_blog(array(
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
          ));
          $this->session->set_flashdata('message', 'Blog Saved');
          redirect('/admin/blog');
        }

        $this->load->view('includes/template', array('main_content' => '/admin/blog/add'));
    }

}