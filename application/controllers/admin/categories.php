<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Categories extends CI_Controller {

    private $view_path = "admin/categories/";

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in')) {
            redirect('admin/login');
        }
        $this->load->model('category_model');
    }

    public function index($page = 0) {
        $this->output->enable_profiler(TRUE);
        $per_page = 10;
        $this->load->library('pagination');
        $category_count = count($this->category_model->get_all_categories());
        $data['categories'] = $this->category_model->get_limited_categories($page, $per_page);

        $config['full_tag_open'] = '<div class="pagination"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="disabled"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&lt;';
        $config['next_link'] = '&gt;';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['base_url'] = site_url('admin/categories/index');
        $config['uri_segment'] = 4;
        $config['total_rows'] = $category_count;
        $config['per_page'] = $per_page; 

        $this->pagination->initialize($config);

        $data['search_string_selected'] = '';
        $data['order'] = 'id';
        $data['main_content'] = 'admin/categories/list';
        $this->load->view('includes/template', $data);
    }

    public function add() {
        $this->form_validation->set_error_delimiters('<li>', '</li>');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('slug', 'Slug', 'required');

        if ($this->form_validation->run()) {
          $this->category_model->store_category(array(
            'name' => $this->input->post('name'),
            'slug' => $this->input->post('slug'),
          ));
          $this->session->set_flashdata('message', 'Category Saved');
          redirect('/admin/categories');
        }

        $this->load->view('includes/template', array('main_content' => '/admin/categories/add'));
    }

    public function update($id) {
        $this->form_validation->set_error_delimiters('<li>', '</li>');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('slug', 'Slug', 'required');

        if ($this->form_validation->run()) {
          $this->category_model->store_category(array(
            'name' => $this->input->post('name'),
            'slug' => $this->input->post('slug'),
          ));
          $this->session->set_flashdata('message', 'Category Saved');
          redirect('/admin/categories');
        }

        $this->load->view('includes/template', array('main_content' => '/admin/categories/add'));
    }

}