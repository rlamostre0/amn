<?php

class Search extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('clinics_model');
	}

	public function index(){
		$data_seo = array(
			'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
		);
		$this->load->view('templates/header',$data_seo);
		$this->load->view('templates/footer');
		
	}

	public function clinic(){
		$data_seo = array(
			'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
		);

		//$data['search_state'] = $this->clinics_model->get_clinic_by_state('CO');
		//echo $state;
		$state = $this->uri->segment(3);
		$s = $this->clinics_model->get_clinic_by_state($state);

		$arr = array(
				'state' => $state,
				's'	=> $s
				);
		
		$this->load->view('templates/header',$data_seo);
		$this->load->view('search',$arr);

/*
		if($state!=''){
			$s = $this->clinics_model->get_clinic_by_state($state);
			foreach($s as $p){
			echo $p['name']."<br/>";
		}
		}*/
		$this->load->view('templates/footer');
	}

}

?>