<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Testimonial extends CI_Controller {

    private $view_path = "";

    public function __construct() {
        parent::__construct();
        $this->load->model('testimonials_model');
    }

    public function index() {
        $data_seo = array(
            'meta_title' => 'Testimonial',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $data = array();
        $data["recordlist"] = $this->testimonials_model->get_all_testimonials();

        $testimonials = Array();
        $reviews = $this->testimonials_model->get_all_testimonials();
        foreach ($reviews as $p) {
            $comments = $this->testimonials_model->get_all_comments($p["id"]);
            array_push($testimonials, Array($p, $comments));
        }
        $data["recordlist"] = $testimonials;
        $data["mode"] = "";
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'testimonial', $data);
        $this->load->view('templates/footer');
    }

    public function reply() {
        $issuccess = false;
        $data = array();
        $testimonialId = 0;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            if (!($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0))) {
                $this->form_validation->set_rules('name', 'Name', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
            }

            $this->form_validation->set_rules('content', 'Content', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
                $data = $this->input->post(NULL, TRUE);
                $data["datetime"] = date("Y-m-d H:i:s");
                if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) {
                    $data["customerid"] = $this->session->userdata('currentcustomerid');
                }
                if ($this->testimonials_model->store_testimonial($data)) {
                    $issuccess = true;
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
            } else {
                $data['replyerrors'] = validation_errors();
                $testimonialId = intval($this->input->post("parentid"));
            }
        } else if ($this->input->server('REQUEST_METHOD') === 'GET') {
            $testimonialId = intval($this->input->get("id"));
        }
        $data["mode"] = "reply";
        if ($issuccess) {
            redirect("testimonial");
        } else {
            $data_seo = array(
                'meta_title' => 'Testimonial | Reply',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );            
            if ($testimonialId > 0) {
                $data["reviewId"] = $testimonialId;
                $reviews = $this->testimonials_model->get_testimonial_by_id($testimonialId);
                if (!empty($reviews)) {
                    $data["review"] = $reviews[0];
                }
            }
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'testimonial', $data);
            $this->load->view('templates/footer');
        }
    }

    public function add() {
        $issuccess = false;
        $data = array();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('content', 'Content', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
                $data = $this->input->post(NULL, TRUE);
                if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) {
                    $data["customerid"] = $this->session->userdata('currentcustomerid');
                }
                $data["datetime"] = date("Y-m-d H:i:s");
                if ($this->testimonials_model->store_testimonial($data)) {
                    $issuccess = true;
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
            } else {
                $data['adderrors'] = validation_errors();
            }
        }
        $data["mode"] = "add";
        if ($issuccess) {
            redirect("testimonial");
        } else {
            $data_seo = array(
                'meta_title' => 'Testimonial | Create Review',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );            
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'testimonial', $data);
            $this->load->view('templates/footer');
        }
    }

}
