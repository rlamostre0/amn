<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name> 
     * @see http://codeigniter.com/user_guide/general/urls.html 
     */
    private $view_path = "";

    public function index() {
        /*
          $data_seo = array(
          'meta_title' => 'Find Clinics',
          'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
          'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
          'is_hide_header_slide' => false
          );
          $this->load->view('templates/header', $data_seo);
          $this->load->view($this->view_path.'home');
          $this->load->view('templates/footer'); */
        $this->home();
    }

    public function home() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'home');
        $this->load->view('templates/footer');
    }

    public function pricing() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.pricing{display:block;background:#272727 !important};.pricing.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );

        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'pricing');
        $this->load->view('templates/footer');
    }

    public function gift() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.gift-certificate{display:block;background:#272727 !important};.gift-certificate.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );

        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'gift');
        $this->load->view('templates/footer');
    }

    public function locations() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->model('Clinics_model');
        $data['services'] = $this->Clinics_model->get_services();

        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $zip = $this->input->post('zip_code');
            $miles = $this->input->post('miles');
            $services = $this->input->post('services');

            $results = $this->Clinics_model->search_clinic_via_location($zip, $miles, $services);
            $data['results'] = $results;
            $data['show_results'] = TRUE;
        }
        $this->load->view($this->view_path . 'locations', $data);
        $this->load->view('templates/footer');
    }

    public function location_specifics($clinic_id, $slug) {

        $this->load->model('Clinics_model');
        $results = $this->Clinics_model->get_clinic_by_id($clinic_id);
        $clinic = $results[0];
        $data_seo = array(
            'meta_title' => $clinic['name'],
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $data['clinic'] = $clinic;
        $this->load->view($this->view_path . 'location_specifics', $data);
        $this->load->view('templates/footer');
    }

    public function modalities() {
        $data_seo = array(
            'meta_title' => 'Modalities',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.modalities{display:block;background:#272727 !important};.modalities.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'modalities');
        $this->load->view('templates/footer');
    }

    public function diagnosis() {
        $data_seo = array(
            'meta_title' => 'Diagnosis',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.modalities{display:block;background:#272727 !important};.modalities.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'diagnosis');
        $this->load->view('templates/footer');
    }

    public function faq() {
        $data_seo = array(
            'meta_title' => 'Frequently Asked Questions ',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.book{display:block;background:#272727 !important};.book.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'faq');
        $this->load->view('templates/footer');
    }

    public function book() {
        $data_seo = array(
            'meta_title' => 'Book',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.modalities{display:block;background:#272727 !important};.book.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'book');
        $this->load->view('templates/footer');
    }

    public function underconstruction() {
        $data_seo = array(
            'meta_title' => 'Book',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.modalities{display:block;background:#272727 !important};.book.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'underconstruction');
        $this->load->view('templates/footer');
    }
        public function redirect() {
            $redirect=$this->session->userdata('redirect_url');
            $hasRedirect=($redirect==false?$redirect:true);
            if($hasRedirect)
                redirect($redirect, 'refresh');
            else
                redirect(site_url(), 'refresh');
        }
    public function sitemap() {
        $data_seo = array(
            'meta_title' => 'Sitemap',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.modalities{display:block;background:#272727 !important};.book.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view('pages/sitemap');
        $this->load->view('templates/footer2');
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */