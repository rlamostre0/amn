<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Franchise extends CI_Controller {

    private $view_path = "";

    public function __construct() {
        parent::__construct();
        $this->load->model('clinics_model');
    }

    public function index() {
        $data_seo = array(
            'meta_title' => 'Individual Franchise',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );        
        $getData = $this->input->get(NULL, TRUE);
        $data_seo["latitude"] = $getData["lat"];
        $data_seo["longitude"] = $getData["lng"];
        
        $this->load->view('franchise/home', $data_seo);
    }    
}
