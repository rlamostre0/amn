<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customer extends CI_Controller {

    private $view_path = "customer/";

    public function __construct() {
        parent::__construct();
        $this->load->model('customers_model');
    }

    public function signup() {
        $issuccess = false;
        $data = null;
				$this->session->unset_userdata('redirect_url_signup');
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('lastname', 'Last Name', 'required');
            $this->form_validation->set_rules('firstname', 'First Name', 'required');
            $this->form_validation->set_rules('contact_home', 'Home Contact Number', 'required|numeric');
            $this->form_validation->set_rules('retypepassword', 'Re-type Password', 'required');
            $this->form_validation->set_rules('address_zip', 'Zip Address', 'required');
            //$this->form_validation->set_rules('middlename', 'Middle Name', 'required');
            $this->form_validation->set_rules('birth_date', 'Birth Date', 'required|date');
            $this->form_validation->set_rules('children_count', 'No. of Children', 'numeric');
            $this->form_validation->set_rules('height', 'Height', 'numeric');
            $this->form_validation->set_rules('weight', 'Weight', 'numeric');
            $this->form_validation->set_rules('loginid', 'Login ID', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error" style="color: red; font-weight: normal;"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
							$data = $this->input->post(NULL, TRUE);
							$isValid = true;
							if ($data["password"] != $data["retypepassword"]) {
								$this->form_validation->add_message("password", "Password NOT match");
								$isValid = false;
							}
							if ($isValid) {
								$data["password"] = $this->bcrypt->hash($data["password"]);
								unset($data["retypepassword"]);
								$data["logincount"] = 0;
								$userid=$this->session->userdata('currentcustomerid');
								$isLoggedIn=($userid==false?$userid:true);
								if($isLoggedIn){
									$data['isGuestAccount']=1;
									$data['parentAccount']=$userid;
								}
								if ($this->customers_model->store_customer($data)) {
									$issuccess = true;
									$data['flash_message'] = TRUE;
								} else {
									$data['flash_message'] = FALSE;
								}
							}
            } else {
                $data['signuperrors'] = validation_errors();
            }
        }
        if ($issuccess) {
					$redirect=$this->session->userdata('redirect_url');
					$hasRedirect=($redirect==false?$redirect:true);
					if($hasRedirect){
						$customer = $this->customers_model->get_customer_by_loginid($data["loginid"]);
						if (!empty($customer)) {
								if ($data["password"]==$customer[0]["password"]) {
										$s = array(
												'currentcustomerid' => $customer[0]["id"],
												'currentcustomerloginid' => $customer[0]["loginid"],
												'currentcustomeremail' => $customer[0]["email"],
												'currentcustomerfirstname' => $customer[0]["firstname"],
												'currentcustomerlastname' => $customer[0]["lastname"],
												'currentcustomerprofilephoto' => $customer[0]["profilephoto"]
										);
										$this->session->set_userdata($s);
										$loginCount = intval($customer[0]["logincount"]);
										$id = $customer[0]["id"];
										$count = $loginCount + 1;
										$this->customers_model->update_customer($id, Array("logincount" => "$count"));
										$issuccess = true;
								}
						}
						redirect($redirect, 'refresh');
					}
					redirect("customer/login");
        } else {
            $data_seo = array(
                'meta_title' => 'Sign Up for Free',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'signup', $data);
            $this->load->view('templates/footer');
        }
    }

    public function log_in() {
        $this->load->model('Users_model');
        $this->load->library('form_validation');
        if ($this->form_validation->run()) {
            $user_name = $this->input->post('user_name');
            $password = $this->__encrip_password($this->input->post('password'));
            $is_valid = $this->Users_model->validate($user_name, $password);
            if ($is_valid) {
                $data = array(
                    'user_name' => $user_name,
                    'is_logged_in' => true
                );
                $this->session->set_userdata($data);
								redirect('admin/clinics');
            }
        }
        $data_seo = array(
            'meta_title' => '',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $data = array();
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'log_in', $data);
        $this->load->view('templates/footer');

    }

    public function login() {
        $issuccess = false;
        $errors = array();
        $loginCount = 0;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('customerloginid', 'Login ID', 'required|valid_email');
            $this->form_validation->set_rules('customerpassword', 'Password', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error" style="color: red; font-weight: normal;"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
                $data = $this->input->post(NULL, TRUE);
                $customer = $this->customers_model->get_customer_by_loginid($data["customerloginid"]);
                if (!empty($customer)) {
                    if ($this->bcrypt->verify($data["customerpassword"], $customer[0]["password"])) {
                        $s = array(
                            'currentcustomerid' => $customer[0]["id"],
                            'currentcustomerloginid' => $customer[0]["loginid"],
                            'currentcustomeremail' => $customer[0]["email"],
                            'currentcustomerfirstname' => $customer[0]["firstname"],
                            'currentcustomerlastname' => $customer[0]["lastname"],
                            'currentcustomerprofilephoto' => $customer[0]["profilephoto"]
                        );
                        $this->session->set_userdata($s);
                        $loginCount = intval($customer[0]["logincount"]);
                        $id = $customer[0]["id"];
                        $count = $loginCount + 1;
                        $this->customers_model->update_customer($id, Array("logincount" => "$count"));
                        $issuccess = true;
                    } else {
                        $this->form_validation->add_message("password", "Incorrect User/Password");
                    }
                }
            } else {
                $errors["loginerrors"] = validation_errors();
            }
        }
        if ($issuccess) {
						$redirectSignup=$this->session->userdata('redirect_url_signup');
						$hasRedirectSignup=($redirectSignup==false?$redirectSignup:true);
						$redirect=$this->session->userdata('redirect_url');
						$hasRedirect=($redirect==false?$redirect:true);
						if($hasRedirectSignup)
							redirect($redirectSignup, 'refresh');
						else if($hasRedirect)
							redirect($redirect, 'refresh');
            if ($loginCount == 0) {
                //uploadimages();
            } else {
                $data_seo = array(
                    'meta_title' => 'Login',
                    'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                    'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                    'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                    'is_hide_header_slide' => false
                );
                $this->load->view('templates/header', $data_seo);
                $this->load->view('home');
                $this->load->view('templates/footer');
            }
        } else {
            $data_seo = array(
                'meta_title' => 'Login',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            $this->load->view('templates/header', array_merge($data_seo, $errors));
            $this->load->view('home');
            $this->load->view('templates/footer');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
				$this->session->unset_userdata('redirect_url');
        $s = array(
            'currentcustomerid' => null,
            'currentcustomerloginid' => null,
            'currentcustomeremail' => null,
            'currentcustomerfirstname' => null,
            'currentcustomerlastname' => null
        );
        $this->session->set_userdata($s);
        $data_seo = array(
            'meta_title' => 'Login',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view('home');
        $this->load->view('templates/footer');
    }

    public function uploadimages() {
        $issuccess = false;
        $data = null;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $data = $this->input->post(NULL, TRUE);
            $uploadPath = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']) . "uploads/";
            $photoPath = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']) . "assets/img/photos/";

            $customer = $this->customers_model->get_customer_by_id($this->session->userdata('currentcustomerid'));
            if (!empty($customer)) {
                if ($customer[0]["profilephoto"] != "") {
                    unlink($photoPath . $customer[0]["profilephoto"]);
                }
                if ($customer[0]["discountproofphoto"] != "") {
                    unlink($photoPath . $customer[0]["discountproofphoto"]);
                }
            }

            $customer = array();
            $customer["profilephoto"] = $this->input->post("profilephotofile-filename");
            $customer["discountproofphoto"] = $this->input->post("discountphotofile-filename");
            $this->customers_model->update_customer($this->session->userdata('currentcustomerid'), $customer);

            rename($uploadPath . $customer["profilephoto"], $photoPath . $customer["profilephoto"]);
            rename($uploadPath . $customer["discountproofphoto"], $photoPath . $customer["discountproofphoto"]);

            $this->session->set_userdata("currentcustomerprofilephoto", $customer["profilephoto"]);
            $issuccess = true;
        }
        if ($issuccess) {
            redirect("pages/home");
        } else {
            $data_seo = array(
                'meta_title' => 'Upload Images',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'uploadimages', $data);
            $this->load->view('templates/footer');
        }
    }

    public function schedule($clinic=NULL) {
        $data = null;
        $data_seo = array(
            'meta_title' => 'Create an Appointment',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'schedule', $data);
        $this->load->view('templates/footer');
    }

    public function getcontrolpanel() {
        $data = array();
				$this->load->model('users_model');
        if($this->session->userdata('is_admin') == true){
					$customer = $this->users_model->get_member_by_loginid($this->session->userdata('user_name'));
				}else{
					$customer = $this->customers_model->get_customer_by_id($this->session->userdata('currentcustomerid'));
				}
        if (!empty($customer)) {
            $data["customer"] = $customer[0];
        }        
        echo json_encode($data);
    }

}
