<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Soapnote extends CI_Controller {

    private $view_path = "therapist/";

    public function __construct() {
        parent::__construct();
        $this->load->model('therapists_model');
        $this->load->model('clinics_model');
        $this->load->model('soapnotes_model');
    }

    public function index() {
        $data_seo = array(
            'meta_title' => 'Soap Notes',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $data = array();
        $getData = $this->input->get(NULL, TRUE);
        $therapist_id = $getData["id"];
        $therapist = $this->therapists_model->get_therapist_by_id($therapist_id);
        $clinic = $this->clinics_model->get_clinic_by_id($therapist[0]["clinic_id"]);
        
        $data["therapistid"] = $therapist_id;        
        $data["customerid"] = 0;
        $data["therapist_fullname"] = $therapist[0]["firstname"] . " " . $therapist[0]["lastname"];
        $data["therapist_positiontitle"] = $therapist[0]["position_title"];
        $data["franchise_location"] = $clinic[0]["location"];
        $data["franchise_locationdesc"] = $clinic[0]["location_description"];
        $data["franchise_phoneno"] = $clinic[0]["phone_no"];
        $data["franchise_licenseno"] = $clinic[0]["license_no"];                
        
        $data["client_name"] = "";
        $data["client_date"] = "";
        $data["client_birthdate"] = "";
        $data["client_healthins"] = "";
        $data["client_currentmeds"] = "";        
        
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'soapnote', $data);
        $this->load->view('templates/footer');
    }

    public function save() {        
        $data = array();
        $data = $this->input->post(NULL, TRUE);
        
        if ($this->input->server('REQUEST_METHOD') === 'POST') {                        
            $this->form_validation->set_rules('addl_client_info', 'Additional Notes', 'trim');
            $this->form_validation->set_rules('question_1', 'Goals/Update', 'required');
            $this->form_validation->set_rules('question_2', 'Location/Symptoms/Intensity/Frequency/Duration/Onset', 'required');
            $this->form_validation->set_rules('question_3', null, 'trim');            
            $this->form_validation->set_rules('question_4', null, 'trim');            
            $this->form_validation->set_rules('question_5', null, 'trim');            
            $this->form_validation->set_rules('question_6', null, 'trim');            
            $this->form_validation->set_rules('question_7', null, 'trim');            
            $this->form_validation->set_rules('question_8', null, 'trim');            
            $this->form_validation->set_rules('question_9', null, 'trim');            
            $this->form_validation->set_rules('question_10', null, 'trim');            
            $this->form_validation->set_error_delimiters('<div class="alert alert-error" style="color: red; font-weight: normal;"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            
            if ($this->form_validation->run()) {
                if ($this->soapnotes_model->store_soapnote($data)) {
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
                echo "true";
            } else {
                $data['soapnoteerrors'] = validation_errors();
                echo "false";
            }
        }
        
        $data_seo = array(
            'meta_title' => 'Soap Notes',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );                
        $this->load->view('templates/header', $data_seo);
        $data["addl_client_info"] = "here";
        $this->load->view($this->view_path . 'soapnote', $data);
        $this->load->view('templates/footer');
    }
    
    public function listing() {
        $data_seo = array(
            'meta_title' => 'Soap Notes List',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        
        $data = array();
        $data["recordlist"] = $this->soapnotes_model->get_all_soapnotes();
        
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'soapnotelisting', $data);
        $this->load->view('templates/footer');
    }    
}
