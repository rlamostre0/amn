<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Location extends CI_Controller {

    private $view_path = "location/";

    public function __construct() {
        parent::__construct();
        $this->load->model('clinics_model');
        $this->load->model('locations_model');
    }

    public function index() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
        );

        $data['services'] = $this->clinics_model->get_services();
        $data["usstates"] = $this->locations_model->get_all_us_states();
        
        $locations = Array();
        $s = $this->clinics_model->get_clinic_states();
        foreach ($s as $p) {
            $c = $this->clinics_model->get_clinics_cities($p["address_state"]);
            array_push($locations, Array($p, $c));
        }
        $data["locations"] = $locations;
        
        $data["search_type"] = 0;
        $data["zip_for_search"] = "";
        $data["miles_for_search"] = "";
        $data["city_for_search"] = "";
        $data["state_for_search"] = "";
        $data["search_now"] = 0;
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'search', $data);
        $this->load->view('templates/footer');
    }

    public function search() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
        );
        $data['services'] = $this->clinics_model->get_services();
        $data["usstates"] = $this->locations_model->get_all_us_states();
        
        $locations = Array();
        $s = $this->clinics_model->get_clinic_states();
        foreach ($s as $p) {
            $c = $this->clinics_model->get_clinics_cities($p["address_state"]);
            array_push($locations, Array($p, $c));
        }
        $data["locations"] = $locations;
        
        $data["search_type"] = 0;
        $data["search_now"] = 0;
        $data["zip_for_search"] = "";
        $data["miles_for_search"] = "";
        $data["city_for_search"] = "";
        $data["state_for_search"] = "";
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $zip = $this->input->post('search_zipcode');
            $miles = $this->input->post('search_mile');
            $state = $this->input->post('search_state');
            $city = $this->input->post('search_city');
            $data["zip_for_search"] = $zip;
            $data["miles_for_search"] = $miles;
            $data["city_for_search"] = $city;
            $data["state_for_search"] = $state;
            if ($zip != null && $zip != "") {
                $data["search_now"] = 1;
                $data["search_type"] = 1;
            }
            if ($miles != null && $miles != "") {
                $data["search_now"] = 1;
                $data["search_type"] = 2;
            }
            if (($city != null && $city != "") && ($state != null && $state != "")) {
                $data["search_now"] = 1;
                $data["search_type"] = 3;
            }
        }
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'search', $data);
        $this->load->view('templates/footer');
    }

    public function searchbycity() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
        );
        $data['services'] = $this->clinics_model->get_services();
        $data["usstates"] = $this->locations_model->get_all_us_states();
        
        $locations = Array();
        $s = $this->clinics_model->get_clinic_states();
        foreach ($s as $p) {
            $c = $this->clinics_model->get_clinics_cities($p["address_state"]);
            array_push($locations, Array($p, $c));
        }
        $data["locations"] = $locations;
        
        $data["search_now"] = 0;
        $data["city_for_search"] = "";
        $data["state_for_search"] = "";
        if ($this->input->server('REQUEST_METHOD') === 'GET') {
            $city = $this->input->get('city');
            $state = $this->input->get('state');
            $data["city_for_search"] = $city;
            $data["state_for_search"] = $state;
            $data["search_now"] = 1;
        }
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'searchbycity', $data);
        $this->load->view('templates/footer');
    }

    public function directory() {
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
        );
        $data['services'] = $this->clinics_model->get_services();
        
        $locations = Array();
        $s = $this->clinics_model->get_clinic_states();
        foreach ($s as $p) {
            $c = $this->clinics_model->get_clinics_cities($p["address_state"]);
            array_push($locations, Array($p, $c));
        }
        $data["locations"] = $locations;
        
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'searchdirectory', $data);
        $this->load->view('templates/footer');
    }

    public function getcities() {
        $state = $this->input->post('state');
        echo json_encode($this->locations_model->get_all_us_cities($state));
    }

    public function indiv(){

        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'inline_css' => '.items.locations{display:block;background:#272727 !important};.locations.top.nav a{background:#272727}',
            'is_hide_header_slide' => true
        );
        $state = $this->uri->segment(3);
        $s = $this->clinics_model->get_clinic_by_id($state);
        $arr = array(
                'state' => $state,
                's' => $s
                );
        
        $s = $this->clinics_model->get_clinic_by_state($state);
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path.'indiv',$arr);
        $this->load->view('templates/footer');
    }

}
