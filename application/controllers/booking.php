<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  - Booking Controller , Amn
  - Jes 6/24/14
  - handles booking request and views
 */

use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;
use Omnipay\Common\GatewayFactory;

class Booking extends CI_Controller {

    private $view_folder = "booking/";

    function index() {
			//$this->basic();
			$franchise_no = $this->uri->segment(2, 0);
			$this->session->set_userdata('redirect_url', site_url()."book/$franchise_no");
			$userid=$this->session->userdata('currentcustomerid');
			$isLoggedIn=($userid==false?$userid:true);
			$scheduleData=$this->session->userdata('scheduleData');
			$hasSchedule=($scheduleData==false?$scheduleData:true);
			if($isLoggedIn && $hasSchedule){
				redirect(site_url()."booking/basic/$franchise_no", 'refresh');
			}else if(!$hasSchedule){
				redirect(site_url()."schedule/$franchise_no", 'refresh');
			}else if($franchise_no!=0){
				$data_seo = array(
						'meta_title' => 'Step 1 - Personal Data -  Intake Form',
						'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
						'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
						'is_hide_header_slide' => true
				);
				$page_vars=array(
					'franchise_no'=>$franchise_no,
				);
				$this->load->view('templates/header', $data_seo);
				$this->load->view('booking/index',$page_vars);
				$this->load->view('templates/footer');
			}else{
				redirect(site_url(), 'refresh');
			}
    }

    // step 1
    // basic data 
    function basic() {

			$franchise_no = $this->uri->segment(3, 0);
			$hasSetFranchiseNo=$franchise_no!=0;
			$hasSkippedLogin=$this->uri->segment(4)=='skiplogin';
			$userid=$this->session->userdata('currentcustomerid');
			$isLoggedIn=($userid==false?$userid:true);
			$scheduleData=$this->session->userdata('scheduleData');
			$hasSchedule=($scheduleData==false?$scheduleData:true);
			$bookingRules=(
				($hasSetFranchiseNo) && (
					$hasSkippedLogin ||
					$isLoggedIn
				) 
			);
			if(($hasSetFranchiseNo && !$isLoggedIn && !$hasSkippedLogin) || !$hasSchedule){

                $this->session->unset_userdata('scheduleData');
				redirect(site_url()."book/$franchise_no", 'refresh');
			}else if($bookingRules){
				$this->session->unset_userdata('redirect_url');
				$data_seo = array(
            'meta_title' => 'Step 1 - Personal Data -  Intake Form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'is_hide_header_slide' => true
        );

        $this->load->view('templates/header', $data_seo);
        $this->load->view('intakeform');
        $this->load->view('templates/footer');
			}else{
				redirect(site_url(), 'refresh');
			}
    }

    // step 2
    // payment set up 
    function payment() {
        $data_seo = array(
            'meta_title' => 'Step 2 - Payment Method - Intake Form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'is_hide_header_slide' => false
        );
        
        // $formData = ['number' => '4242424242424242', 'expiryMonth' => '6', 'expiryYear' => '2016', 'cvv' => '123'];
        if ($this->input->post('submit', TRUE) == "submit") {

            $gateway = GatewayFactory::create('AuthorizeNet_AIM');

            $gateway->getDefaultParameters();   
            $gateway->setApiLoginId('84zEQe3N');   
            $gateway->setTransactionKey('938gvBY683gQN3d6');
            $gateway->setDeveloperMode(true);

            $formData = $this->get_card_info();
            $amount = $formData['amount'];
            $currency = $formData['currency'];
            unset($formData['amount']);
            unset($formData['currency']);

            $message = "Sorry, there was an error processing your payment. Please check the input fields and try again.";
            
            try {
                $response = $gateway->purchase(['amount' => $amount, 'currency' => $currency, 'card' => $formData])->send();

                if ($response->isSuccessful()) {
                    redirect('booking/success');
                } else {
                    // payment failed: display message to customer

                    $data = array(
                        'error' => TRUE,
                        'message' => $message,
                        'formData' => $formData
                    );
                }
            } catch(\Exception $e) {
                $data = array(
                    'error' => TRUE,
                    'message' => $message,
                    'formData' => $formData
                );
            }
            

        } else {
            $data['message'] = "";
            $data['formData'] = $this->get_card_info();
            $data['error'] = FALSE;
        }

        $this->load->view('templates/header', $data_seo);
        $this->load->view('booking/payment', $data);
        $this->load->view('templates/footer');
    }

    function get_card_info() {
        $formData = array(
            'firstName' => $this->input->post('firstName', TRUE),
            'lastName' => $this->input->post('lastName', TRUE),
            'billingAddress1' => $this->input->post('billingAddress1', TRUE),
            'billingCity' => $this->input->post('billingCity', TRUE),
            'billingState' => $this->input->post('billingState', TRUE),
            'province' => $this->input->post('province', TRUE),
            'billingPostcode' => $this->input->post('billingPostcode', TRUE),
            'billingCountry' => $this->input->post('billingCountry', TRUE),
            'email' => $this->input->post('email', TRUE),
            'billingPhone' => $this->input->post('billingPhone', TRUE),
            'number' => $this->input->post('number', TRUE),
            'cvv' => $this->input->post('cvv', TRUE),
            'issueNumber' => $this->input->post('issueNumber', TRUE),
            'startMonth' => $this->input->post('startMonth', TRUE),
            'startYear' => $this->input->post('startYear', TRUE),
            'expiryMonth' => $this->input->post('expiryMonth', TRUE),
            'expiryYear' => $this->input->post('expiryYear', TRUE),
            'amount' => $this->input->post('amount', TRUE),
            'currency' => $this->input->post('currency', TRUE)
        );

        return $formData;
    }

    //succes payment
    function success() {
        $prev_link = $_SERVER['HTTP_REFERER'];

            $data_seo = array(
                'meta_title' => 'Step 3 - Medical Information - Intake Form',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'is_hide_header_slide' => false
            );

            $this->load->view('templates/header', $data_seo);
            $this->load->view('booking/success');
            $this->load->view('templates/footer');
    }


    // step 3
    // medical information
    function medical() {
        $data_seo = array(
            'meta_title' => 'Step 3 - Medical Information - Intake Form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'is_hide_header_slide' => false
        );

        $data = array();
        //save submitted data to input[type=hidden]
        foreach ($_POST as $key => $value)
            $data['form_intake_data'][$key] = $value;
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_folder . 'medical_info', $data);
        $this->load->view('templates/footer');
    }

    // step 4
    // anatomical black and white chart
    function anatomical() {
        $data_header = array(
            'meta_title' => 'Step 4 - Anatomical - Intake Form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'css' => array('booking.css'),
        );
        $data = array();
        $data_footer = array('js' => array('jquery-ui.js', 'booking.js',));
        //save submitted data to input[type=hidden]
        $data['form_intake_data'] = array();
        foreach ($_POST as $key => $value)
            $data['form_intake_data'][$key] = $value;

        $this->load->view('templates/header', $data_header);
        $this->load->view($this->view_folder . 'anatomical', $data);
        $this->load->view('templates/footer', $data_footer);
    }

    // step 5
    // Waiver
    function waiver() {
        $data_seo = array(
            'meta_title' => 'Step 5 - Waiver - Intake Form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'is_hide_header_slide' => false
        );
        $data = array();
        //save submitted data to input[type=hidden]
        foreach ($_POST as $key => $value)
            $data['form_intake_data'][$key] = $value;
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_folder . 'waiver', $data);
        $this->load->view('templates/footer');
    }

    // step 6
    // Finish and confirm form data
    function finish() {
        $data_seo = array(
            'meta_title' => 'Done - Intake Form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
        );
        $data = array();
        //save submitted data to input[type=hidden]
        foreach ($_POST as $key => $value)
            $data['form_intake_data'][$key] = $value;
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_folder . 'finish', $data);
        $this->load->view('templates/footer');
    }
	
    function contract() {
        $prev_link = $_SERVER['HTTP_REFERER'];

        if ($prev_link == "http://amn.dev/booking/medical_history") {
    		$data_seo = array(
    				'meta_title' => 'Step 2 - Personal Data -  Contract and Waiver Form',
    				'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
    				'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
    				'is_hide_header_slide' => true
    		);
    		$this->load->view('templates/header', $data_seo);
    		$this->load->view('booking/contract',$_POST);
    		$this->load->view('templates/footer');
        } else {
            redirect();
        }
	}

		function confirm_guest(){
			$this->session->set_userdata('redirect_url_signup', site_url()."customer/signup");
        $data_seo = array(
            'meta_title' => 'Find Clinics',
            'meta_keywords' => 'find clinics near you, massage,advance massage network',
            'meta_description' => 'find clinics near you, massage,advance massage network',
            'is_hide_header_slide' => true
        );

			$this->load->view('templates/header', $data_seo);
			$this->load->view('booking/confirmlogin');
			$this->load->view('templates/footer');
		}
		/*function payment() {
			$data_seo = array(
					'meta_title' => 'Step 3 - Personal Data - Payment',
					'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
					'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
					'is_hide_header_slide' => false
			);
			$this->load->view('templates/header', $data_seo);
			$this->load->view('booking/contract',$_POST);
			$this->load->view('templates/footer');
		}*/

        function medical_history() {
            $userda = $this->session->userdata;

            // Redirect user into home page when he hasn't made a schedule yet.
            if(!$userda['scheduleData']){redirect('/pages/home');};
            
            // Proceed if user already made a schedule.
            $data_seo = array(
                'meta_title' => 'Medical History Form',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'is_hide_header_slide' => true
            );
            $data = array();

            $this->load->view('templates/header', $data_seo);
            $this->load->view('medical_history_form');
            $this->load->view('templates/footer');
        }
}
