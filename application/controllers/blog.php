<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blog extends CI_Controller {

    private $view_path = "/";

    public function __construct() {
        parent::__construct();
        $this->load->model('blogs_model');
        $this->load->model('category_model');
    }

    public function index($page = 0) {
        $data_seo = array(
            'meta_title' => 'Blog',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );

        $data = array();
        $per_page = 10;
        $data["recordlist"] = $this->blogs_model->get_limited_blogs($page, $per_page);
        $blog_count = count($this->blogs_model->get_all_blogs());
        $data['categories'] = $this->category_model->get_all_categories();
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&lt;';
        $config['next_link'] = '&gt;';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['base_url'] = site_url('/blog/index');
        $config['uri_segment'] = 3;
        $config['total_rows'] = $blog_count;
        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'blog', $data);
        $this->load->view('templates/footer');
    }

    public function leavecomment() {
        $issuccess = false;
        $data = array();
        $parentBlogId = 0;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            if (!($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0))) {
                $this->form_validation->set_rules('name', 'Name', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
            }
            $this->form_validation->set_rules('content', 'Content', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
                $data = $this->input->post(NULL, TRUE);
                $data["datetime"] = date("Y-m-d H:i:s");
                if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) {
                    $data["customerid"] = $this->session->userdata('currentcustomerid');
                }
                if ($this->blogs_model->store_blog($data)) {
                    $issuccess = true;
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
                $data["parentBlogId"] = $data["parentid"];
            } else {
                $data['leavecommenterrors'] = validation_errors();
                $parentBlogId = intval($this->input->post("parentid"));
                $data["parentBlogId"] = $parentBlogId;
            }
        } else if ($this->input->server('REQUEST_METHOD') === 'GET') {
            $parentBlogId = intval($this->input->get("id"));
            $data["parentBlogId"] = $parentBlogId;
        }
        $data["blog"] = $this->blogs_model->get_blog_by_id($data["parentBlogId"]);
        $data["blog"] = $data["blog"][0];
        $data["comments"] = $this->blogs_model->get_all_comments($data["parentBlogId"]);
        $data["mode"] = "comment";
        if ($issuccess) {
            $data_seo = array(
                'meta_title' => 'Blog | Leave a Comment',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'leavecomment', $data);
            $this->load->view('templates/footer');
        } else {
            $data_seo = array(
                'meta_title' => 'Blog | Leave a Comment',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'blog', $data);
            $this->load->view('templates/footer');
        }
    }

    public function reply() {
        $issuccess = false;
        $data = array();
        $parentBlogId = 0;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            if (!($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0))) {
                $this->form_validation->set_rules('name', 'Name', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
            }

            $this->form_validation->set_rules('content', 'Content', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
                $data = $this->input->post(NULL, TRUE);
                $data["datetime"] = date("Y-m-d H:i:s");
                if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) {
                    $data["customerid"] = $this->session->userdata('currentcustomerid');
                }
                if ($this->blogs_model->store_blog($data)) {
                    $issuccess = true;
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
            } else {
                $data['replyerrors'] = validation_errors();
                $parentBlogId = intval($this->input->post("parentid"));
            }
        } else if ($this->input->server('REQUEST_METHOD') === 'GET') {
            $parentBlogId = intval($this->input->get("id"));
        }
        $data["mode"] = "reply";
        if ($issuccess) {
            redirect("blog");
        } else {
            $data_seo = array(
                'meta_title' => 'Blog | Reply',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            if ($parentBlogId > 0) {
                $data["parentBlogId"] = $parentBlogId;
                $comments = $this->blogs_model->get_blog_by_id($parentBlogId);
                if (!empty($comments)) {
                    $data["comment"] = $comments[0];
                }
            }
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'blog', $data);
            $this->load->view('templates/footer');
        }
    }

    public function add() {
        $issuccess = false;
        $data = null;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('content', 'Content', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            if ($this->form_validation->run()) {
                $data = $this->input->post(NULL, TRUE);
                if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) {
                    $data["customerid"] = $this->session->userdata('currentcustomerid');
                }
                $data["datetime"] = date("Y-m-d H:i:s");
                if ($this->blogs_model->store_blog($data)) {
                    $issuccess = true;
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
            }
        }
        $data["mode"] = "add";
        if ($issuccess) {
            redirect("blog");
        } else {
            $data_seo = array(
                'meta_title' => 'Blog | Create Review',
                'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
                'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
                'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
                'is_hide_header_slide' => false
            );
            $data = array();
            $this->load->view('templates/header', $data_seo);
            $this->load->view($this->view_path . 'blog', $data);
            $this->load->view('templates/footer');
        }
    }

}
