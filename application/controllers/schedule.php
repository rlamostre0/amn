<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Schedule extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('schedule_model');
    }
		function index() {
			/*if ($this->session->userdata('is_logged_in')) {
					redirect('admin/products');
			} else {
					$this->load->view('admin/login');
			}*/
			$franchise_no = $this->uri->segment(2, 0);
			$this->load->model('Clinics_model');
			$clinic=$this->Clinics_model->get_clinic_by_franchiseno($franchise_no);
			$this->load->model('Therapists_model');
			$therapists=$this->Therapists_model->get_therapist_clinic_by_franchiseno($franchise_no); 
			$clinic=$clinic[0];
			$data_seo = array(
				'meta_title' => 'Find Clinics',
				'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
				'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
				'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
				'is_hide_header_slide' => true
			);
			$page_var=array(
				//'get_schedules'=>$this->get_schedule($franchise_no),
				'clinic'=>$clinic,
				'therapists'=>$therapists
			);
			$userid=$this->session->userdata('currentcustomerid');
			$isLoggedIn=($userid==false?$userid:true);
			$continueToSchedules=(
				//isLogggedIn and admin
				//hasFranchiseNo set
				true
			);
			if($continueToSchedules){
				$this->load->view('templates/header', $data_seo);
				$this->load->view('schedule/index',$page_var);
				$this->load->view('templates/footer2');
			}else{
				// redirect(site_url(), 'refresh');
			}
    }
		function get_schedule(){ //($franchiseNo) 
			$franchiseNo=$this->input->post('franchiseNo');
			$therapist_id=$this->input->post('therapist_id');
			if(!$franchiseNo){
				return false;
			}
			$this->load->model('Schedule_model');
			$customersList=$this->Schedule_model->customer_schedules($franchiseNo,$therapist_id); 
			$this->load->model('Block_model');
			$blocksList=$this->Block_model->getBlock($franchiseNo,$therapist_id); 
			$customers=array();
			foreach($customersList as $customer){
				$start = date("Y-m-d\TH:i:s", strtotime($customer['start']));
				$end = date("Y-m-d\TH:i:s", strtotime($customer['end']));
				if($admin=false || ($customer['id']==$this->session->userdata('currentcustomerid') && isset($customer['id']))){
					$title=$customer['lastname']." ".$customer['firstname']." ".substr($customer['middlename'],0,1);
				}else{
					$title="reserved";
				}
				$customers[]=array(
					'title'=>$title,
					'start'=>$start,
					'end'=>$end,
				);
			}
			foreach($blocksList as $block){
				$start = date("Y-m-d\TH:i:s", strtotime($block['start']));
				$end = date("Y-m-d\TH:i:s", strtotime($block['end']));
				$title=$block['description'];
				$customers[]=array(
					'title'=>$title,
					'start'=>$start,
					'end'=>$end,
					'color'=>'#5c071a',
					'borderColor'=>'#fa5c1a'
				);
			}
			$return_json['customers']=$customers;
			//return json_encode($customers);
			echo json_encode($customers);
		}
		function get_therapists(){
			$franchiseNo=$this->input->post('franchiseNo');
			$this->load->model('Therapists_model');
			$therapistList=$this->Therapists_model->get_therapist_clinic_by_franchiseno($franchiseNo); 
			$therapists=array();
			$therapists[0]=array(
				"id"=>'Therapists',
				"label"=>"Therapists",
				"branch"=>array(),
				"inode"=>true,
				"open"=>false,
				"icon"=>"folder",
				"size"=>"",
				"radio"=> false,
				'therapists'=>count($therapistList),
			);
			$i=0;
			foreach($therapistList as $list){
				if($i==0){
					$check=true;
					$i++;
				}else{
					$check=false;
				}
				$therapists[0]['branch'][]=array(
					"id"=>$list['therapist_id'],
					"label"=>$list['firstname'].' '.$list['lastname'],
					"branch"=>array(),
					"inode"=>false,
					"open"=>false,
					"radio"=>true,
					'setCheck'=>$check
				);
			}
			echo json_encode($therapists);
			// echo '
				// [{
					// "id":"Therapists",
					// "label":"Therapists",
					// "branch":
					// [{
						// "id":"1",
						// "label":"Amy Whittle",
						// "branch":[],
						// "inode":false,
						// "open":false,
						// "icon":"file"
					// }],
					// "inode":true,
					// "open":false,
					// "icon":"folder",
					// "type":"File folder",
					// "size":""
				// }]
			// ';
		}
	public function blockform (){
		$this->load->view('schedule/blockform');
	}
	public function setschedule ()
	{	
		$customer_id = $this->session->userdata('currentcustomerid');
		if($customer_id)
		{
			if ($this->input->server('REQUEST_METHOD') === 'POST') {
				$this->form_validation->set_rules('event', 'Title', 'required');
				$this->form_validation->set_rules('estart', 'Start', 'required');
				$this->form_validation->set_rules('eend', 'End', 'required');
				
				if ($this->form_validation->run()) {
					$title = $this->input->post("event");
					$eventstart = str_replace("T"," ",$this->input->post("estart"));
					$eventend = str_replace("T"," ",$this->input->post("eend"));
					
					if($this->schedule_model->get_schedule_conflict($eventstart,$eventend))
					{
						$data["therapist_id"] = 1;
						$data["customer_id"] = $customer_id;
						$data["title"] = $title;
						$data["start"] = $eventstart;
						$data["end"] = $eventend;
						$this->schedule_model->store_schedule($data);
						
						echo $this->input->post("event");
					}
					else
						echo "false";
				}
			}
		}
	}
	
	public function schedules($therapist_id=1)
	{	
		$customer_id = $this->session->userdata('currentcustomerid');
		
		if($customer_id)
		{
			$schedules = $this->schedule_model->get_schedules_by_therapist($therapist_id,$customer_id);
			echo json_encode($schedules);
		}
	}
	
	public function otherschedules($therapist_id=1)
	{	
		$customer_id = $this->session->userdata('currentcustomerid');
		if($customer_id)
		{
			$schedules = $this->schedule_model->get_schedules_by_therapist_not($therapist_id,$customer_id);
			echo json_encode($schedules);
		}
	}
	
	public function removeschedule($therapist_id=1)
	{	
		$customer_id = $this->session->userdata('currentcustomerid');
		if($customer_id)
		{
			if ($this->input->server('REQUEST_METHOD') === 'POST') {
				$this->form_validation->set_rules('estart', 'Start', 'required');
				$this->form_validation->set_rules('eend', 'End', 'required');
				if ($this->form_validation->run()) {
					$title = $this->input->post("event");
					$start = str_replace("T"," ",$this->input->post("estart"));
					$end = str_replace("T"," ",$this->input->post("eend"));
					$schedules = $this->schedule_model->delete_schedule($therapist_id,$customer_id,$start,$end);
					echo "true";
				}
			}
		}
	}

	public function save(){
		if(isset($_POST['schedulePost']) && $_POST['schedulePost']=='schedule'){
			$this->session->set_userdata('scheduleData', json_decode($_POST['scheduleData'],true));
			// var_dump(json_decode($_POST['scheduleData'],true));
			redirect('book/'.$_POST['franchisno']);
		}else if(isset($_POST['schedulePost']) && $_POST['schedulePost']=='intakeform'){
			$scheduleData = $this->session->userdata('scheduleData');
			$scheduleData['intakeform'] = $_POST;
			$this->session->set_userdata('scheduleData', $scheduleData);
			echo "true";
		}else if(isset($_POST['schedulePost']) && $_POST['schedulePost']=='medicalhistory'){
			$scheduleData = $this->session->userdata('scheduleData');

			$data = array_merge($scheduleData['intakeform'], $_POST);
			unset($scheduleData['intakeform']);
			$scheduleData['intakeform'] = $data;
			$this->session->set_userdata('scheduleData', $scheduleData);
			echo "true";
		}else if(isset($_POST['schedulePost']) && $_POST['schedulePost']=='contract'){
			$scheduleData = $this->session->userdata('scheduleData');
			$contract=$_POST;
			$intakeform=$scheduleData['intakeform'];
			$painMarker=json_decode($intakeform['painmarker'],true);
			unset($scheduleData['intakeform']);
			unset($contract['schedulePost']);
			unset($intakeform['schedulePost']);
			unset($intakeform['painmarker']);
			$this->load->model('Intakeforms_model');
			$intake_form_id=$this->Intakeforms_model->store_intakeform($intakeform);
			$contract['intake_form_id']=$intake_form_id;
			$scheduleData['intake_form_id']=$intake_form_id;
			for($i=0;$i<count($painMarker);$i++){
				$painMarker[$i]['intake_form_id']=$intake_form_id;
			}
			$this->Intakeforms_model->store_pain_marker(0, json_decode(json_encode($painMarker)));
			$this->load->model('Contracts_model');
			$this->Contracts_model->set_contract($contract);
			$this->load->model('Schedule_model');
			$this->Schedule_model->set_schedule($scheduleData);
			
			$this->session->unset_userdata('scheduleData');
			redirect('booking/payment');
		}else{
			// $this->load->model('Schedule_model');
			// $this->Schedule_model->set_schedule($data);
		}
		// $save=(
			// isset($_POST['start']) && 
			// isset($_POST['end']) && 
			// isset($_POST['customer_id']) &&
			// isset($_POST['assigned_therapist']) &&
			// isset($_POST['clinic_id']) 
		// );
		// if($save){
			// $data = array(
				 // 'start' => $_POST['start'] ,
				 // 'end' => $_POST['end'] ,
				 // 'clinic_id' => $_POST['clinic_id'],
				 // 'assigned_therapist' => $_POST['assigned_therapist'],
				 // 'customer_id' => $_POST['customer_id'],
			// );
			// $this->load->model('Schedule_model');
			// $this->Schedule_model->set_schedule($data);
		// }
	}
	function block_event() {
		$blockPost=$this->input->post(NULL, TRUE);
		$blockPost['description']=trim($blockPost['description']);
		$error1=array(
			'message'=>'Date is not set.',
			'condition'=>(
				$blockPost['start']=='' ||
				$blockPost['end']=='' 
			),
		);
		$start=array(
			'y'=>date('Y',strtotime($blockPost['start'])).'-',
			'm'=>date('m',strtotime($blockPost['start'])).'-',
			'd'=>date('d',strtotime($blockPost['start'])).' ',
			'h'=>date('H',
				strtotime(
					$blockPost['startHour'].':'.
					$blockPost['startMinutes'].' '.
					$blockPost['startMeridiem']
				)
			).':',
			'i'=>date('i',
				strtotime(
					$blockPost['startHour'].':'.
					$blockPost['startMinutes'].' '.
					$blockPost['startMeridiem']
				)
			).':00',
		);
		$blockPost['start']='';
		foreach($start as $key=>$value){
			$blockPost['start']=$blockPost['start'].$value;
		}
		$end=array(
			'y'=>date('Y',strtotime($blockPost['end'])).'-',
			'm'=>date('m',strtotime($blockPost['end'])).'-',
			'd'=>date('d',strtotime($blockPost['end'])).' ',
			'h'=>date('H',
				strtotime(
					$blockPost['endHour'].':'.
					$blockPost['endMinutes'].' '.
					$blockPost['endMeridiem']
				)
			).':',
			'i'=>date('i',
				strtotime(
					$blockPost['endHour'].':'.
					$blockPost['endMinutes'].' '.
					$blockPost['endMeridiem']
				)
			).':00',
		);
		$blockPost['end']='';
		foreach($end as $key=>$value){
			$blockPost['end']=$blockPost['end'].$value;
		}
		unset($blockPost['startHour']);
		unset($blockPost['startMinutes']);
		unset($blockPost['startMeridiem']);
		unset($blockPost['endHour']);
		unset($blockPost['endMinutes']);
		unset($blockPost['endMeridiem']);
		$error2=array(
			'message'=>'Start date should be less than the end date.',
			'condition'=>(
				date('Y-m-d H:i:s',strtotime($blockPost['start'])) > 
				date('Y-m-d H:i:s',strtotime($blockPost['end']))
			),
		);
		$error3=array(
			'message'=>'Please provide a description.',
			'condition'=>(
				$blockPost['description']==''
			),
		);
		$saveConditions=(
			!$error1['condition'] && 
			!$error2['condition'] && 
			!$error3['condition']
		);
		if($saveConditions){
			$this->load->model('Block_model');
			$this->Block_model->setBlock($blockPost);
			echo json_encode(array(
				'success'=>true,
				'eventTitle'=>$blockPost['description'],
				'start'=>date('Y-m-d\TH:i:s',strtotime($blockPost['start'])),
				'end'=>date('Y-m-d\TH:i:s',strtotime($blockPost['end']))
			));
		}else{
			$msg="Errors:\n";
			if($error1['condition']){
				$msg=$msg.$error1['message']."\n";
			}
			if($error2['condition']){
				$msg=$msg.$error2['message']."\n";
			}
			if($error3['condition']){
				$msg=$msg.$error3['message']."\n";
			}
			echo json_encode(array(
				'success'=>false,
				'msg'=>$msg
			));
		}
	}	
	public function clear(){
		$this->session->sess_destroy();
	}
	public function showsession(){
		var_dump($this->session->userdata);
		var_dump($POST);
	}
}
?>