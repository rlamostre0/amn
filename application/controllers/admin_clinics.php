<?php
class Admin_clinics extends CI_Controller {

    /**
     * name of the folder responsible for the views 
     * which are manipulated by this controller
     * @constant string
     */
    const VIEW_FOLDER = 'admin/clinics';

    /**
     * Responsable for auto loading the model
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('clinics_model');
        $this->load->model('customers_model');

        if (!$this->session->userdata('is_logged_in')) {
            redirect('admin/login2');
        }
    }

    public function add_therapist_info() {
        if ($this->input->post('submit', TRUE) == "submit") {

            $firstname = trim($this->input->post('firstname', TRUE));
            $middlename = trim($this->input->post('middlename', TRUE));
            $lastname = trim($this->input->post('lastname', TRUE));
            $suffix = trim($this->input->post('suffix', TRUE));
            $lastname .= ", ".strtoupper($suffix);    //combine suffix to last name

            $license_number = trim($this->input->post('license_number', TRUE));

            $day = trim($this->input->post('birthdate_day', TRUE));
            $month = trim($this->input->post('birthdate_month', TRUE));
            $year = trim($this->input->post('birthdate_year', TRUE));

            $timestamp = mktime(0, 0, 0, $month, $day, $year);
            $birthdate = date("F j, Y", $timestamp);

            $gender = trim($this->input->post('gender', TRUE));
            $address = trim($this->input->post('address', TRUE));
            $contact_number = trim($this->input->post('contact_number', TRUE));

            $therapist_info_id = $this->input->post('therapist_info_id', TRUE);
            $clinic_id = $this->input->post('clinic_id', TRUE);

            $data = array(
                'clinic_id' => $clinic_id,
                'member_id' => $therapist_info_id,
                'firstname' => ucfirst($firstname),
                'middlename' => ucfirst($middlename),
                'lastname' => $lastname,
                'license_number' => $license_number,
                'birthdate' => $birthdate,
                'gender' => $gender,
                'address' => $address,
                'contact_number' => $contact_number
            );

            $therapist_data = array(
                'first_name' => ucfirst($firstname),
                'last_name' => ucfirst($lastname)
            );

            $this->load->model('users_model');
            $this->users_model->update_therapist_info($therapist_data, $therapist_info_id);

            $this->load->model('Therapists_model');
            $this->Therapists_model->add_new_therapist($data);

            $message = "Successfully added ".ucfirst($firstname)." ".ucfirst($lastname)." to the list.";
            $this->session->set_flashdata('message', $message);

            redirect('admin/clinics/update/'.$clinic_id);

        }

    }  

    public function add_therapist() {
        $clinic_id = $this->input->post('clinic_id', TRUE);
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        $email = $this->input->post('email', TRUE);
        $user_role_id = 4; //4 for therapist  as stated in user_role table

        $data = array(
            'user_name' => $username,
            'pass_word' => md5($password),
            'email_addres' => $email,
            'user_role_id' => $user_role_id
        );

        $this->load->model('users_model');
        $this->users_model->add_therapist_information($data);

        $therapist_info_query = $this->users_model->get_therapist_info($data);
        foreach($therapist_info_query as $t) {
            $therapist_info_id = $t->id;
        }

        $page_vars = array(
            'customer' => "",
            'isLoggedIn' => "",
            'clinic_id' => $clinic_id,
            'therapist_info_id' => $therapist_info_id
        );

        $this->load->view('includes/header', $page_vars);
        $this->load->view('admin/therapists/add_information', $page_vars);
        $this->load->view('includes/footer', $page_vars);
    }

    /**
     * Load the main view with all the current model model's data.
     * @return void
     */
    public function index() {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');
        $order = $this->input->post('order');
        $order_type = $this->input->post('order_type');

        //pagination settings
        $config['per_page'] = 5;

        $config['base_url'] = base_url() . 'admin/clinics';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0) {
            $limit_end = 0;
        }

        //if order type was changed
        if ($order_type) {
            $filter_session_data['order_type'] = $order_type;
        } else {
            //we have something stored in the session? 
            if ($this->session->userdata('order_type')) {
                $order_type = $this->session->userdata('order_type');
            } else {
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;

        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data
        //filtered && || paginated
        if ($search_string !== false && $order !== false || $this->uri->segment(3) == true) {

            /*
              The comments here are the same for line 79 until 99

              if post is not null, we store it in session data array
              if is null, we use the session data already stored
              we save order into the the var to load the view with the param already selected
             */
            if ($search_string) {
                $filter_session_data['search_string_selected'] = $search_string;
            } else {
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if ($order) {
                $filter_session_data['order'] = $order;
            } else {
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            if (isset($filter_session_data)) {
                $this->session->set_userdata($filter_session_data);
            }

            //fetch sql data into arrays
            $data['count_products'] = $this->clinics_model->count_clinics($search_string, $order);
            $config['total_rows'] = $data['count_products'];

            //fetch sql data into arrays
            if ($search_string) {
                if ($order) {
                    $data['clinics'] = $this->clinics_model->get_clinics($search_string, $order, $order_type, $config['per_page'], $limit_end);
                } else {
                    $data['clinics'] = $this->clinics_model->get_clinics($search_string, '', $order_type, $config['per_page'], $limit_end);
                }
            } else {
                if ($order) {
                    $data['clinics'] = $this->clinics_model->get_clinics('', $order, $order_type, $config['per_page'], $limit_end);
                } else {
                    $data['clinics'] = $this->clinics_model->get_clinics('', '', $order_type, $config['per_page'], $limit_end);
                }
            }
        } else {

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['count_products'] = $this->clinics_model->count_clinics();
            $data['clinics'] = $this->clinics_model->get_clinics('', '', $order_type, $config['per_page'], $limit_end);
            $config['total_rows'] = $data['count_products'];
        }//!isset($search_string) && !isset($order)
        //initializate the panination helper 
        $this->pagination->initialize($config);

        //load the view
        $data['main_content'] = 'admin/clinics/list';
        $this->load->view('includes/template', $data);
    }

// has therapist
    public function has_therapist() {
        $clinic_id = $this->input->post('clinic_id', TRUE);
        $this->load->model('Therapists_model');
        $query = $this->Therapists_model->get_num_therapist($clinic_id);
        
        echo json_encode($query->num_rows());
    }


    public function delete_sched() {
        $intake_form_id = $this->input->post('id', TRUE);
        $start = $this->input->post('client_start', TRUE);
        $end = $this->input->post('client_end', TRUE);

        $this->load->model('schedule_model');
        $this->schedule_model->delete_sched($intake_form_id, $start, $end);

        $this->session->set_flashdata('message', 'Reservation is successfully cancelled.'); 

        redirect($_SERVER['HTTP_REFERER']);
       
    }

    public function schedule_client() {
        $this->load->model('Therapists_model');
        $this->load->model('schedule_model');
        
        $therapist_id = $this->input->post('therapist_id', TRUE);
        $temp = $this->input->post('selected_date', TRUE);
        $date = explode(" ", $temp);
        $massage_date = $date[0];

        $result = $this->Therapists_model->get_therapist_by_t_id($therapist_id);
        $clinic_id = $result[0]['clinic_id'];

        $query = $this->schedule_model->get_sched_client($clinic_id, $therapist_id, $massage_date);
        
        foreach ($query as $q) {
            $start = explode(" ", $q['start']);
            $q['start'] = $start;
            $end = explode(" ", $q['end']);
            $q['end'] = $end;
        }

        echo json_encode($query);

    }

//index
        public function scheduler() {
            //$franchise_no = $this->uri->segment(2, 0);
            $this->load->model('clinics_model');
            $this->load->model('Therapists_model');
            $this->load->model('schedule_model');

            if (is_numeric($this->uri->segment(4))) {   // magbuhat ug error trap for segments 3 and 4
                $clinic_id = $this->uri->segment(3);
                $therapist_id = $this->uri->segment(4);

                $is_correct = $this->Therapists_model->is_therapist_to_clinic($therapist_id, $clinic_id);

                if ($is_correct) {
                    $massage_date = date("Y-m-d");  //current date set whenever a therapist is clicked
                    $query = $this->schedule_model->get_sched_client($clinic_id, $therapist_id, $massage_date);    
                } else {
                    redirect('admin/clinics');
                }

            }

            if (!is_numeric($this->uri->segment(3)) && !is_numeric($this->uri->segment(4))) {
                
                if($this->uri->segment(3) == 'soap'){
                    redirect('admin_clinics/soap');
                }
                else{
                    redirect('admin/clinics');
                }
            }

            $id = $this->uri->segment(3);
            $clinic = $this->clinics_model->get_clinic_by_id($id);
            $therapists = $this->Therapists_model->get_therapist_by_clinic_id($id); 

			$scripts=array(
				'admin_scheduler'
			);

            if (empty($query)) {
                $query = "";
            }
             if (empty($therapist_id)) {
                $therapist_id = $therapists[0]['id'];
             }

            //prints date in a nice format
            $this->load->model('schedule_model');
            $the_date = $this->schedule_model->convert_date($query);

			$page_vars = array(
                'customer' => "",
                'isLoggedIn' => "",
				'scripts' => $scripts,
				'clinic' =>$clinic,
				'therapists' => $therapists,
                'query' => $query,
                'therapist_id' => $therapist_id,
                'the_date' => $the_date
			);

			$this->load->view('includes/header', $page_vars);
			$this->load->view('admin/scheduler/index', $page_vars);
			$this->load->view('includes/footer', $page_vars);
		}


    public function add() {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            //form validation
            $this->form_validation->set_rules('name', 'name', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');


            //if the form has passed through the validation
            if ($this->form_validation->run()) {
                $data = $this->input->post();
                $data['modified'] = time();
                //if the insert has returned true then we show the flash message
                if ($this->clinics_model->store_clinic($data)) {
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
            }
        }
        //load the view
        $data['services'] = $this->clinics_model->get_services();
        $data['main_content'] = 'admin/clinics/add';
        $this->load->view('includes/template', $data);
    }

    /**
     * Update item by his id
     * @return void
     */
    public function update() {
        //product id 
        $id = $this->uri->segment(4);

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            //form validation
            $this->form_validation->set_rules('name', 'name', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run()) {

                $data = $this->input->post();
                $data['modified'] = time();
                //if the insert has returned true then we show the flash message
                if ($this->clinics_model->update_clinic($id, $data) == TRUE) {
                    $this->session->set_flashdata('flash_message', 'updated');
                } else {
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/clinics/update/' . $id . '');
            }//validation run
        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data
        //product data 
        $data['services'] = $this->clinics_model->get_services();
        $data['clinic'] = $this->clinics_model->get_clinic_by_id($id);
        $clinic_services_array = $this->clinics_model->get_clinic_services_by_id($id);
        foreach ($clinic_services_array as $id => $services) {
            $clinic_services[] = $services['service_id'];
        }

        if (empty($clinic_services)) {
            $clinic_services = "";
        }  


        $this->load->model('Therapists_model');
        $therapists = $this->Therapists_model->get_therapist_by_clinic_id($id);

        if (empty($therapists)) {
            $therapists = "";
        }
        $data['clinic_id'] = $id;

        $data['therapists'] = $therapists;
        $data['clinic_services'] = $clinic_services;
        //load the view
        $data['main_content'] = 'admin/clinics/edit';
        $this->load->view('includes/template', $data);
    }

    public function soap(){

        $uid = $this->uri->segment(3);
        $customers = $this->customers_model->get_customer_by_id($uid);
        $arr = array(
            'uid' => $uid,
            'customers' => $customers
        );
        
        $this->load->view('includes/header');
        $this->load->view('soap/index', $arr);
    }



//update

    /**
     * Delete product by his id
     * @return void
     */
    public function delete() {
        //cllinic  id 
        $id = $this->uri->segment(4);
        $this->clinics_model->delete_clinic($id);
        redirect('admin/clinics');
    }

//edit
}


