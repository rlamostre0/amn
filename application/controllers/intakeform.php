<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Intakeform extends CI_Controller {

    private $view_path = "";

    public function __construct() {
        parent::__construct();
        $this->load->model('intakeforms_model');
    }

    public function index() {
        $data_seo = array(
            'meta_title' => 'Intake form',
            'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
            'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
            'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
            'is_hide_header_slide' => false
        );
        $this->load->view('templates/header', $data_seo);
        $this->load->view($this->view_path . 'intakeform');
        $this->load->view('templates/footer');
    }

    public function add() {
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('address_street', 'Street', 'required');
            $this->form_validation->set_rules('address_city', 'City', 'required');
            $this->form_validation->set_rules('address_state', 'State', 'required');
            $this->form_validation->set_rules('birth_date', 'Birth date', 'required');
            //$this->form_validation->set_rules('gender', 'Gender', 'required');

            $this->form_validation->set_rules('address_zip', 'Zip Address', 'required');
            $this->form_validation->set_rules('contact_home', 'Contact Home Number', 'required|numeric');
            $this->form_validation->set_rules('email', 'Email Address', 'required');
            $this->form_validation->set_rules('q21', 'Medication and Supplements', 'required');

            //$this->form_validation->set_rules('q15', 'Kind of pressure prefered?', 'required');
            $this->form_validation->set_rules('q22', 'Are you pregnant?', 'required');
            $this->form_validation->set_rules('q23', 'Suffer from epilepsy or seizures?', 'required');
            $this->form_validation->set_rules('q24', 'Diabetic?', 'required');
            $this->form_validation->set_rules('q25', 'Surgeries?', 'required');
            $this->form_validation->set_rules('q26', 'Do you have nut allergies?', 'required');
            $this->form_validation->set_rules('q27', 'Other allergies?', 'required');
            $this->form_validation->set_rules('q32', 'Recent accident?', 'required');
            $this->form_validation->set_rules('q33', 'Motor vehicle accident?', 'required');
            $this->form_validation->set_rules('q34', 'Other accident?', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            if ($this->form_validation->run() == FALSE) {
                $data = $this->input->post(NULL, TRUE);
                $pmdata = json_decode($this->input->post("painmarker"));
                if ($this->intakeforms_model->store_intakeform($data)) {
                    $this->intakeforms_model->store_pain_marker(0, $pmdata);
                    $data['flash_message'] = TRUE;
                } else {
                    $data['flash_message'] = FALSE;
                }
                echo "true";
            } else {
                $data['intakeformerrors'] = validation_errors();
                echo "false";
            }
        }
        /* $data_seo = array(
          'meta_title' => 'Intake form',
          'meta_keywords' => 'Massage, massage therapist, massage therapy, day spas, spas, medical day spa, medical Massage, pain management, spa massages, swedish Massage, deep tissue massage, sports massage, thai massage, neuromuscular massage, structural integration, rolfing, orthopedic massage, lymphatic massage, cranial sacral massage, myofascial release, prostate massage',
          'meta_description' => 'Advanced Massage Network, Your Medical Massage Resource, Your best local massage therapists at one low cost in one network.',
          'inline_css' => '.items.home{display:block;background:#272727 !important};.home.top.nav a{background:#272727}',
          'is_hide_header_slide' => false
          );
          $this->load->view('templates/header', $data_seo);
          $this->load->view($this->view_path . 'intakeform');
          $this->load->view('templates/footer'); */
    }

}
