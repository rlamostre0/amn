<?php

class UploadImage extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function uploadprofilephoto() {
        $config['upload_path'] = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']) . "uploads/";
        $config['allowed_types'] = "gif|jpg|jpeg|png";
        $config['max_size'] = "5000";
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload("profilephotofile")) {
            $finfo = $this->upload->data();
            $this->createThumbnail($finfo['file_name']);
            $data['uploadInfo'] = $finfo;
            $data['thumbnail_name'] = $finfo['raw_name'] . '_thumb' . $finfo['file_ext'];
            $data['file_name'] = $finfo['file_name'];
            $data['url'] = str_replace("index.php/", "", base_url()) . "uploads/" . $finfo['file_name'];
            echo json_encode($data);
        }
    }

    function uploaddiscountphoto() {
        $config['upload_path'] = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']) . "uploads/";
        $config['allowed_types'] = "gif|jpg|jpeg|png";
        $config['max_size'] = "5000";
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload("discountphotofile")) {
            $finfo = $this->upload->data();
            $this->createThumbnail($finfo['file_name']);
            $data['uploadInfo'] = $finfo;
            $data['thumbnail_name'] = $finfo['raw_name'] . '_thumb' . $finfo['file_ext'];
            $data['file_name'] = $finfo['file_name'];
            $data['url'] = str_replace("index.php/", "", base_url()) . "uploads/" . $finfo['file_name'];
            echo json_encode($data);
        }
    }

    function createThumbnail($filename) {
        $config['image_library'] = "gd2";
        $config['source_image'] = "uploads/" . $filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = "80";
        $config['height'] = "80";
        $this->load->library('image_lib', $config);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }

}
