<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  | example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  | http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  | $route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  | $route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */
$route['default_controller'] = 'pages';
$route['404_override'] = '';

/*pages*/
$route['sitemap'] = 'pages/sitemap';
/*end pages*/

/* admin */
$route['admin'] = 'user/index';
$route['admin/signup'] = 'user/signup';
$route['admin/create_member'] = 'user/create_member';
$route['admin/login'] = 'user/index';
$route['admin/logout'] = 'user/logout';
$route['admin/login/validate_credentials'] = 'user/validate_credentials';
/*
  $route['admin/products'] = 'admin_products/index';
  $route['admin/products/add'] = 'admin_products/add';
  $route['admin/products/update'] = 'admin_products/update';
  $route['admin/products/update/(:any)'] = 'admin_products/update/$1';
  $route['admin/products/delete/(:any)'] = 'admin_products/delete/$1';
  $route['admin/products/(:any)'] = 'admin_products/index/$1'; //$1 = page number */

$route['pricingpage/index'] = 'pricingpage/index';
$route['admin/clinics'] = 'admin_clinics/index';
$route['admin/schedule_client'] = 'admin_clinics/schedule_client';
$route['admin/delete_sched'] = 'admin_clinics/delete_sched';
$route['admin/clinics/add'] = 'admin_clinics/add';
$route['admin/clinics/update'] = 'admin_clinics/update';
$route['admin/scheduler'] = 'admin_clinics/scheduler/$1';
$route['admin/scheduler/(:any)'] = 'admin_clinics/scheduler/$1';
$route['admin/scheduler/(:any)/(:any)'] = 'admin_clinics/scheduler/$1/$1';
$route['admin/view_intake_form/(:any)'] = 'admin_clinics/view_intake_form/$1';
$route['admin/clinics/view_schedule/(:any)'] = 'admin_clinics/view_schedule';
$route['admin/clinics/update/(:any)'] = 'admin_clinics/update/$1';
$route['admin/clinics/delete/(:any)'] = 'admin_clinics/delete/$1';
$route['admin/clinics/(:any)'] = 'admin_clinics/index/$1'; //$1 = page number

$route['admin/soap(:any)'] = 'admin_clinics/soap/$1';

$route['admin/clinics/has_therapist'] = 'admin_clinics/has_therapist'; //$1 = page number
$route['admin/clinics/add_therapist'] = 'admin_clinics/add_therapist';
$route['admin/clinics/add_therapist_info'] = 'admin_clinics/add_therapist_info';



/*-- superadmin --*/
$route['superadmin/add_therapist'] = 'superadmin/add_therapist';



/*-- bookings --*/
$route['booking/confirm_guest'] = 'booking/confirm_guest';
$route['book/(:any)'] = 'booking/index/$1';
$route['booking/medical_history'] = 'booking/medical_history';
$route['booking/payment'] = 'booking/payment';
$route['booking/success'] = 'booking/success';
$route['booking/get_card_info'] = 'booking/get_card_info';
$route['booking/skip/18c48e'] = 'booking/skip/18c48e';  //skip intake form filling out
$route['booking/skip/08d271'] = 'booking/skip/08d271';  //skip medical history filling out

/*-- end-bookings --*/
/*-- schedule --*/
$route['schedule'] = 'schedule/index';
$route['schedule/blockform'] = 'schedule/blockform';
$route['schedule/block_event'] = 'schedule/block_event';
$route['schedule/save'] = 'schedule/save';
$route['schedule/get_schedule'] = 'schedule/get_schedule';
$route['schedule/get_therapists'] = 'schedule/get_therapists';
$route['schedule/clear'] = 'schedule/clear';
$route['schedule/showsession'] = 'schedule/showsession';
$route['schedule/(:any)'] = 'schedule/index/$1';
/*-- end-schedule --*/


/*-- customer --*/
$route['customer/booking_login/'] = 'customer/booking_login';

$route['booking/clear_sess/'] = 'booking/clear_sess';

/*-- end of customer --*/

$route['location/indiv/(:any)'] = 'location/indiv/$1';
/* End of file routes.php */
/* Location: ./application/config/routes.php */