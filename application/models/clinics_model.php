<?php

class Clinics_model extends CI_Model {

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_schedule_by_clinic($id) {
        $this->db->select('*');
        $this->db->where('therapist_id', $id);
        $query = $this->db->get('appointments');
        return $query->result_array();
    }
    
    /**
     * Get clinic by his id
     * @param int $clinic_id 
     * @return array
     */
    public function get_clinic_by_id($id) {
        $this->db->select('*');
        $this->db->from('clinics');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_clinic_by_state($id){
        $this->db->select('*');
        $this->db->from('clinics');
        $this->db->where('address_state',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_clinic_by_franchiseno($id) {
        $this->db->select('*');
        $this->db->from('clinics');
        $this->db->where('franchiseno', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get services by id
     * @param int $clinic_id 
     * @return array
     */
    public function get_clinic_services_by_id($id) {
        $this->db->select('service_id');
        $this->db->from('clinics_services');
        $this->db->where('clinic_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get product by his is
     * @param int $product_id 
     * @return array
     */
    public function get_services() {
        $this->db->select('*');
        $this->db->from('services');
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Fetch manufacturers data from the database
     * possibility to mix search, filter and order
     * @param string $search_string 
     * @param strong $order
     * @param string $order_type 
     * @param int $limit_start
     * @param int $limit_end
     * @return array
     */
    public function get_clinics($search_string = null, $order = null, $order_type = 'Asc', $limit_start = null, $limit_end = null) {

        $this->db->select('*');
        $this->db->from('clinics');

        if ($search_string) {
            $this->db->like('name', $search_string);
        }
        $this->db->group_by('id');

        if ($order) {
            $this->db->order_by($order, $order_type);
        } else {
            $this->db->order_by('id', $order_type);
        }

        if ($limit_start && $limit_end) {
            $this->db->limit($limit_start, $limit_end);
        }

        if ($limit_start != null) {
            $this->db->limit($limit_start, $limit_end);
        }

        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     * Count the number of rows
     * @param int $search_string
     * @param int $order
     * @return int
     */
    function count_clinics($search_string = null, $order = null) {
        $this->db->select('*');
        $this->db->from('clinics');
        if ($search_string) {
            $this->db->like('name', $search_string);
        }
        if ($order) {
            $this->db->order_by($order, 'Asc');
        } else {
            $this->db->order_by('id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     * Store the new item into the database
     * @param array $data - associative array with data to store
     * @return boolean 
     */
    function store_clinic($data) {
        $services = $data['services'];
        unset($data['services']);
        $insert = $this->db->insert('clinics', $data);
        if ($insert) {
            return $this->store_clinic_services($services, $this->db->insert_id());
        }
        return false;
    }

    /**
     * Add / Update clinic services
     * @param array $clinic_id = clinic's id
     * @param array $data -  associative array with data to store
     * @return boolean
     */
    function store_clinic_services($data, $clinic_id) {
        $array_services = array();
        foreach ($data as $i => $service_id) {
            $array_services[] = array('service_id' => $service_id, 'clinic_id' => $clinic_id);
        }
        $this->db->where('clinic_id', $clinic_id);
        $this->db->delete('clinics_services');


        return $this->db->insert_batch('clinics_services', $array_services);
    }

    /**
     * Update manufacture
     * @param array $data - associative array with data to store
     * @return boolean
     */
    function update_clinic($id, $data) {
        $services = $data['services'];
        unset($data['services']);

        $this->db->where('id', $id);
        $this->db->update('clinics', $data);
        $report = array();

        $this->store_clinic_services($services, $id);

        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete manufacturer
     * @param int $id - manufacture id
     * @return boolean
     */
    function delete_clinic($id) {
        $this->db->where('id', $id);
        $this->db->delete('clinics');

        $this->db->where('clinic_id', $id);
        $this->db->delete('clinics_services');
    }

    /**
     * Search Clinics from location page
     * @param varchar $zip - zip code
     * @param varchar $miles - miles away from the user
     * @param array  $services - array of selected services
     * @return results
     */
    function search_clinic_via_location($zip, $miles, $services) {
        $sql = " SELECT DISTINCT(C.id),C.* FROM clinics C LEFT JOIN  clinics_services CS ON C.id=CS.clinic_id WHERE CS.service_id IN (" . implode(",", $services) . ") ";
        if ((int) $zip > 0) {
            $sql .=" AND C.address_zipcode = '" . $zip . "'";
        }
        return $this->db->query($sql)->result();
    }

    /**
     * Get Clinic's States
     * @return results
     */
    function get_clinic_states() {
        $query = $this->db->query('SELECT DISTINCT states.state,clinics.address_state FROM clinics '
                . 'LEFT JOIN states ON states.state_code=clinics.address_state '
                . 'ORDER BY clinics.address_state ASC');
        return $query->result_array();
    }

    /**
     * Get Clinics by states
     * @param varchar $state - state
     * @return results
     */
    function get_clinics_cities($state) {
        $query = $this->db->query('SELECT DISTINCT address_city, address_state '
                . 'FROM clinics WHERE address_state = ? '
                . 'ORDER BY address_state, address_city ASC', $state);
        return $query->result_array();
    }

}
