<?php

class Intakeforms_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_intakeform_by_id($id) {
        $this->db->select('*');
        $this->db->from('intake_forms');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function store_intakeform($data) {
        $insert = $this->db->insert('intake_forms', $data);
				$insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function update_intakeform($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('intake_forms', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete_intakeform($id) {
        $this->db->where('id', $id);
        $this->db->delete('intake_forms');
    }

    function store_pain_marker($userId, $data) {
        for ($i = 0; $i < count($data); $i++) {
            $p = Array();
            $p["userid"] = $userId;
            $p["indexno"] = $data[$i]->indexno;
            $p["x"] = $data[$i]->x;
            $p["y"] = $data[$i]->y;
            $p["modalitytype"] = $data[$i]->modalitytype;
            $p["date"] = $data[$i]->date;
            $this->db->insert('pain_marker', $p);
        }
    }

    public function get_pain_marker($userId) {
        $this->db->select('*');
        $this->db->from('pain_marker');
        $this->db->where('userid', $userId);
        $query = $this->db->get();
        return $query->result_array();
    }

}
