<?php

class Soapnotes_model extends CI_Model {

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get product by his is
     * @param int $id 
     * @return array
     */
    public function get_soapnote_by_id($id) {
        $this->db->select('*');
        $this->db->from('soap_notes');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Get soapnote by his is
     * @return array
     */
    public function get_all_soapnotes() {
        $query = $this->db->query('SELECT soap_notes.*, '
                . 'customers.lastname as client_lastname, customers.firstname as client_firstname, '
                . 'customers.email as client_email, customers.profilephoto as client_photo,'
                . 'therapists.lastname as therapist_lastname, therapists.firstname as therapist_firstname, '
                . 'therapists.license_number as therapist_license_number, therapists.position_title as therapist_position_title '
                . 'FROM soap_notes '
                . 'LEFT JOIN customers ON customers.id=soap_notes.customer_id '
                . 'LEFT JOIN therapists ON therapists.id=soap_notes.therapist_id '
                . 'ORDER BY soap_notes.datetime DESC');
        return $query->result_array();
    }

    /**
     * Fetch soapnotes data from the database
     * possibility to mix search, filter and order
     * @param string $search_string 
     * @param strong $order
     * @param string $order_type 
     * @param int $limit_start
     * @param int $limit_end
     * @return array
     */
    public function get_soapnotes($search_string = null, $order = null, $order_type = 'Asc', $limit_start = null, $limit_end = null) {

        $this->db->select('*');
        $this->db->from('soap_notes');

        if ($search_string) {
            $this->db->like('name', $search_string);
        }
        $this->db->group_by('id');

        if ($order) {
            $this->db->order_by($order, $order_type);
        } else {
            $this->db->order_by('id', $order_type);
        }

        if ($limit_start && $limit_end) {
            $this->db->limit($limit_start, $limit_end);
        }

        if ($limit_start != null) {
            $this->db->limit($limit_start, $limit_end);
        }

        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     * Count the number of rows
     * @param int $search_string
     * @param int $order
     * @return int
     */
    function count_soapnotes($search_string = null, $order = null) {
        $this->db->select('*');
        $this->db->from('soap_notes');
        if ($search_string) {
            $this->db->like('name', $search_string);
        }
        if ($order) {
            $this->db->order_by($order, 'Asc');
        } else {
            $this->db->order_by('id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     * Store the new item into the database
     * @param array $data - associative array with data to store
     * @return boolean 
     */
    function store_soapnote($data) {
        $insert = $this->db->insert('soap_notes', $data);
        return $insert;
    }

    /**
     * Update manufacture
     * @param array $data - associative array with data to store
     * @return boolean
     */
    function update_soapnote($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('soap_notes', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete soapnote
     * @param int $id - manufacture id
     * @return boolean
     */
    function delete_soapnote($id) {
        $this->db->where('id', $id);
        $this->db->delete('soap_notes');
    }

}
