<?php

class Locations_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_location_by_id($id) {
        $this->db->select('*');
        $this->db->from('locations');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_locations() {
        $this->db->select('*');
        $this->db->from('locations');        
        $query = $this->db->get();
        return $query->result_array();
    }

    function store_location($data) {
        $insert = $this->db->insert('locations', $data);
        return $insert;
    }

    function update_location($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('locations', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_all_us_cities($state) {
        $query = $this->db->query("SELECT * FROM cities WHERE state_code=? ORDER BY city ASC", Array($state));
        return $query->result_array();
    }

    public function get_all_us_states() {
        $query = $this->db->query("SELECT * FROM states ORDER BY state ASC");
        return $query->result_array();
    }

}
