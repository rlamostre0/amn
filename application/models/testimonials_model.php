<?php

class Testimonials_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_all_testimonials() {
        $query = $this->db->query('SELECT testimonials.*, customers.lastname, customers.firstname, customers.email as cemail, customers.profilephoto '
                . 'FROM testimonials '
                . 'LEFT JOIN customers ON customers.id=testimonials.customerid '
                . 'WHERE parentid <= 0 '
                . 'ORDER BY testimonials.datetime DESC');
        return $query->result_array();
    }

    public function get_all_comments($id) {
        $query = $this->db->query('SELECT testimonials.*, customers.lastname, customers.firstname, customers.email as cemail, customers.profilephoto '
                . 'FROM testimonials '
                . 'LEFT JOIN customers ON customers.id=testimonials.customerid '
                . 'WHERE testimonials.parentid=? '
                . 'ORDER BY testimonials.datetime DESC', Array($id));
        return $query->result_array();
    }

    public function get_testimonial_by_id($id) {
        $query = $this->db->query('SELECT testimonials.*, customers.lastname, customers.firstname, customers.email as cemail, customers.profilephoto '
                . 'FROM testimonials '
                . 'LEFT JOIN customers ON customers.id=testimonials.customerid '
                . 'WHERE testimonials.id = ?', Array($id));
        return $query->result_array();
    }

    public function store_testimonial($data) {
        $insert = $this->db->insert('testimonials', $data);
        return $insert;
    }

    public function update_testimonial($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('testimonials', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_limited_testimonials($offset = 0, $limit = 0) {
        return $this->db->get('testimonials', $limit, $offset)->result_array();
    }

    public function delete_testimonial($id) {
        $this->db->where('id', $id);
        $this->db->delete('testimonials');
    }

}
