<?php

class Customers_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_customer_by_id($id) {
        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_customer_by_loginid($loginid) {
        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('loginid', $loginid);
        $query = $this->db->get();
        return $query->result_array();
    }

    function store_customer($data) {
        $insert = $this->db->insert('customers', $data);
        return $insert;
    }

    function update_customer($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('customers', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete_customer($id) {
        $this->db->where('id', $id);
        $this->db->delete('customers');
    }

}
