<?php
class Block_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function setBlock($data){
		$insert=$this->db->insert('blocks', $data); 
		return $insert;
	}
	function getBlock($franchiseNo,$therapist_id){
		$this->db->select('*');
		$this->db->from('blocks');
		$this->db->join('clinics', 'blocks.clinic_id = clinics.id');
		$this->db->where('clinics.franchiseno', $franchiseNo); 
		$this->db->where('blocks.therapist_id', $therapist_id); 
		$query = $this->db->get();
		return $query->result_array();
	}
}