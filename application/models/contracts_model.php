<?php

class Contracts_model extends CI_Model {

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	public function set_contract($data){
		$insert = $this->db->insert('contracts', $data);
		return $insert;
	}
}
