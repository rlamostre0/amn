<?php

class Blogs_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_all_blogs() {
        $query = $this->db->query('SELECT blogs.*, customers.lastname, customers.firstname, customers.email as cemail, customers.profilephoto '
                . 'FROM blogs '
                . 'LEFT JOIN customers ON customers.id=blogs.customerid '
                . 'WHERE parentid <= 0 '
                . 'ORDER BY blogs.datetime DESC');
        return $query->result_array();
    }

    public function get_limited_blogs($offset = 0, $limit = 0) {
        return $this->db->get('blogs', $limit, $offset)->result_array();
    }

    public function get_blogs() {
        return $this->db->get('blogs')->result_array();
    }

    public function get_all_comments($id) {
        $query = $this->db->query('SELECT blogs.*, customers.lastname, customers.firstname, customers.email as cemail, customers.profilephoto '
                . 'FROM blogs '
                . 'LEFT JOIN customers ON customers.id=blogs.customerid '
                . 'WHERE blogs.parentid=? '
                . 'ORDER BY blogs.datetime DESC', Array($id));
        return $query->result_array();
    }

    public function get_all_comments_count($id) {
        $query = $this->db->query('SELECT COUNT(*) AS thiscount '
                . 'FROM blogs WHERE blogs.parentid=? ', Array($id));
        return $query->first_row()->thiscount;
    }
    
    public function get_blog_by_id($id) {
        $query = $this->db->query('SELECT blogs.*, customers.lastname, customers.firstname, customers.email as cemail, customers.profilephoto '
                . 'FROM blogs '
                . 'LEFT JOIN customers ON customers.id=blogs.customerid '
                . 'WHERE blogs.id = ?', Array($id));
        return $query->result_array();
    }

    function store_blog($data) {
        $insert = $this->db->insert('blogs', $data);
        return $insert;
    }

    function update_blog($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('blogs', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete_blog($id) {
        $this->db->where('id', $id);
        $this->db->delete('blogs');
    }

}
