<?php

class Therapists_model extends CI_Model {

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get therapists by his is
     * @param int $id 
     * @return array
     */

    public function add_new_therapist($data) {
        $this->db->insert('therapists', $data);
    }

    public function get_num_therapist($clinic_id) {
        $this->db->select('*');
        $this->db->from('therapists');
        $this->db->where('clinic_id', $clinic_id);
        $query = $this->db->get();

        
        return $query;
    }

    public function is_therapist_to_clinic($therapist_id, $clinic_id) {
        $this->db->select('*');
        $this->db->from('therapists');
        $this->db->where('id', $therapist_id);
        $this->db->where('clinic_id', $clinic_id);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_clients_by_therapist($id) {
        $this->db->select('*');
        $this->db->from('therapists');
        $this->db->where('clinic_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_therapist_by_id($id) {
        $this->db->select('*');
        $this->db->from('therapists');
        $this->db->where('clinic_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_therapist_by_t_id($id) {
        $this->db->select('*');
        $this->db->from('therapists');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_therapist_by_clinic_id($clinic_id) {
        $this->db->select('*');
        $this->db->from('therapists');
        $this->db->where('clinic_id', $clinic_id);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_therapist_clinic_by_franchiseno($franchiseno) {
        $this->db->select('*, therapists.id as therapist_id');
        $this->db->from('therapists');
				$this->db->join('clinics', 'therapists.clinic_id = clinics.id');
        $this->db->where('clinics.franchiseno', $franchiseno);
        $query = $this->db->get();
        return $query->result_array();
    }
    /**
     * Fetch therapists data from the database
     * possibility to mix search, filter and order
     * @param string $search_string 
     * @param strong $order
     * @param string $order_type 
     * @param int $limit_start
     * @param int $limit_end
     * @return array
     */
    public function get_therapists($search_string = null, $order = null, $order_type = 'Asc', $limit_start = null, $limit_end = null) {

        $this->db->select('*');
        $this->db->from('therapists');

        if ($search_string) {
            $this->db->like('name', $search_string);
        }
        $this->db->group_by('id');

        if ($order) {
            $this->db->order_by($order, $order_type);
        } else {
            $this->db->order_by('id', $order_type);
        }

        if ($limit_start && $limit_end) {
            $this->db->limit($limit_start, $limit_end);
        }

        if ($limit_start != null) {
            $this->db->limit($limit_start, $limit_end);
        }

        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     * Count the number of rows
     * @param int $search_string
     * @param int $order
     * @return int
     */
    function count_therapists($search_string = null, $order = null) {
        $this->db->select('*');
        $this->db->from('therapists');
        if ($search_string) {
            $this->db->like('name', $search_string);
        }
        if ($order) {
            $this->db->order_by($order, 'Asc');
        } else {
            $this->db->order_by('id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     * Store the new item into the database
     * @param array $data - associative array with data to store
     * @return boolean 
     */
    function store_therapist($data) {
        $insert = $this->db->insert('therapists', $data);
        return $insert;
    }

    /**
     * Update manufacture
     * @param array $data - associative array with data to store
     * @return boolean
     */
    function update_therapist($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('therapists', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete therapist
     * @param int $id - manufacture id
     * @return boolean
     */
    function delete_therapist($id) {
        $this->db->where('id', $id);
        $this->db->delete('therapists');
    }

}
