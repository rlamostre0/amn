<?php
class Users_model extends CI_Model {
    
    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }    


    function update_therapist_info($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('memberships', $data);
    }

    function add_therapist_information($data) {
        $this->db->insert('memberships', $data);
    }

    function get_therapist_info($data) {
        $user_name = $data['user_name'];
        $pass_word = $data['pass_word'];
        $email_addres = $data['email_addres'];
        $user_role_id = $data['user_role_id'];

        $this->db->select('*');
        $this->db->from('memberships');
        $this->db->where('user_name', $user_name);
        $this->db->where('pass_word', $pass_word);
        $this->db->where('email_addres', $email_addres);
        $this->db->where('user_role_id', $user_role_id);
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->result();
    }



    /**
     * Validate the login's data with the database
     * @param string $user_name
     * @param string $password
     * @return void
     */

    function validate($user_name, $password) {
        $this->db->where('user_name', $user_name);
        $this->db->where('pass_word', $password);
        $query = $this->db->get('memberships');
        if ($query->num_rows == 1) {
            return true;
        }
    }
    function get_member_by_loginid($user_name) {
        $this->db->where('user_name', $user_name);
        $query = $this->db->get('memberships');
        return $query->result_array();
    }

    /**
     * Serialize the session data stored in the database, 
     * store it in a new array and return it to the controller 
     * @return array
     */
    function get_db_session_data() {
        $query = $this->db->select('user_data')->get('ci_sessions');
        $user = array(); /* array to store the user data we fetch */
        foreach ($query->result() as $row) {
            $udata = unserialize($row->user_data);
            /* put data in array using username as key */
            $user['user_name'] = $udata['user_name'];
            $user['is_logged_in'] = $udata['is_logged_in'];
        }
        return $user;
    }

    /**
     * Store the new user's data into the database
     * @return boolean - check the insert
     */
    function create_member() {

        $this->db->where('user_name', $this->input->post('username'));
        $query = $this->db->get('memberships');

        if ($query->num_rows > 0) {
            echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
            echo "Username already taken";
            echo '</strong></div>';
        } else {

            $new_member_insert_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email_addres' => $this->input->post('email_address'),
                'user_name' => $this->input->post('username'),
                'pass_word' => md5($this->input->post('password'))
            );
            $insert = $this->db->insert('memberships', $new_member_insert_data);
            return $insert;
        }
    }

}
