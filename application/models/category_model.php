<?php

class Category_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_limited_categories($offset = 0, $limit = 0) {
        return $this->db->get('categories', $limit, $offset)->result_array();
    }

    public function get_all_categories() {
        return $this->db->get('categories')->result_array();
    }

    function store_category($data) {
        $insert = $this->db->insert('categories', $data);
        return $insert;
    }

    function update_blog($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if ($report !== 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete_blog($id) {
        $this->db->where('id', $id);
        $this->db->delete('categories');
    }

}
