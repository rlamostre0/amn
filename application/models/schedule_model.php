<?php

class Schedule_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function delete_sched($intake_form_id, $start, $end) {
        $this->db->where('intake_form_id', $intake_form_id);
        $this->db->where('start', $start);
        $this->db->where('end', $end);
        $this->db->delete('schedules');
    }

    public function get_sched_for_time($query, $tbl_time) {

        foreach($query as $q) {
            $temp = explode(" ", $q['start']);
            $start_time = substr($temp[1], 0, 5);
            $start_time = trim($start_time);
            $tbl_time = trim($tbl_time);

            if ($start_time == $tbl_time) {
                $data['name'] = $q['name'];

                if (empty($data['name'])) {
                    $data['name'] = "Reserved";
                }

                $data['intake_form_id'] = $q['intake_form_id'];
                
                break;
            } else {
                $data = "";
            }
        }

        return $data;
    }

    public function convert_date($query) {

    	if (!empty($query)) {
	    	foreach ($query as $q) {
			    $the_date = explode(" ", $q['start']);
			    $split_date = explode("-", $the_date[0]);
			    break;
			}
    	} else {
    		$split_date = array(
    			0 => date('Y'),
    			1 => date('m'),
    			2 => date('d')
    		);
    	}

		$timestamp = mktime(0, 0, 0, $split_date[1], $split_date[2], $split_date[0]);
		$the_date = date("F j, Y", $timestamp);
		return $the_date;
    }
     public function get_sched_client($clinic_id, $therapist_id, $massage_date) {    //get client schedules of a certain date
        $this->db->select('*');
        $this->db->from('schedules');
        $this->db->join('therapists', 'schedules.assigned_therapist = therapists.id');
        $this->db->join('intake_forms', 'schedules.intake_form_id = intake_forms.id');
        $this->db->where('schedules.clinic_id', $clinic_id);
        $this->db->where('assigned_therapist', $therapist_id);
        $this->db->like('start', $massage_date);
        $this->db->like('end', $massage_date);
        $query = $this->db->get();
        return $query->result_array();
    }

	function customer_schedules($franchiseNo,$therapist_id){
		$this->db->select('*, customers.id as id');
		$this->db->from('schedules');
		$this->db->join('intake_forms', 'schedules.intake_form_id = intake_forms.id','left');
		$this->db->join('clinics', 'schedules.clinic_id = clinics.id','left');
		$this->db->join('customers', 'schedules.customer_id = customers.id','left');
		$this->db->where('clinics.franchiseno', $franchiseNo);
		$this->db->where('schedules.assigned_therapist', $therapist_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	function store_schedule($data) {
		$insert = $this->db->insert('schedules', $data);
		return $insert;
	}
	
	public function get_schedules_by_therapist($therapist_id,$customer_id) {
        $this->db->select('title,start,end');
        $this->db->from('schedules');
        $this->db->where('therapist_id', $therapist_id);
		$this->db->where('customer_id', $customer_id);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	public function get_schedules_by_therapist_not($therapist_id,$customer_id) {
        $this->db->select('title,start,end');
        $this->db->from('schedules');
        $this->db->where('therapist_id', $therapist_id);
		$this->db->where_not_in('customer_id', $customer_id);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	public function get_schedule_conflict($start,$end) {
        $this->db->select('*');
        $this->db->from('schedules');
        $this->db->where('start >=', $start);
		$this->db->where('end <=', $end);
        $query = $this->db->get();
		if ($query->num_rows() > 0)
			return false;
		else 
			return true;
    }

    function delete_schedule($therapist_id,$customer_id,$start,$end) {
		$this->db->where('therapist_id', $therapist_id);
		$this->db->where('customer_id', $customer_id);
        $this->db->where('start', $start);
		$this->db->where('end', $end);
        $this->db->delete('schedules');
    }
	public function set_schedule($data){
		$insert = $this->db->insert('schedules', $data);
		return $insert;
	}

}
