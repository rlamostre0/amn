<?php
	//sitemap page
?>

<div class='row' style='padding:50px'>
	<center><h3> Site Map<br/><br/></h3></center>
	<div class='large-4 columns sitemap'>

		<ul> Main Pages
			<li><a href='<?php site_url() ?>/pages/home'>Home Page</a></li>
			<li><a href='<?php site_url() ?>/pages/modalities'>Massage Modality Page</a></li>
			<li><a href='<?php site_url() ?>/temp'>Pricing Page and AMN Information Page</a></li>
			<li><a href='<?php site_url() ?>/pages/gift'>Gift Certificate Page</a></li>
			<li><a href='<?php site_url() ?>/testimonial'>Testimonial Page</a></li>
			<li><a href='<?php site_url() ?>/blog'>AMN Blog Page</a></li>
			<li><a href='<?php site_url() ?>/blog'>Problems and Solutions Page</a></li>
			<li><a href='<?php site_url() ?>'>Site Map</a></li>
			<li><a href='<?php site_url() ?>'>Therapist, Client, and Administration Control Panels</a></li>
			<li><a href="<?php site_url() ?>">AMN how to purchase a franchise website</a></li>
		</ul>
	</div>
	<div class='large-4 columns sitemap'>
		<ul> Location Pages
			<li><a href='<?php site_url() ?>/location'>National Franchise Locations Page</a></li>
			<li><a href='<?php site_url() ?>/location'>The different major city Franchise Location Page</a></li>
			<li><a href='<?php site_url() ?>/location'>All the different individual Franchise Location Pages</a></li>
		</ul>
	</div>
	<div class='large-4 columns sitemap'>
		<ul> Scheduler
			<li><a href='<?php site_url() ?>/schedule'>Scheduler</a></li>
			<li><a href='<?php site_url() ?>'>Contact Information Page on Scheduler</a></li>
			<li><a href='<?php site_url() ?>'>Medical History Page on Scheduler</a></li>
			<li><a href='<?php site_url() ?>'>Soap notes pages attached to medical history pages</a></li>
		</ul>
	</div>
		<div class='large-4 columns sitemap'>
		<ul> Contract Pages
			<li><a href='<?php site_url() ?>'>Intake form Contract Page</a></li>
			<li><a href='<?php site_url() ?>/customer/signup'>Membership Contract Page</a></li>
		</ul>
	</div>
</div>