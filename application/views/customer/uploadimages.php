<link rel="stylesheet" href="/assets/css/customer.css" />
<div id="customer-uploadimages-section" class="large-12 columns">
    <?php if (isset($uploadimageserrors)) { ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $uploadimageserrors; ?>
            </div>            
        </div>
        <?php
    }
    echo form_open('customer/uploadimages', array("id" => "uploadphotoform"));
    ?>
    <div class="row " style="padding: 10px 0px 10px 0px;">        
        <div class="large-12 columns">
            <h3>Upload Photo and Other Images</h3>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">&nbsp;</div>
    </div>
    <div class="row" style="margin-bottom: 25px;">
        <div class="large-2 columns entry-caption">Upload Profile Photo</div>
        <div class="large-10 columns">
            <input type="hidden" id="profilephotofile-url" name="profilephotofile-url"/>
            <input type="hidden" id="profilephotofile-filename" name="profilephotofile-filename"/>
            <input type="file" id="profilephotofile" name="profilephotofile"/>
            <br/>
            <span id="profilephoto-error" style="display: none; color: red;">&nbsp;</span> 
            <img id="profilephoto-img" src="<?php echo set_value('profilephotofile-url'); ?>" style="width: 300px; height: 300px; border: 1px solid lightgray; display: block;"/>
        </div>
    </div> 
    <div class="row">
        <div class="large-2 columns entry-caption">Upload Proof of Discount Photo</div>
        <div class="large-10 columns">
            <input type="hidden" id="discountphotofile-url" name="discountphotofile-url"/>
            <input type="hidden" id="discountphotofile-filename" name="discountphotofile-filename"/>
            <input type="file" id="discountphotofile" name="discountphotofile"/>
            <br/>
            <p>
                Please provide a photograph of an official document with your membership registered name on it. 
                Proving that you deserve the discount. $2 discount off membership each month If you are 
                a: healthcare practitioner, in the armed services, fire and rescue, or Senior Citizen.
            </p>
            <br/>
            <span id="discountphoto-error" style="display: none; color: red;">&nbsp;</span> 
            <img id="discountphoto-img" src="<?php echo set_value('discountphotofile-url'); ?>" style="width: 800px; height: 500px; border: 1px solid lightgray; display: block;"/>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="large-12 columns">
            <button type="submit">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" onclick="parent.location = '<?php echo site_url() ?>'">Cancel</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        $('#profilephotofile').change(function() {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                if (!$('#profilephotofile').val()) {
                    $("#profilephoto-error").show();
                    $("#profilephoto-error").html("File is empty<br/><br/>");
                    return false;
                }

                var fsize = $('#profilephotofile')[0].files[0].size;
                var ftype = $('#profilephotofile')[0].files[0].type;

                switch (ftype) {
                    case 'image/png':
                    case 'image/gif':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        $("#profilephoto-error").show();
                        $("#profilephoto-error").html("<b>" + ftype + "</b> Unsupported file type!<br/><br/>");
                        return false;
                }

                if (fsize > 1048576) {
                    $("#profilephoto-error").show();
                    $("#profilephoto-error").html("<b>" + fsize + "</b> Too big Image file! Please reduce the size of your photo using an image editor.<br/><br/>");
                    return false;
                }

                $.ajax({
                    url: '<?php echo site_url() ?>uploadimage/uploadprofilephoto',
                    type: 'POST',
                    data: new FormData($('#uploadphotoform')[0]),
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data, textStatus, jqXHR) {
                        $('#profilephoto-img').attr("src", data.url);
                        $('#profilephotofile-url').val(data.url);
                        $('#profilephotofile-filename').val(data.file_name);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert("Upload Error: " + textStatus);
                    }
                });

                $("#profilephoto-error").hide();
                $("#profilephoto-error").html("");
            } else {
                $("#profilephoto-error").show();
                $("#profilephoto-error").html("Please upgrade your browser, because your current browser lacks some new features we need!<br/><br/>");
                return false;
            }
        });

        $('#discountphotofile').change(function() {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                if (!$('#discountphotofile').val()) {
                    $("#discountphoto-error").show();
                    $("#discountphoto-error").html("File is empty<br/><br/>");
                    return false;
                }

                var fsize = $('#discountphotofile')[0].files[0].size;
                var ftype = $('#discountphotofile')[0].files[0].type;

                switch (ftype) {
                    case 'image/png':
                    case 'image/gif':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                        break;
                    default:
                        $("#discountphoto-error").show();
                        $("#discountphoto-error").html("<b>" + ftype + "</b> Unsupported file type!<br/><br/>");
                        return false;
                }

                if (fsize > 1048576) {
                    $("#discountphoto-error").show();
                    $("#discountphoto-error").html("<b>" + fsize + "</b> Too big Image file! Please reduce the size of your photo using an image editor.<br/><br/>");
                    return false;
                }

                $.ajax({
                    url: '<?php echo site_url() ?>uploadimage/uploaddiscountphoto',
                    type: 'POST',
                    data: new FormData($('#uploadphotoform')[0]),
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data, textStatus, jqXHR) {
                        $('#discountphoto-img').attr("src", data.url);
                        $('#discountphotofile-url').val(data.url);
                        $('#discountphotofile-filename').val(data.file_name);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    }
                });

                $("#discountphoto-error").hide();
                $("#discountphoto-error").html("");
            } else {
                $("#discountphoto-error").show();
                $("#discountphoto-error").html("Please upgrade your browser, because your current browser lacks some new features we need!<br/><br/>");
                return false;
            }
        });
    });

</script>