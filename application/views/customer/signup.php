<link rel="stylesheet" href="/assets/css/customer.css" />
<div id="customer-signup-section" class="large-12 columns form-inputs">
    <?php if (isset($signuperrors)) { ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $signuperrors; ?>
            </div>            
        </div>
    <?php
    }
    echo form_open('customer/signup', array('id' => 'customer-signup-form'));
    ?>
    <div class="row">        
        <div class="large-12 columns">
            <h3>Registration</h3>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns">&nbsp;</div>
        <div class="large-3 columns required-field">
            <font size="1">Last Name</font>
        </div>
        <div class="large-4 columns required-field">
            <font size="1">First Name</font>
        </div>
        <div class="large-3 columns">
            <font size="1">Middle Name</font>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">Name</div>
        <div class="large-3 columns">
            <input name="lastname" type="text" value="<?php echo set_value('lastname'); ?>"/>
        </div>
        <div class="large-4 columns">
            <input name="firstname" type="text" value="<?php echo set_value('firstname'); ?>"/>
        </div>
        <div class="large-3 columns">
            <input name="middlename" type="text" value="<?php echo set_value('middlename'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">Address</div>
        <div class="large-10 columns">
            <input name="address_street" type="text" value="<?php echo set_value('address_street'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">City</div>
        <div class="large-2 columns">
            <input name="address_city" type="text" value="<?php echo set_value('address_city'); ?>"/>
        </div>
        <div class="large-1 columns required-field entry-caption">State</div>
        <div class="large-3 columns">
            <select name="address_state" style="padding: 3px; width: 100%;">
                <option value="AL" <?php echo set_select('address_state', 'AL'); ?>>Alabama</option>
                <option value="AK" <?php echo set_select('address_state', 'AK'); ?>>Alaska</option>
                <option value="AZ" <?php echo set_select('address_state', 'AZ'); ?>>Arizona</option>
                <option value="AR" <?php echo set_select('address_state', 'AR'); ?>>Arkansas</option>
                <option value="CA" <?php echo set_select('address_state', 'CA'); ?>>California</option>
                <option value="CO" <?php echo set_select('address_state', 'CO'); ?>>Colorado</option>
                <option value="CT" <?php echo set_select('address_state', 'CT'); ?>>Connecticut</option>
                <option value="DE" <?php echo set_select('address_state', 'DE'); ?>>Delaware</option>
                <option value="DC" <?php echo set_select('address_state', 'DC'); ?>>District Of Columbia</option>
                <option value="FL" <?php echo set_select('address_state', 'FL'); ?>>Florida</option>
                <option value="GA" <?php echo set_select('address_state', 'GA'); ?>>Georgia</option>
                <option value="HI" <?php echo set_select('address_state', 'HI'); ?>>Hawaii</option>
                <option value="ID" <?php echo set_select('address_state', 'ID'); ?>>Idaho</option>
                <option value="IL" <?php echo set_select('address_state', 'IL'); ?>>Illinois</option>
                <option value="IN" <?php echo set_select('address_state', 'IN'); ?>>Indiana</option>
                <option value="IA" <?php echo set_select('address_state', 'IA'); ?>>Iowa</option>
                <option value="KS" <?php echo set_select('address_state', 'KS'); ?>>Kansas</option>
                <option value="KY" <?php echo set_select('address_state', 'KY'); ?>>Kentucky</option>
                <option value="LA" <?php echo set_select('address_state', 'LA'); ?>>Louisiana</option>
                <option value="ME" <?php echo set_select('address_state', 'ME'); ?>>Maine</option>
                <option value="MD" <?php echo set_select('address_state', 'MD'); ?>>Maryland</option>
                <option value="MA" <?php echo set_select('address_state', 'MA'); ?>>Massachusetts</option>
                <option value="MI" <?php echo set_select('address_state', 'MI'); ?>>Michigan</option>
                <option value="MN" <?php echo set_select('address_state', 'MN'); ?>>Minnesota</option>
                <option value="MS" <?php echo set_select('address_state', 'MS'); ?>>Mississippi</option>
                <option value="MO" <?php echo set_select('address_state', 'MO'); ?>>Missouri</option>
                <option value="MT" <?php echo set_select('address_state', 'MT'); ?>>Montana</option>
                <option value="NE" <?php echo set_select('address_state', 'NE'); ?>>Nebraska</option>
                <option value="NV" <?php echo set_select('address_state', 'NV'); ?>>Nevada</option>
                <option value="NH" <?php echo set_select('address_state', 'NH'); ?>>New Hampshire</option>
                <option value="NJ" <?php echo set_select('address_state', 'NJ'); ?>>New Jersey</option>
                <option value="NM" <?php echo set_select('address_state', 'NM'); ?>>New Mexico</option>
                <option value="NY" <?php echo set_select('address_state', 'NY'); ?>>New York</option>
                <option value="NC" <?php echo set_select('address_state', 'NC'); ?>>North Carolina</option>
                <option value="ND" <?php echo set_select('address_state', 'ND'); ?>>North Dakota</option>
                <option value="OH" <?php echo set_select('address_state', 'OH'); ?>>Ohio</option>
                <option value="OK" <?php echo set_select('address_state', 'OK'); ?>>Oklahoma</option>
                <option value="OR" <?php echo set_select('address_state', 'OR'); ?>>Oregon</option>
                <option value="PA" <?php echo set_select('address_state', 'PA'); ?>>Pennsylvania</option>
                <option value="RI" <?php echo set_select('address_state', 'RI'); ?>>Rhode Island</option>
                <option value="SC" <?php echo set_select('address_state', 'SC'); ?>>South Carolina</option>
                <option value="SD" <?php echo set_select('address_state', 'SD'); ?>>South Dakota</option>
                <option value="TN" <?php echo set_select('address_state', 'TN'); ?>>Tennessee</option>
                <option value="TX" <?php echo set_select('address_state', 'TX'); ?>>Texas</option>
                <option value="UT" <?php echo set_select('address_state', 'UT'); ?>>Utah</option>
                <option value="VT" <?php echo set_select('address_state', 'VT'); ?>>Vermont</option>
                <option value="VA" <?php echo set_select('address_state', 'VA'); ?>>Virginia</option>
                <option value="WA" <?php echo set_select('address_state', 'WA'); ?>>Washington</option>
                <option value="WV" <?php echo set_select('address_state', 'WV'); ?>>West Virginia</option>
                <option value="WI" <?php echo set_select('address_state', 'WI'); ?>>Wisconsin</option>
                <option value="WY" <?php echo set_select('address_state', 'WY'); ?>>Wyoming</option>
            </select>
        </div>
        <div class="large-1 columns entry-caption required-field">Zip</div>
        <div class="large-3 columns">
            <input name="address_zip" type="text" value="<?php echo set_value('address_zip'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns entry-caption">&nbsp;</div>
        <div class="large-10 columns">
            <div class="row">
                <div class="large-6 columns"><font size="1">Home</font></div>
                <div class="large-6 columns end"><font size="1">Work</font></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns entry-caption required-field">Contact No.</div>
        <div class="large-10 columns">
            <div class="row">
                <div class="large-6 columns required-field"><input name="contact_home" type="text" value="<?php echo set_value('contact_home'); ?>"/></div>
                <div class="large-6 columns end"><input name="contact_work" type="text" value="<?php echo set_value('contact_work'); ?>"/></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">Email</div>
        <div class="large-10 columns">
            <input name="email" type="text" value="<?php echo set_value('email'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">Date of Birth</div>
        <div class="large-2 columns">
            <input name="birth_date" id="birth_date" type="text" value="<?php echo set_value('birth_date'); ?>" autocomplete="off"/>
        </div>
        <div class="large-1 columns entry-caption">Age</div>
        <div class="large-3 columns">
            <input name="age" type="text" value="<?php echo set_value('age'); ?>"/>
        </div>
        <div class="large-1 columns entry-caption">Gender</div>
        <div class="large-3 columns">
            <select name="gender" style="padding: 3px;">
                <option value="0" <?php echo set_select('gender', '0'); ?>>Male</option>
                <option value="1" <?php echo set_select('gender', '1'); ?>>Female</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns entry-caption">Height</div>
        <div class="large-2 columns">
            <input name="height" type="text" value="<?php echo set_value('height'); ?>" style="width: 60%; display: inline-block;"/>
            <font size="2">ft./inches</font>
        </div>
        <div class="large-1 columns entry-caption">Weight</div>
        <div class="large-2 columns">
            <input name="weight" type="text" value="<?php echo set_value('weight'); ?>" style="width: 75%; display: inline-block;"/>
            <font size="2">lbs</font>
        </div>
        <div class="large-1 columns entry-caption">Marital Status</div>
        <div class="large-2 columns">
            <select name="civil_status" style="padding: 3px;">
                <option value="0" <?php echo set_select('civil_status', '0'); ?>>Single</option>
                <option value="1" <?php echo set_select('civil_status', '1'); ?>>Married</option>
                <option value="2" <?php echo set_select('civil_status', '2'); ?>>Widowed</option>
                <option value="3" <?php echo set_select('civil_status', '3'); ?>>Devorsed</option>
            </select>
        </div>
        <div class="large-1 columns entry-caption">No. of Children</div>
        <div class="large-1 columns">
            <input name="children_count" type="text" value="<?php echo set_value('children_count'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns entry-caption">Occupation</div>
        <div class="large-10 columns">
            <input name="occupation" type="text" value="<?php echo set_value('occupation'); ?>"/>
        </div>
    </div>
    <div class="row " style="padding: 10px 0px 10px 0px;">        
        <div class="large-12 columns">
            <h4>Account Information</h4>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">Login ID (Email)</div>
        <div class="large-10 columns">
            <input name="loginid" type="text" value="<?php echo set_value('loginid'); ?>" style="width: 50%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field entry-caption">Password</div>
        <div class="large-10 columns">
            <input name="password" type="password" value="<?php echo set_value('password'); ?>" autocomplete="off" style="width: 50%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns entry-caption required-field">Re-type Password</div>
        <div class="large-10 columns">
            <input name="retypepassword" type="password" autocomplete="off" style="width: 50%;"/>
        </div>
    </div>

    <div class="row">
        <div class="large-12 columns">
            <button id="signup-submit" type="button">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" onclick="parent.location = '<?php echo site_url() ?>'">Cancel</button>
        </div>
    </div>
<?php echo form_close(); ?>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        dateStartValue=new Date();
        dateCurrentYear = dateStartValue.getFullYear();
        dateStartValue=dateStartValue.getFullYear()-105;
        new Pikaday({
            field: document.getElementById('birth_date'),
            firstDay: 1,
            format: 'L',
            setDefaultDate: true,
            minDate: new Date(dateStartValue+'-01-01'),
            maxDate: new Date('2050-12-31'),
            yearRange: [dateStartValue, dateCurrentYear]
        });

        $('#signup-submit').click(function() {
            $('#customer-signup-form').submit();
        });

    });

</script>