
<img class='frame_img img-responsive' src='<?php site_url() ?>/assets/img/pricing/main.jpg'>
<h1>
Pricing
</h1>
<p>
Our prices are the most competitive in the nation and we are proud to be able to compete with the largest and least expensive massage and spa franchises; All the while giving you the finest massage experience possible. You will find that our rates are some of the lowest in your area, with or without a membership.<br/><br/>

<b>If you buy 10 in-house massages, you receive VIP status and one free in-house massage.<br/><br/></b>

<b>If you buy 10 out-call massages, you receive VIP status and one free out-call massage.<br/><br/></b>

We are not your stereotypical massage parlor. In fact, we try to break all of the massage molds. At Advanced Massage Network, we believe the massage industry is in a horrible state at the moment, for clients and therapists. It is a hit-or-miss for clients and talented therapists are paid well below their worth, forcing them to leave the industry or go out on their own. We practice all massage modalities and specialize in the most advanced modalities available, including osteopathic soft tissue manipulation and medical massage. We promise you the highest quality massage therapy and results you will find.<br/><br/>

<b>Introductory pricing (For the first time visit.):  $59.99 for a 60 Minute intro session. $89.99 for a 90 Minute intro session.<br/><br/></b>

<b>Membership dues:  $60 in advanced, or $7 (each month) for a 12 month under contract.<br/><br/></b>

The new discount for military, senior citizens, and health care practitioners is $2 off each months dues. Making their dues $5 for 12 months. The discount off paying the membership in advanced for these individuals is $10. If these individuals pay in advance they pay $55 for membership.<br/><br/>

<b>Membership rates for a session: $65 for a 60 min.; $95 for a 90 min.; and $125 for a 2hr.<br/><br/></b>

Book another appointment the same day, or one day afterwards your session, and receive an additional $2 off your next appointment.<br/><br/>

<b>Non-membership rates: $84 for a 60 min.; $120 for a 90 min.; and $150 for a 2hr.</b>
</p>
