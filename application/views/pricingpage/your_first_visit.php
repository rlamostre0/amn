
<img class='frame_img img-responsive' src='<?php site_url() ?>/assets/img/pricing/main.jpg'>
</img>
<h1>
Your First Visit
</h1>
<p>
If this is your first massage experience it absolutely will be a great one. You are lucky if your first massage experience is with us, because you could not have picked any better. Our massage therapists are absolutely the best in your city. We can afford to recruit the top therapists because we pay our therapist better than the rest. We encourage you to use our online scheduler to book your first appointment. However, if you are not computer savvy, we will gladly talk you through the process by calling our customer service hotline. We also encourage you to use our problems and solutions area where we assess your structural issues and determine the appropriate modality to treat your structural problem. If you join as a member, we present you with many perks such as competitive rates not found anywhere at this advanced level of massage and VIP points which allow you to earn free massage sessions.</p>
