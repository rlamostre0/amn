<!--<!DOCTYPE html>
<html>
<head>
	<title>Advanced Massage Network</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='stylesheet' href='css/foundation.css' type='text/css' />
	<link rel='stylesheet' href='css/normalize.css' type='text/css' />
	<link rel='stylesheet' href='style.css' type='text/css' />
	<script src='js/jquery-1.11.2.min.js'></script>
	<script src='js/dropdown.js'></script>
	<script src='js/ajax.js'></script>
</head>
<body>
	<div class='fixed'>
	<nav class='top-bar' data-topbar role='navigation'>
		<ul class='title-area'>
			<li class='name'>
				<h1><a href='http://www.advancedmassagenetwork.com'><img src='img/logo.png'></img>Advanced Massage Network</a></h1>
			</li>
		</ul>
		<section>
			<ul class='right email'>
				<li>Info@advancedmassagenetwork.com</li>
			</ul>
			<ul class='right p_number'>
				<li>1-844-ADV-MASS</li>
			</ul>
		</section>
	</nav>
</div>
-->
<!--
<div class='container amn_head'>
	<div class='row'>
		<div class='amn_banner_con small-12 columns'>
			<div class='medium-9 small-4 columns'>
			<a href="http://advancedmassagepractitioner.com/"><p class='head_button'>Welcome</p></a>
			<a href="http://advancedmassagepractitioner.com/pages/modalities"><p class='head_button'>Massage Modalities</p></a>
			<a href="http://advancedmassagepractitioner.com/location"><p class='head_button'>Locations</p></a>
			<a href="http://advancedmassagepractitioner.com/pages/pricing"><p class='head_button'>Pricing and Membership</p></a>
			<a href="http://advancedmassagepractitioner.com/pages/gift"><p class='head_button'>Gift Certificates</p></a>
			<a href="http://advancedmassagepractitioner.com/pages/first_visit"><p class='head_button'>Your First Visit</p></a>
			<a href="http://advancedmassagepractitioner.com/testimonial"><p class='head_button'>Testimonials</p></a>
			<a href="http://advancedmassagepractitioner.com/pages/faq"><p class='head_button'>FAQs</p></a>
			<a href="http://advancedmassagepractitioner.com/blog"><p class='head_button'>Blog</p></a>
			<a href=""><p class='head_button'>Search Symptom by Diagnosis</p></a>
			<a href="http://advancedmassagepractitioner.com/aboutus"><p class='head_button'>About Us</p></a>
			</div>
			
			<div class='medium-3 small-10 columns login'>
				<div class='row'>
					<div class='login_head small-12 columns'>
						<small>
						<a href=''>Book Now!</a>
						<a href=''>Join Now!</a>
						<a href=''>Contact Us</a>
						</small>
					</div>
				</div>
				<div class='row' style='padding:16px'>
					<input type='text' placeholder='username' class='medium-12 columns'></input>
					<input type='password' placeholder='password' class='medium-12 columns'></input>
					<a class='button tiny signup_btn'>sign up</a>
					<a class='button tiny login_btn'>login</a>		
				</div>
			</div>
		
	</div>
</div>
-->
<link rel="stylesheet" href="/assets/css/pricing-style.css" />
<script src="/assets/js/ajax.js"></script>
<script src="/assets/js/dropdown.js"></script>
<div class='container pricing-page'>
	<div class='row body'>
		<div class='medium-4 small-12 columns'>
			<ul class='sidebar small-10'>
				<li class='title cost_and_perks'>Cost & Perks</li>
				<div class='drop cost_and_perks'>
				<li>Pricing</li>
				<li>Membership</li>
				<li>Chair Massage</li>
				<li>Become a Chair Massage Affiliate</li>
				<li>VIP Point System</li>
				<li class='first_pop'>Discounts</li>
				<li>Health Insurance</li>
				<li>VIP Point System</li>
				</div>
				<li class='title finding'>Finding The Right Therapist</li>
				<div class='drop finding'>
				<li>Your First Visit</li>
				<li>Modalities</li>
				<li>Problems and Solutions</li>
				<li>Testimonial</li>
				<li>FAQs</li>
				</div>
				<li class='title more_about_us'>More About Us</li>
				<div class='drop more_about_us'>
				<li>About Us</li>
				<li>Terms of Use</li>
				<li>Privacy Policy</li>
				<li>Contact Us</li>
			</div>
				<li class='title press_room'>Press Room</li>
				<div class='drop press_room'>
				<a href='amn_in_the_news.php' target='frame'><li>AMN in the News</li></a>
				<a href='press_release.php' target='frame'><li>Press Release</li></a>
				<a href='video_commercials.php' target='frame'><li>Video Commercials</li></a>
				<a href='blog.php' target='frame'><li>Blog</li></a>
				</div>
				<li class='title franchise_opportunities'>Franchise Opportunities</li>
				<div class='drop franchise_opportunities'>
				<li>Therapist</li>
				<li>Health Care Practitioners</li>
			</div>
			</ul>
		</div>
		<div class='medium-8 small-12 columns' id='frame'></div>
		</div>

</div>