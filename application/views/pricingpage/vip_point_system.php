
<img class='frame_img img-responsive' src='<?php site_url() ?>/assets/img/pricing/main.jpg'>
<h1>
The VIP point system
</h1>
<p>
is a way to earn free massage sessions. When you purchase sessions or complete enough participation points through our website, you will receive a free massage. Check out the link in our control panel today to begin earning!

If you earn 1500 points, you receive a free in-house massage. 2500 points will earn you one out-call massage. If you purchase 10 in-house massages, you immediately receive VIP status and a free massage. If you purchase 10 out-call massages, you receive VIP status and a free out-call massage. If you save up your points and reach a certain number you will earn a very special surprise!
</p>
