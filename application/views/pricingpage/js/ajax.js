$(document).ready(function(){


	// LOAD ON STARTUP
	$('#frame').load('about_us.php')


	// LOAD ON CLICK
  $("li").click(function(){
  	var link = $(this).text()+".php";
  	
  	link = link.replace(/ /g,'_');
        link = link.toLowerCase();
    $( "#frame" ).load(link);
  });

  $(window).scroll(function(){

  	var height = $(window).scrollTop();

  	if(height > 200){
  		$('nav').css('height','calc(100% + 20px)').css('padding','10px');
      $('nav').find('li').css('font-size','16px');
  	}
  	if(height < 200){
  		$('nav').css('height','calc(100% - 20px').css('padding','-10px');
      $('nav').find('li').css('font-size','12px');
  	}

  })
});