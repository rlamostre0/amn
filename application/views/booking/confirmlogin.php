<div class="row" style="margin-top: 10px;"><br><br>
	<div class="large-12 columns">
		<div class="large-6 large-centered columns">
			<div class="panel callout radius" style="height:400px;"> 
				<div class="large-12 columns">
					<center>
					<h3>Login to Advanced Massage Network</h3>
					</center>
				</div>
				<?php echo form_open('customer/login', array('id' => 'login-form', 'style' => 'margin: 0px;')); ?>
				<div class="row">
					<div class="large-12 columns">
							<input type="text" id="customer-login" name="customerloginid"/>
					</div>
				</div> 
				<div class="row">
					<div class="large-12 columns">
							<input type="password" id="customer-password" name="customerpassword"/>
					</div>
				</div> 
				<div class="row">
						<center><input type="submit" value="Login" class="medium white nice button radius"/></center>
					<div class="large-12 columns">
					</div>
				</div>                    
				<?php echo form_close(); ?>
				<div class="row">
					<div class="large-6 large-centered columns">
						<div class="panel callout radius"> 
							<center>* Main Member must login to confirm adding the guest account. <br><br>* Guest accounts are only limited up to 3 persons.</center>
						</div>
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>