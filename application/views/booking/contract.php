
<form id="submitForm" action="<?php echo '/schedule/save'; ?>" method="POST">

<div class="row" style="margin-top: 10px;"><br><br>
	<div class="large-8 large-centered columns">
		<div class="row">
			<div class="large-10 columns contract">
				<h3>STANDARD MASSAGE WAVIER</h3>
<p>
HEALTHY HISTORIES AND CONTACT INFORMATION ARE PROTECTED AND ARE SOLELY THE PROPERTY OF ADVANCED MASSAGE NETWORK, 

LLC. THE ADVANCED MASSAGE NETWORK THERAPIST AND THE CLIENT UNLESS OTHERWISE AUTHORIZED.<br/><br/>

PLEASE TAKE A MOMENT TO READ THE FOLLOWING INFORMAITON AND SIGN WHERE INDICATED IN RED.<br/><br/>

It is extremely important that you inform Advanced Massage Network, LLC. (AMN) therapists that perform services for you of any contagious 

pathology that you may have or if you are undergoing Chemotherapy. In addition, please inform your network therapist of any open lesions or 

abrasions to prevent any transfer of infection. This does not necessarily contraindicate you for massage. It only means that your therapist may 

have to use gloves during the session, which will not diminish the affect of the treatment. It is Advanced Massage Network responsibility to keep 

Advanced Massage Network therapists and you both safe and protected for the best possible therapy.<br/><br/>

Advanced Massage Network locations are professional environments and our therapists are medically based massage therapists. Should there be 

any inappropriate behavior noted in during the treatment session, the therapist and Advanced Massage Network has the right to cancel the 

session and demand full payment. <br/><br/>

I affirm that the information in my contact information form and medical history form is complete and accurate, and I answered all questions 

honestly. I agree to keep Advanced Massage Network and my Advanced Massage Network therapist updated and informed as to any changes in 

my medical profile as they occur and complete an amendment to my existing medical / health history in the Advanced Massage Network’s secure 

data base. It is my responsibility to explain and discuss all physical conditions with the Advanced Massage Network therapist, so that they may do 

their job. I am responsible to notify Advanced Massage Network within three (3) days of my massage if I notice any increase in pain or numbness.<br/><br/>

I understand that payment is due at the time of treatment unless other arrangements have been made. I am responsible for payment if third party 

payment is not made, and that a $30 fee will be assessed for a retuned (non-sufficient funds) check. I agree to give 24 hours notice of cancellation 

of a scheduled appointment. If less than 24 hours is given, I agree that Advanced Massage Network, LLC. may charge me $45 for the time of the 

missed session. I also agree that Advanced Massage Network, LLC may charge the credit card that was given by me to initially secure the 

appointment time, and to keep this credit card on file for future appointments made by me. <br/><br/>

<p class='und'>ONCE MORE I UNDERSTAND THAT IF I CANCEL ANY APPOINTMENT WITH ADVANCED MASSAGE NETWORK, LLC. WITH LESS THAN TWENTY-

FOUR (24) HOUR’S NOTICE, I AM RESPONSIBLE TO PAY FOR THE TIME WHICH WAS SCHEDULED WITH ADVANCED MASSAGE NETWORK, LLC. .<br/><br/></p>
			
		<!--<div class="row">
			<div class="large-6 large-centered columns">
				<label>If you have read the contract<br>Please initial here
					<input id="contract" name="contract" type="text" placeholder="initial" style="background:#FF1919;color: #ffffff;" >
				</label>
			</div>
		</div>-->
		<div class="row">
			<div class="large-6 large-centered columns">
				<label>If you have read the details of payment<br>Please initial here 
					<input id="payment" name="payment" type="text" placeholder="initial" style="background:#FF1919;color: #ffffff;" >
				</label>
			</div>
		</div>
		<br/><br/><p>
		I understand that neither Advanced Massage Network, LLC. , or the owner of leased office space will be liable for any injuries or loss sustained to 

myself or to my property while on the premises. I release Advanced Massage Network, LLC, Advanced Massage Network, LLC. Franchise Owners, 

and the Advanced Massage Network therapists of any liability for any unforeseen injuries, which may occur on the premises.<br/><br/>

In addition, the therapists of Advanced Massage Network, LLC disclaim responsibility for any injury sustained while performing exercises or 

stretches suggested to me. I will not begin exercises or stretches without first consulting my physician if under one’s care. <br/><br/>

TO CLIENT: Your personal medical and health information will not be disclosed outside authorized Administration of Advanced Massage 

Network, LLC. , and the Advanced Massage Network, LLC. therapist that provided therapy without written consent given by you after a request 

has been made. In addition, any authorization may be revoked in writing unless I have taken any action in reliance on the authorization. <br/><br/>

Advanced Massage Network therapists are Nationally Certified and Licensed massage therapists that may suggest types of treatment, exercises 

and stretches as part of your home care program. Advanced Massage Network therapists are not physicians that can diagnose. Our therapists are 

only authorized within their scope of practice to give you their professional opinion from their extended medical background.<br/><br/>	

Advanced Massage Network therapists are prepared to speak with your physician at your request in regard to your treatment plan. You only need 

to notify Advanced Massage Network that he/she may be calling and the name of the physician who is calling after you have filled out a release 

form.</p><br/><br/>
<p class='und'>ONCE MORE THE ADVANCED MASSAGE NETWORK THERAPIST, FRANCHISE OWNER, OTHER HEATHCARE PRACTITIONERS AT THE LOCATION 

OF THE APPOINTMENT: ARE NOT LIABLE FOR ANY INJURY OR LOSS ON THE PREMISES OF ADVANCED MASSAGE NETWORK, LLC. FRANCHISE 

LOCATIONS DURING OR AROUND THE TIME OF THE APPOINMENT SCHEDULED.</p><br/><br/>
</div>
		</div>
		<div class="row">
			<?php 
				$info = $this->session->userdata('scheduleData')['intakeform'];
			?>
			<div class="large-6 large-centered columns">
				<label>Name
					<input type="text" placeholder="<?php echo $info['name']; ?>" style="border: 1px solid #f00;" disabled >
				</label>
			</div>
		</div>
		<div class="row">
			<div class="large-6 large-centered columns">
				<label>Address
					<textarea placeholder="large-12.columns" rows="6" style="border: 1px solid #f00;" disabled ><?php 
					echo $info['address_street']."\n".$info['address_city']."\n".$info['address_state']."\n".$info['address_zip']."\n"; 
					?></textarea>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="large-6 large-centered columns">
				<p style="font-size:12px;">If all the information is true and currently up to date in your Contact Information, 
				Medical History, and you agree to our Company's Policies please check the box to proceed.</p>
				<center>
					<p style="font-size:12px;"><input id="finalize" type="checkbox" name="agreed_on_contract"> <strong>FINALIZED and BOOK NOW</strong></p>
					<input type="hidden" name="schedulePost" id="schedulePost" value="contract">
					<input type="hidden" name="payment" id="paymentMode" value="">
					<a id="proceed" class="button radius" href="<?php echo '/booking/payment'; ?>">Proceed <i class="fi-arrow-right small"></i></a>
				</center>
			</div>
		</div>
	</div>
</div>
</form>
<div id="myModal" class="reveal-modal tiny" style="margin-top: -17em;" data-reveal>
  <div class="row">
		<div class="large-12 large-centered columns">
			<h2 id="modalTitle">Payment</h2>
		</div>
	</div>
  <div id="modalBody">
		<div class="row">
			<div class="large-12 large-centered columns">
				<p>All appointments must be secured when scheduled with a credit card, or gift card on file.
				If you would prefer to use a gift card to secure this appointment with please call our operator at <strong>1-855-238-6277</strong>.</p>
			</div>
		</div><hr></br>
		<div class="row">
			<div class="large-12 columns">
			<center>
				<a class="button radius btnPayment" style="font-size: 12px;" href="<?php echo site_url()."booking/payment"; ?>"><span style="font-size: 14px;">Prepay for the Appointment</span></a>
				<br><br><p style="font-size: 12px; color: red; margin-top: -2.75em;">Prepay for the appointment and receive bonus VIP Points.</p>
				
				<a class="button radius btnPayment" href="<?php echo site_url()."booking/payment" ?>" data-payment="paylater"><span style="font-size: 14px;">Pay the Day of the Appointment</span></a>
				<div class="row">
					<div class="large-2 columns" style="margin-left: 0em;">
						<input type="checkbox">
					</div>
					<div class="large-11 columns">
						<p style="font-size: 12px; color: red; margin-top: -2.75em;">I would prefer to pay at the appointment with a different Credit Card or Cash.</p>
					</div>
				</div>
			</center>
			</div>
		</div>
	</div>
  <a class="close-reveal-modal">&#215;</a>
</div>
<script>
$(function() {
	var payment="";
	$( "body" ).on( "click", "#proceed", function(e) {
		if($("#contract").val()==""){
			e.preventDefault();
			alert("Have you read the contract?");
			$("html, body").animate({ scrollTop: $('#contract').offset().top }, 1000);
		}else if($("#payment").val()==""){
			e.preventDefault();	
			alert("Have you read the details of payment?");
			$("html, body").animate({ scrollTop: $('#payment').offset().top }, 1000);
		}else if(!($("#finalize").is(':checked'))){
			e.preventDefault();
			alert("Customer details has not yet been finalized");
			$("html, body").animate({ scrollTop: $('#finalize').offset().top }, 1000);
		}else if(payment!=""){
			e.preventDefault();
			$('#paymentMode').val(payment);
			$('#submitForm').submit();
		}else{
			e.preventDefault();
			$('#myModal').foundation('reveal', 'open');
		}
	});
	$( "body" ).on( "click", ".btnPayment", function(e) {
		e.preventDefault();
		payment=$(this).attr('data-payment');
		$('#proceed').trigger("click");
	});
});
</script>