<div class="row" style="margin-top: 10px;"><br><br>
	<div class="large-12 columns">
		<div class="large-6 columns">
			<div class="panel callout radius" style="height:400px;"> 
				<div class="large-12 columns">
					<center>
					<h3>Login to Advanced Massage Network</h3>
					</center>
				</div>
				<?php echo form_open('customer/login', array('id' => 'login-form', 'style' => 'margin: 0px;')); ?>
				<div class="row">
					<div class="large-12 columns">
							<input type="text" id="customer-login" name="customerloginid"/>
					</div>
				</div> 
				<div class="row">
					<div class="large-12 columns">
							<input type="password" id="customer-password" name="customerpassword"/>
					</div>
				</div> 
				<div class="row">
						<center><input type="submit" value="Login" class="medium white nice button radius"/></center>
					<div class="large-12 columns">
					</div>
				</div>                    
				<?php echo form_close(); ?>
				<div class="row">
					<div class="large-6 large-centered columns">
						<div class="panel callout radius"> 
							<center>Log in to proceed or you can skip <a href="<?php echo site_url()."booking/basic/$franchise_no/skiplogin"; ?>">here</a>.</center>
						</div>
					</div>
				</div>					
			</div>
		</div>
		<div class="large-6 columns">
			<div class="panel callout radius"> 
				<center>
					<p>Not yet a member? Sign up now for a membership with your intro session and receive points towards a free session today.</p>
					<p><a class="medium white nice button radius" href="<?php echo site_url().'customer/signup' ?>" style="padding:10px;">Signup here</a></p>
				</center>
			</div>
			<div class="panel callout radius"> 
				<center><h3>Family and Friend's </h3><h5>Guest Membership Sign up</h5>
					<a class="medium white nice button radius" href="<?php echo site_url()."booking/confirm_guest"; ?>" style="padding:10px;">Proceed here</a>
				</center>
			</div>
		</div>
	</div>
</div>