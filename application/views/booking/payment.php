<div class="row" style="margin-top: 10px;"><br><br>
	<div class="large-8 large-centered columns">
		<h3>Payment</h3>
		<div class="row">
			<?php if (!empty($message) || $error): ?>
				<div class="large-12 columns">
					<hr>
					<h4>Error</h4>
					
					<div data-alert class="alert-box alert">
						<?php echo $message; ?>
					</div>
				</div>
			<?php endif; ?>
			<form method="post" action="/booking/payment">
				<div class="large-12 columns">
					<hr>
					<h4>Account Details</h4>
				</div>
				<div class="large-6 columns">
					<label>First Name
						<input type="text" placeholder="First Name" name="firstName" value="<?php echo $formData['firstName']; ?>" value="<?php echo $formData['firstName']; ?>" required/>
					</label>
				</div>
				<div class="large-6 columns">
					<label>Last Name
						<input type="text" placeholder="Last Name" name="lastName" value="<?php echo $formData['lastName']; ?>" value="<?php echo $formData['firstName']; ?>" required/>
					</label>
				</div>

				<div class="large-9 columns">
					<label>Billing Address
						<input type="text" placeholder="Billing Address" name="billingAddress1" value="<?php echo $formData['billingAddress1']; ?>" required/>
					</label>
				</div>
				<div class="large-3 columns">
					<label>City
						<input type="text" placeholder="City" name="billingCity" value="<?php echo $formData['billingCity']; ?>" required/>
					</label>
				</div>

				<div class="large-3 columns">
					<label>State
						<?php 
						$states = array(
							'AL'=>'Alabama',
							'AK'=>'Alaska',
							'AZ'=>'Arizona',
							'AR'=>'Arkansas',
							'CA'=>'California',
							'CO'=>'Colorado',
							'CT'=>'Connecticut',
							'DE'=>'Delaware',
							'DC'=>'District of Columbia',
							'FL'=>'Florida',
							'GA'=>'Georgia',
							'HI'=>'Hawaii',
							'ID'=>'Idaho',
							'IL'=>'Illinois',
							'IN'=>'Indiana',
							'IA'=>'Iowa',
							'KS'=>'Kansas',
							'KY'=>'Kentucky',
							'LA'=>'Louisiana',
							'ME'=>'Maine',
							'MD'=>'Maryland',
							'MA'=>'Massachusetts',
							'MI'=>'Michigan',
							'MN'=>'Minnesota',
							'MS'=>'Mississippi',
							'MO'=>'Missouri',
							'MT'=>'Montana',
							'NE'=>'Nebraska',
							'NV'=>'Nevada',
							'NH'=>'New Hampshire',
							'NJ'=>'New Jersey',
							'NM'=>'New Mexico',
							'NY'=>'New York',
							'NC'=>'North Carolina',
							'ND'=>'North Dakota',
							'OH'=>'Ohio',
							'OK'=>'Oklahoma',
							'OR'=>'Oregon',
							'PA'=>'Pennsylvania',
							'RI'=>'Rhode Island',
							'SC'=>'South Carolina',
							'SD'=>'South Dakota',
							'TN'=>'Tennessee',
							'TX'=>'Texas',
							'UT'=>'Utah',
							'VT'=>'Vermont',
							'VA'=>'Virginia',
							'WA'=>'Washington',
							'WV'=>'West Virginia',
							'WI'=>'Wisconsin',
							'WY'=>'Wyoming',
						);
						?>
						<select name="billingState">
							<?php foreach($states as $state): ?>
								<option value="<?php echo $state; ?>"><?php echo $state; ?></option>
							<?php endforeach; ?>
						</select>
					</label>
				</div>
				<div class="large-3 columns">
					<label>Province (Optional)
						<input type="text" placeholder="Province" name="province" value="<?php echo $formData['province']; ?>" />
					</label>
				</div>
				<div class="large-3 columns">
					<label>Zip/Post Code
						<input type="text" placeholder="Zip/Post Code" name="billingPostcode" value="<?php echo $formData['billingPostcode']; ?>" required/>
					</label>
				</div>
				<div class="large-3 columns">
					<label>Country
						<input type="text" placeholder="Country" name="billingCountry" value="<?php echo $formData['billingCountry']; ?>" required/>
					</label>
				</div>

				<div class="large-6 columns">
					<label>E-mail Address
						<input type="email" placeholder="E-mail Address" name="email" value="<?php echo $formData['email']; ?>" required/>
					</label>
				</div>
				<div class="large-6 columns">
					<label>Phone Number
						<input type="text" placeholder="Phone Number" name="billingPhone" value="<?php echo $formData['billingPhone']; ?>" required/>
					</label>
				</div>

				<div class="large-12 columns">
					<hr>
					<h4>Card Details</h4>
				</div>

				<div class="large-6 columns">
					<label>Card Number
						<input type="text" placeholder="Card Number" name="number" value="<?php echo $formData['number']; ?>" required/>
					</label>
				</div>
				<div class="large-3 columns">
					<label>CVV Number
						<input type="text" placeholder="CVV Number" name="cvv" value="<?php echo $formData['cvv']; ?>" required/>
					</label>
				</div>
				<div class="large-3 columns">
					<?php 
					$types = array('MasterCard', 'Visa', 'Discover', 'Amex', 'Diners Club', 'JCB', 'Switch', 'Solo', 'Dankort', 'Maestro', 'Forbrugsforeningen', 'Laser');
					?>
					<label>Type
						<select name="type">
							<?php foreach($types as $type): ?>
							<option value="<?php echo $type; ?>"><?php echo $type; ?></option>
							<?php endforeach; ?>
						</select>
					</label>
				</div>

				<div class="large-4 columns">
					<label>Issue Number
						<input type="text" placeholder="Issue Number" name="issueNumber" value="<?php echo $formData['issueNumber']; ?>" required/>
					</label>
				</div>
				<div class="large-4 columns">
					<?php 
						$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
					?>
					<label>Start Date
						<div class="row">
							<div class="small-6 columns" style="padding: 0em">
								<select name="startMonth">
									<?php $i = 1; ?>
									<?php foreach($months as $month): ?>
										<option value="<?php echo $i; ?>"><?php echo $month; ?></option>
										<?php $i++; ?>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="small-6 columns">
								<select name="startYear">
									<?php for($i=1999; $i<2040; $i++): ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
								</select>
							</div>
						</div>
					</label>
				</div>
				<div class="large-4 columns">
					<label>Expiry Date
						<div class="row">
							<div class="small-6 columns" style="padding: 0em">
								<select name="expiryMonth">
									<?php $i = 1; ?>
									<?php foreach($months as $month): ?>
										<option value="<?php echo $i; ?>"><?php echo $month; ?></option>
										<?php $i++; ?>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="small-6 columns">
								<select name="expiryYear">
									<?php for($i=1999; $i<2040; $i++): ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
								</select>
							</div>
						</div>
					</label>
				</div>

				<div class="large-4 columns">
					<label>Amount
						<input type="text" name="amount" value="35.00" readonly/>
						<input type="hidden" name="currency" value="USD">
					</label>
				</div>

				<div class="large-12 columns">
					<hr>
				</div>

				<div class="large-12 columns">
		            <button type="submit" name="submit" value="submit">Save and Continue</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
$(function() {
	// $( "body" ).on( "click", "#proceed", function(e) {});
	
});
</script>