 

<div class='container top'>
	<div clas='span2 columns'>
	<ul class='breadcrumb'>
		<li>
			Clinic
		</li>
		/
		<li>
			Scheduler
		</li>
	</ul>

		<div clas='span2 columns'>
		<div class='page-header users-header'><h2>Subjective, Objective, Assessment, and Plan</h2></div>	
		
			<div class='well span5' id='gender'>
				<!-- <select multiple='multiple'>
					<option>option 1</option>
					<option>option 1</option>
					<option>option 1</option>
					<option>option 1</option>
				</select> -->
				<ul class='nav nav-tabs'>
					<li class='active'><a href='#male' data-toggle='male'>Male</a></li>
					<li><a href='#female' data-toggle='female'>Female</a></li>
				</ul>
				<div class='tab-content'>
					<div class='tab-pane active' id='male'>
						<div id="human-body-map"></div>
					</div>
					<div class='tab-pane' id='female'>Female</div>
				</div>
			</div>

			


			<div class='well span5 with-banner'>
				<img></img>
                <?php

                foreach($customers as $c){

                ?>
				<h3>Client Information</h3><br/>
				<p>Clients Name: <?php echo $c['firstname'].' '.$c['middlename'].' '.$c['lastname'];?></p>
				<p>Attending Therapist: Ronn Lamostre</p><br/>
				<p>Date of therapy</p><input class='date-of-therapy' data-date-format='mm/dd/yyyy'></input>
				<p>Date of current</p><input class='date-of-current'></input>
				<h3>SOAP Information</h3><br/>

				<!-- ACCORDION STARTS HERE -->

				<div class='accordion' id='accordion2'>
					<div class='accordion-group'>
						<div class='accordion-heading'>
							<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#acc1'>
								<h5>Subjective</h5>
							</a>
						</div>
						<div id='acc1' class='accordion-body collapse in'>
							<div class='accordion-inner'>
								<p> Stuff about subjective </p>
							</div>
						</div>
					</div>
				</div>

				<div class='accordion' id='accordion3'>
					<div class='accordion-group'>
						<div class='accordion-heading'>
							<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion3' href='#acc2'>
								<h5>Objective</h5>
							</a>
						</div>
						<div id='acc2' class='accordion-body collapse in'>
							<div class='accordion-inner'>
								<p> Stuff about objectives </p>
							</div>
						</div>
					</div>
				</div>

				<div class='accordion' id='accordion4'>
					<div class='accordion-group'>
						<div class='accordion-heading'>
							<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion4' href='#acc3'>
								<h5>Assessment</h5>
							</a>
						</div>
						<div id='acc3' class='accordion-body collapse in'>
							<div class='accordion-inner'>
								<p> Stuff about objectives </p>
							</div>
						</div>
					</div>
				</div>

				<div class='accordion' id='accordion5'>
					<div class='accordion-group'>
						<div class='accordion-heading'>
							<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion5' href='#acc4'>
								<h5>Plan</h5>
							</a>
						</div>
						<div id='acc4' class='accordion-body collapse in'>
							<div class='accordion-inner'>
								<p> Stuff about objectives </p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div id="pain-description" style="display: none;">
        <table>
            <tr>
                <td width="20%">Modality:</td>
                <td width="80%">
                    <select id="modality-selection" name="modality-selection">
                        <option value="0"> Medical Massage / Remedial Massage</option>
                        <option value="1"> Spa / Swedish / Deep Tissue</option>
                        <option value="2"> Fascia Manipulation / Myofascial Release</option>
                        <option value="3"> Trigger Point Therapy / Neuromuscular Therapy</option>
                        <option value="4"> Lymphatic Massage / Lymphatic Drainage</option>
                        <option value="5"> Energy Work / Reiki</option>
                        <option value="6"> Eastern Medicine massage</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="modality-date" name="modality-date" type="text"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button id="save" class="modality-selection-button">Save</button>&nbsp;&nbsp;
                    <button id="discard" class="modality-selection-button">Discard</button>&nbsp;&nbsp;
                    <button id="delete" class="modality-selection-button" type="submit">Delete</button>
                </td>
            </tr>
        </table>
    </div>
</div>

</div>
<div class='row'>

</div>
</div>
<?php

}

?>
<script>
	$('#gender a').click(function(e){
		e.preventDefault();
		$(this).tab('show');
	});
    var locations = [];
    var lcount = 0;

    var isDisplayed = false;
    var humanFrontLocs = [[111, 0], [158, 0], [168, 50], [158, 92], [239, 140], [263, 334], [223, 342], [233, 290], [197, 204], [204, 366],
        [206, 595], [159, 591], [134, 361], [101, 594], [59, 591], [59, 361], [73, 205], [38, 334], [0, 338], [34, 131],
        [109, 93], [95, 40], [111, 0]];
    var humanBackLocs = [[408, 0], [442, 26], [437, 79], [511, 119], [550, 345], [512, 346], [481, 199], [458, 235], [476, 378], [475, 592],
        [436, 597], [407, 366], [380, 599], [335, 596], [338, 354], [367, 223], [340, 196], [304, 347], [270, 344], [279, 219], [312, 111],
        [385, 76], [375, 21], [408, 0]];

    var markedIcons = [false, false, false, false, false, false, false];
    var selectedMarker = null;

        $('[id^="marker-icon-"]').hover(function () {
            if (!isDisplayed) {
                var id = $(this).attr("id");
                var n = id.substring(id.length - 1);
                $('#marker-desc-' + n).css("top", (parseInt($(this).offset().top) - $('#header').height() + 23) + "px");
                $('#marker-desc-' + n).css("left", (parseInt($(this).offset().left)
                        - $('.row:has(#marker-icon-' + n + '):last').offset().left) + "px");
                $('#marker-desc-' + n).show();
                isDisplayed = true;
            }
        }, function () {
            var id = $(this).attr("id");
            var n = id.substring(id.length - 1);
            $('#marker-desc-' + n).hide();
            isDisplayed = false;
        });

        $('[id^="marker-icon-"]').click(function () {
            var id = $(this).attr("id");
            var n = parseInt(id.substring(id.length - 1)) - 1;
            if (!markedIcons[n]) {
                $(this).css("border", "1px dotted red");
                markedIcons[n] = true;
                if (n === 0) {
                    $('#q8').val(1);
                } else if (n === 1) {
                    $('#q8a').val(1);
                } else if (n === 2) {
                    $('#q8b').val(1);
                } else if (n === 3) {
                    $('#q8c').val(1);
                } else if (n === 4) {
                    $('#q8d').val(1);
                } else if (n === 5) {
                    $('#q8e').val(1);
                } else if (n === 6) {
                    $('#q8f').val(1);
                }
            } else {
                $(this).css("border", "none");
                markedIcons[n] = false;
                if (n === 0) {
                    $('#q8').val(0);
                } else if (n === 1) {
                    $('#q8a').val(0);
                } else if (n === 2) {
                    $('#q8b').val(0);
                } else if (n === 3) {
                    $('#q8c').val(0);
                } else if (n === 4) {
                    $('#q8d').val(0);
                } else if (n === 5) {
                    $('#q8e').val(0);
                } else if (n === 6) {
                    $('#q8f').val(0);
                }
            }
        });

        var isMarkerHit = false;

        $('#human-body-map').click(function (e) {
            if (isMarkerHit) {
                isMarkerHit = false;
                return;
            }
            var t = e.pageY - 20;
            var l = e.pageX - 5;
            lcount += 1;
            var id = "pain-marker-" + lcount;
            var dateId = "pain-date-" + lcount;
            selectedMarker = lcount - 1;
            var cornery = parseInt($('#human-body-map').offset().top);
            var cornerx = parseInt($('#human-body-map').offset().left);

            if (pointInPolygon(humanFrontLocs, e.pageX, e.pageY, cornerx, cornery)
                    || pointInPolygon(humanBackLocs, e.pageX, e.pageY, cornerx, cornery)) {
                var currentDate = new Date();
                var s = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear().toString();
                $('#human-body-map').append('<div id="' + id + '" class="pain-marker modality-marker-red"></div>');
                $('#human-body-map').append('<div id="' + dateId + '" class="pain-marker-date"></div>');


                $('#' + id).css("top", (t - cornery) + "px");
                $('#' + id).css("left", (l - cornerx) + "px");
                $('#' + dateId).css("top", (t - cornery + 10) + "px");
                $('#' + dateId).css("left", (l - cornerx + 45) + "px");

                locations.push({indexno: selectedMarker,
                    x: (l - cornerx), y: (t - cornery),
                    modalitytype: 0, date: s});

                var tt = e.pageY - $('#header').height();
                var ll = e.pageX - $('div:has(#human-body-map):last').offset().left - 18;
                $('#pain-description').css("top", tt + "px");
                $('#pain-description').css("left", ll + "px");
                $('#modality-selection').val(0);
                $('#modality-date').val(s);
                $('#pain-description').show();
                $('#pain-date-' + (selectedMarker + 1)).html($('#modality-date').val());
                $('#pain-date-' + (selectedMarker + 1)).show();

                $('.pain-marker').unbind("click");
                $('.pain-marker').click(function () {
                    var id = $(this).attr("id");
                    var n = parseInt(id.substring(id.lastIndexOf("-") + 1)) - 1;
                    selectedMarker = n;
                    isMarkerHit = true;
                    var m = getLocationsIndex(n);
                    var tt = $(this).offset().top + 20 - $('#header').height();
                    var ll = $(this).offset().left - $('.row:has(#human-body-map):last').offset().left;
                    $('#pain-description').css("top", tt + "px");
                    $('#pain-description').css("left", ll + "px");
                    $('#modality-selection').val(locations[m].modalitytype);
                    $('#modality-date').val(locations[m].date);
                    $('#pain-description').show();
                });
            }
        });

        $(window).resize(function () {
            $('#pain-description').hide();
        });

        $('#pain-description #save').click(function () {
            var m = getLocationsIndex(selectedMarker);
            locations[m].modalitytype = $('#modality-selection').val();
            locations[m].date = $('#modality-date').val();
            $('#pain-date-' + (selectedMarker + 1)).html($('#modality-date').val());

            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-red");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-green");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-orange");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-blue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-yellow");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-darkblue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-violet");

            var p = parseInt($('#modality-selection').val());
            if (p === 0) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-red");
            } else if (p === 1) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-green");
            } else if (p === 2) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-orange");
            } else if (p === 3) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-blue");
            } else if (p === 4) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-yellow");
            } else if (p === 5) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-darkblue");
            } else if (p === 6) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-violet");
            }
            $('#pain-description').hide();
        });

        $('#pain-description #discard').click(function () {
            $('#pain-description').hide();
        });

        $('#pain-description #delete').click(function () {
            $('#pain-marker-' + (selectedMarker + 1)).remove();
            $('#pain-date-' + (selectedMarker + 1)).remove();
            locations.splice(selectedMarker, 1);
            $('#pain-description').hide();
        });
</script>
<script>
	// $(".collapse").collapse();
</script>
<script>
	

</script>
<script src="/assets/js/bootstrap.js"></script>
 <script src="/assets/js/common.js"></script>