<link rel="stylesheet" href="/assets/css/modalities.css" />
 <script src="js/libs/modernizr-2.5.3.min.js"></script>

<div class="row ">
    <div class="large-12 columns ">
        <div class=" large-7 columns left">
            <h3>Modalities</h3>
            <dl class="accordion" data-accordion> 
                <dd> <a id="panel1" href="#panel1">Medical Massage modalities</a> 
                    <div id="panel1" class="content active"> 
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-red"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                     involve the therapist properly assessing the client at the beginning of the session. The therapist then customizes the session to the client’s needs and issues. It is found that medical massage can treat lack of range of motion, which affects gait and posture. It treats acute and chronic pain, tension, and headaches, extremity problems such as plantar fasciitis, carpal tunnel, tennis/golf elbow, inflammation, etc.
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
                <dd> 
                    <a id="panel2" href="#panel2">Spa Massage modalities</a>
                    <div id="panel2" class="content "> 
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-darkblue"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                    are designed to help the individual relax and detoxify. They include Swedish, Deep Tissue, and Body & Bath works.
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
                <dd> 
                    <a id="panel3" href="#panel3">Fascia manipulation or myofascial release modalities.</a>
                    <div id="panel3" class="content">
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-orange"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                    One of the strongest of these modalities is Rolfing a western derivative of Ayurvedic massage. These modalities help loosen the tight fascia surrounding the muscular tissue and joints. Tight or hardened fascia causes loss of range of motion, which can lead to frozen hips or shoulders.
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
                <dd> 
                    <a id="panel4" href="#panel4">Trigger Point Therapy and Neuromuscular Therapy</a> 
                    <div id="panel4" class="content ">
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-green"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                    Fairly new (since the 80s) but very effective massage modalities able to treat acute and chronic pain by treating the trigger points in the muscles. When the trigger point and fascia is tight and there is loss of range of motion over time, the nervous system quickly begins to experience pain that can only be relieved by treating the trigger points that are causing the issues. The most effective neuromuscular therapies available today are the Active Release Technique, Pain Neutralization Technique, and Muscle Exonero Technique. The Muscle Exonero Technique involves manipulation of the trigger point through stretch like the other two but incorporates neuromuscular breathing, and is one of the most beneficial therapies available. Advanced Massage Network is the few places you’ll find this progressive, cutting-edge technique.
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
                <dd> 
                    <a id="panel5" href="#panel5">Lymphatic Drainage</a> 
                    <div id="panel5" class="content ">
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-yellow"></div>
                            </div>    
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                     is designed to stimulate the lymphatic system and reduce swelling of lymph nodes. Lymphatic Drainage is the best modality for this. Cranial Sacral Work and the Muscle Exonero Technique are also beneficial in greatly stimulating the lymphatic system. Physicians and surgeons will find great results with these modalities post-op when quick healing is dependent on how quickly inflammation and edema goes away from the surgical sites of the patient.
                                </p>
                            </div>
                        </div>    
                    </div> 
                </dd> 
                <dd>
                    <a id="panel6" href="#panel6">Energy Work</a> 
                    <div id="panel6" class="content ">
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-blue"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                    are modalities based on treating the external energy field, or aura. The energy field is known throughout Acupuncture, Ayurvedic, and Reiki to get out of balance. We recommend Reiki for individuals suffering from cancer, especially those undergoing chemotherapy, since this modality requires little to no touch. It positively affects the client’s psyche.
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
                <dd>
                    <a id="panel7" href="#panel7">Eastern Massage Modalities</a> 
                    <div id="panel7" class="content ">
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-violet"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                    were some of the first recorded in history. Ayurvedic is the oldest and dates back at least 5,000 years. One of most highly recommended modalities other than Ayurvedic is Thai massage, which is great for applying traction on the joints and stretching the muscles. Acupressure can ease problems with the sympathetic nervous system. We recommend that you try these Oriental modalities at least once!
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
                <dd>
                    <a id="panel8" href="#panel8">Insurance Approved Provider</a> 
                    <div id="panel8" class="content ">
                        <div style="display: table;">
                            <div style="display: table-cell; vertical-align: middle;">
                                <div class="modality-animated-icon modality-animated-icon-black"></div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle;">
                                <p>
                                    These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.
                                </p>
                            </div>
                        </div>
                    </div> 
                </dd> 
            </dl>
        </div>
        <div class=" right large-5 columns">
            <h4>Benefits of Medical Massage Therapy</h4>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
    </div>
</div>
<script type='text/javascript'>
var url = document.URL;
  url = url.split('#')[1]
$(document).ready(function(){
    if(url!='panel1'){
        $('#'+url).trigger('click');
    }
});


</script>
<script>
    $(document).foundation('accordion');
</script>
