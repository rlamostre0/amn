<link rel="stylesheet" href="/assets/css/calendar.css" />
<div class="row">
    <div class="large-12 columns">
        <h2 class='title'>Schedule an Appointment</h2>
        <div id='calendar'>
        </div>
    </div>
</div>
<script src="/assets/js/vendor/jquery.js"></script>
<script src="/assets/js/vendor/fullcalendar/moment.min.js"></script>
<script src="/assets/js/vendor/fullcalendar/jquery-ui.custom.min.js"></script>
<script src="/assets/js/vendor/fullcalendar/fullcalendar.min.js"></script>
<script>
$('#calendar').fullCalendar({
    defaultView: "agendaWeek",
    selectable: true,
    selectHelper: true,
	eventSources: [ 
		{
			url:"/schedule/schedules",
			className:"own"
		},
		{
			url:"/schedule/otherschedules",
			color:"gray"
		}
	],
	eventClick: function(event) {
        if ($(this).hasClass("own")) {
            var r = confirm("Are you sure you want to remove your reservation?");
			if (r == true) {
				$.ajax({
					type: "POST",
					url: "/schedule/removeschedule",
					data: { estart: event.start.format(), eend: event.end.format()}
				})
				.done(function( msg ) {
					if (msg=="true")
						$('#calendar').fullCalendar("removeEvents");
						$('#calendar').fullCalendar("refetchEvents");
				});
			}
        }
    },
    select: function(start, end) {
      var title = prompt('Event Title:');
	  if (title != null&&title !== "") {
		  var eventData;
		  $.ajax({
			type: "POST",
			url: "/schedule/setschedule",
			data: { event: title, estart: start.format(), eend: end.format()}
		  })
		  .done(function( title ) {
		  if (title==""){
              window.location = '/customer/log_in';
		  }
		  else if (title=="false") {
			alert("Reserved");
		  }
		  else
			eventData = {
			  title: title,
			  start: start,
			  end: end,
			  className: "own"
			};
			$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
		  });
	  }
	  $('#calendar').fullCalendar('unselect');
    },
    editable: false,
});
</script>