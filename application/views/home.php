<link rel="stylesheet" href="/assets/css/home.css" />
<script type="text/javascript">

    $(document).ready(function() {
        
        var animationT = null;
        $('.modality').hover(function() {
            var n = 0;
            var t = $(this);
            if (animationT !== null) {
                clearInterval(animationT);
            }
            animationT = setInterval(function() {
                $('.modality-animated-icon', t).css("background-position", n + "px 0px");
                n = n - 87;
                if (n <= -435) {
                    n = 0;
                }
            }, 200);
        }, function() {
            var t = $(this);
            clearInterval(animationT);
            $('.modality-animated-icon', t).css("background-position", "0px 0px");
        });
    });

</script>
<div class='row nico_simple_div' style='padding:20px;color:white'>
<h3 style='color:white'>Advanced Massage Network, LLC. Atlanta, Georgia locations are <b style='color:#3aff68'>OPEN</b> for business.</h3><br/>
The national website and other national franchise locations will be launching officially  very soon when the website is complete.<br/><br/>

<center><font size='5'>
<b>Open: Monday – Sunday;  9am – 10pm<br/><br/>

1016 Piedmont Avenue NE, Atlanta, Ga. 30309<br/><br/>

Call today to book an appointment.<br/><br/>

(404) 482-1633<br/><br/>

Corporate e-mail: Info@advancedmassagenetwork.com<br/><br/>
</font></center>
<h5 style='color:white'><b>We GUARANTEE if we can’t get your trigger points to diminish drastically relieving acute and chronic structural pain, or tension that you have been living with. We will give you your money back. If it works all we ask is that you allow us to record a 30 second or less video testimonial to document the results.  <br/><br/></b></h5>
<center><font size='5'>PRICING:<br/><br/>

Just Trigger point Therapy:<br/><br/>

15 minutes for – $20<br/><br/>

30 minutes for – $35<br/><br/>

Regular Therapy Session Pricing :<br/><br/>

1 hour – $85 per session<br/><br/>

90 minutes – $120 per session<br/><br/>

120 minutes – $150 per session<br/><br/>

Package of 3 – 90 minute sessions is $300<br/><br/>

Package of 3 – 60 minute sessions is $240<br/><br/></font></center><br/><br/>

If our therapy works, which it will. If you give us a 30 second or less video testimonial after your session. We will give you $10 off your session. <br/><br/>
No AMN membership fee required at this time.<br/><br/>
We will provide a therapist for you that specializes in a medical massage modality at these low rates.<br/><br/></b>
</div>
<div class="row nico_simple_div">
    <div class="large-12 columns" style="padding-left: 0px; padding-right: 0px;">
        <h1 class="heading">All Massage Modalities</h1>
    </div>
</div>    
<div class="row nico_simple_div">
    <div class="large-12 columns detail" style="text-align: center;">
        <div style="position: relative; display: inline-block; margin: 0 auto;">
            <div class="modality">            
                <p>Medical Massage Modalities</p>
                <a href='<?php site_url()?>/pages/modalities#panel1'><div class="modality-animated-icon modality-animated-icon-red"></div></a>
            </div>
            <div class="modality">            
                <p>Trigger Point Therapy and Neuromuscular Therapy</p>
                <a href='<?php site_url()?>/pages/modalities#panel4'><div class="modality-animated-icon modality-animated-icon-green"></div></a>
            </div>
            <div class="modality">            
                <p>Fascia Manipulation or Myofascial Release Modalities.</p>
                <a href='<?php site_url()?>/pages/modalities#panel3'><div class="modality-animated-icon modality-animated-icon-orange"></div></a>
            </div>
            <div class="modality">            
                <p>Lymphatic Drainage</p>
                <a href='<?php site_url()?>/pages/modalities#panel5'><div class="modality-animated-icon modality-animated-icon-yellow"></div></a>
            </div>
        </div>
    </div>
</div>
<div class="row nico_simple_div" style="margin-bottom: 12px;">
    <div class="large-12 columns detail" style="text-align: center;">
        <div style="position: relative; display: inline-block; margin: 0 auto;">
            <div class="modality">            
                <p>Spa Massage Modalities</p>
                <a href='<?php site_url()?>/pages/modalities#panel2'><div class="modality-animated-icon modality-animated-icon-darkblue"></div></a>
            </div>
            <div class="modality">            
                <p>Energy Work</p>
                <a href='<?php site_url()?>/pages/modalities#panel6'><div class="modality-animated-icon modality-animated-icon-blue"></div></a>
            </div>
            <div class="modality">            
                <p>Eastern Massage Modalities</p>
                <a href='<?php site_url()?>/pages/modalities#panel7'><div class="modality-animated-icon modality-animated-icon-violet"></div></a>
            </div>
        </div>
    </div>
</div>
<div id="section-2" class="row nico_simple_div" style="margin-bottom: 18px; padding: 10px;">
    <div class="large-4 columns">
        <select name="">
            <option value="" selected>Pains and Structural Body Problems</option>
            <option value="loremipsum">Bulging Disk</option>
            <option value="starbuck">Herniated Disk</option>
            <option value="hotdog">Sciatica</option>
            <option value="apollo">Pelvic Tilt</option>
            <option value="loremipsum">Tennis Elbow</option>
            <option value="starbuck">Golf Elbow </option>
            <option value="hotdog">Migrane Headaches</option>
            <option value="apollo">Lack of Range of Motion</option>
            <option value="loremipsum">Frozen Shoulder</option>
            <option value="starbuck">Frozen Hip</option>
            <option value="hotdog">Plantar Facetious</option>
            <option value="apollo">Carpal Tunnel</option>
            <option value="loremipsum">Muscle Sprains</option>
            <option value="starbuck">Muscle Strains</option>
            <option value="hotdog">Muscle Tears</option>
            <option value="apollo">Additional Stress</option>
            <option value="loremipsum">Atrophy</option>
            <option value="starbuck">Bone Spur</option>
            <option value="hotdog">More Problems lead to more problems.</option>
            <option value="apollo">Additional Stress</option>
        </select>       
    </div>
    <div class="large-4 columns">
        <select name="">
            <option value="" selected>Massage Techniques</option>
            <option value="deeptissuemassage">Deep Tissue Massage</option>
            <option value="starbuck">Medical Massage / Remedial Massage</option>
            <option value="hotdog">Spa / Swedish / Deep Tissue</option>
            <option value="apollo">Trigger Point Therapy / Neuromuscular Therapy</option>
            <option value="starbuck">Lymphatic System / Lymphatic Drainage</option>
            <option value="hotdog">Energy Work / Reiki</option>
            <option value="apollo">Eastern Medicine massage or massage modalities from Asia</option>
            <option value="apollo">Insurance Approved Provider</option>
        </select>
    </div>
    <div class="large-4 columns">
        <select name="">
            <option value="" selected>Health Insurance Provider</option>
            <option value="united-health">Unitedhealth Group</option>
            <option value="united-health">Wellpoint Inc. Group</option>
            <option value="united-health">Kaiser Foundation Group</option>
            <option value="united-health">Humana Group</option>
            <option value="united-health">Aetna Group</option>
            <option value="united-health">Cigna Health Group</option>
            <option value="united-health">Highmark Group</option>
            <option value="united-health">Coventry Corp. Group</option>
            <option value="united-health">HIP Insurance Group</option>
            <option value="united-health">Independence Blue Cross Group</option>
            <option value="united-health">Blue Cross Blue Shield of New Jersey Group</option>
            <option value="united-health">Blue Cross Blue Shield of Michigan Group</option>
            <option value="united-health">California Physicians' Service</option>
            <option value="united-health">Blue Cross Blue Shield of Florida Group</option>
            <option value="united-health">Health Net of California, Inc.</option>
            <option value="united-health">Centene Corp. Group</option>
            <option value="united-health">Carefirst Inc. Group</option>
            <option value="united-health">Wellcare Group</option>
            <option value="united-health">Blue Cross Blue Shield of Massachusetts Group</option>
            <option value="united-health">UHC of California</option>
            <option value="united-health">Lifetime Healthcare Group</option>
            <option value="united-health">Cambia Health Solutions Inc.</option>
            <option value="united-health">Metropolitan Group</option>
            <option value="united-health">Molina Healthcare Inc. Group</option>
        </select>
    </div>
</div>
<div class="row nico_transparency">
    <div class="large-12 columns" style="padding-left: 0px; padding-right: 0px;">
        <h1 class="heading">Advanced Massage Network, “You’re Medical Massage Resource”</h1>
    </div>
</div>
<div class="row nico_transparency">
    <div class="large-6 medium-6 small-12 columns detail" style="text-align: center;">
        <div class="mission-msg">
            <p>
                We are proud to be the first massage franchise to promote medical massage correctly. We have successfully recruited the best independent massage therapists into a larger business setting by paying them what they deserve, more than any other enterprise. Advanced Massage Network is a network of the most advanced massage therapists in your area and we strive to solve your pain issues effectively and efficiently.

We are not your stereotypical massage parlor. In fact, we try to break all of the massage molds. At Advanced Massage Network, we believe the massage industry is in a horrible state at the moment, for clients and therapists. It is a hit-or-miss for clients and talented therapists that are paid well below their worth, forcing them to leave the industry, or go out on their own independently. We practice all massage modalities and specialize in the most advanced massage modalities available, including osteopathic soft tissue manipulation and medical massage. We promise you the highest quality massage therapy and results you will find.

Advanced Massage Network is probably the most unique and innovative massage business you will ever come across today. The networks main goal is to change the massage industry for the better, because the therapist that make up the network are feed up with the lack of education and inexperience that makes up 80% of the current massage industry. The therapist that make up this network of therapists are of the top 10% of the massage industry. They are the best around from within your community. They all are real healers! These individuals eyes are wide open to what society lacks, great therapists and real body manipulation that make a difference. All massage modalities are designed to manipulate the body in different ways. We help you find the correct modality to give you the proper therapy that your body needs to heal it quickly. This network is all about innovation. The therapists all have been hand pick and selected internally by the great massage therapists that already make up the network. Only 10% of the whole massage industry gets to join this exclusive club. That is how selective we are. Honestly there is no point of even going anywhere else as a consumer, because we are the best and our pricing is the most competitive around this we easily guarantee.
            </p>
        </div>
    </div>    
    <div class="large-6 medium-6 small-12 columns detail" style="text-align: center;">
        <div class="video-player">
            <a href="#" class="play"></a>
        </div>
    </div>
</div>
<div class="row nico_transparency" style="margin-bottom: 10px;">    
    <dl class="accordion" data-accordion> 
        <dd> 
            <a class="accordion-item" href="#panel1">How many massage therapists practice Medical Massage, and how much does it usually cost?</a> 
            <div id="panel1" class="content">
                Medical massage is only practiced by 20% of all massage therapists in the United States. Only a select few know more advanced modalities designed to treat acute and chronic pain.<br/><br/>

All of the Advanced Massage Network therapists are trained in the Muscle Exonero Technique, an advanced neuromuscular therapy technique that can begin to alleviate trigger points in one session.<br/><br/>

These advanced medical massage services typically cost between $120 - $200 for a 90 minute session.
            </div> 
        </dd> 
        <dd> 
            <a class="accordion-item" href="#panel2">The difference between spa and medical massage</a> 
            <div id="panel2" class="content">
                Spa massage, indicated by the blue modality icon, is about relaxation and detoxifying the body.<br/><br/>

Medical Massage, indicated by the red modality icon, The massage practitioner assesses the body medically by figuring out what can be done to help the individual suffering from tension and acute or chronic pain. Medical Massage helps improve one’s gait and posture as well as treating headaches, inflammation, lack of range of motion, stress and tension, nerve pain, lower back and neck pain, etc.<br/><br/>

Medical Massage is more interactive and involves interaction with the therapist. Our goal is to properly assess your medical condition and create a massage therapy plan to address and treat the problems. If the problem is a sever condition needing surgery, or if the client if found needing emergency care the client will be advised to seek the appropriate medical attention immediately. Usually this is not the case, and it has been found to treat and even prevent the majority of all structural pain issues before they occur.
            </div> 
        </dd> 
        <dd> 
            <a class="accordion-item" href="#panel3">Why is massage good for you?</a> 
            <div id="panel3" class="content">
                The most important benefit of massage is the stimulation of the lymphatic system. You will sweat out more metabolic waste than usual, which helps detoxify the body.<br/><br/>

Massage is also good for treating the soft tissue and muscular system, which affects the skeletal and nervous system. When trigger points grow larger you lose range of motion and your fascia tissue will start harden. This affects your gait, posture, and overall physical appearance, but it can be corrected with the proper massage modalities. These include Muscle Exonero Technique, Medical Massage, Neuromuscular Therapy, Orthopedic massage, Rolfing, etc.<br/><br/>

Massage releases feel-good chemicals and hormones, tilting the balance away form stress and toward relaxation and healing.<br/><br/>

Massage is absolutely one of the best things you can do to balance yourself in this high-stress and toxic world.<br/><br/>
            </div> 
        </dd> 
        <dd> 
            <a class="accordion-item" href="#panel4">The truth about the massage industry</a> 
            <div id="panel4" class="content">
                Only a select few individuals are educated in advanced massage modalities. Due to lack of time, the majority of massage schools only prepare therapists to pass the national board exam and teach spa massage modalities.  It is left up to the individual after their massage school education to pursue the more advanced massage modalities. Unfortunately only about 5% - 20% take the initiative to further educate themselves and 5% - 10% master Medical Massage.<br/><br/>

Medical Massage education takes significantly more time to master than the typical standard Spa education. The massage industry and large franchises typically pay about 30% - 40% of the session price to the therapist and encourage unnecessary tips. Advanced Massage Network is proud pay out a minimum of 70% to our therapists, which is why we are able to recruit the very best the industry has to offer in your area.<br/><br/>

Advanced Massage Network’s goal is to innovate and change the stagnant massage industry by treating the client with the appropriate modalities customized to their problems. Advanced Massage Network consists of the most elite independent massage therapists in the nation. Check out our innovative scheduler, which speeds up the booking process and effectively suggests the most appropriate modality for your issue.<br/><br/>
            </div> 
        </dd> 
        <dd> 
            <a class="accordion-item" href="#panel5">Get a free chair massage.</a> 
            <div id="panel5" class="content">
                Panel 5. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
            </div> 
        </dd>
        <a href='<?php site_url() ?>/testimonial'>
        <dd style='padding:20px;color:white;background:#3b4154' class='text-center'>
            More Interesting Videos & Client Testimonials.
        </dd>
        </a>
    </dl>
</div>
