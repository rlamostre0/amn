<link rel="stylesheet" href="/assets/css/location.css" />
<script src="http://www.mapquestapi.com/sdk/js/v7.0.s/mqa.toolkit.js?key=Fmjtd%7Cluur2hu229%2C8n%3Do5-9waxuu"></script>
<script src="/assets/js/mapquestsearch.js"></script>
<div class="large-12 columns">
    <?php echo form_open('location/search', array('id' => 'location-search-form')); ?>
    <?php foreach ($locations as $r) { ?>
        <div class="row">
            <div class="large-12 columns" style="padding: 5px 18px;">
                <h3>You searched for clinics in <?= $r[0]["state"]; ?></h3>
                <?php foreach ($r[1] as $c) { ?>
                    <a href="<?php echo site_url() ?>location/searchbycity?city=<?= $c["address_city"]; ?>&state=<?= $c["address_state"]; ?>" style="display: inline-block; margin-right: 30px; margin-bottom: 8px;">View a map of clinics in <?= $c["address_city"]; ?></a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">

    $(document).ready(function () {
    });

</script>
