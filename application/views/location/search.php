<link rel="stylesheet" href="/assets/css/location.css" />
<script src="http://www.mapquestapi.com/sdk/js/v7.0.s/mqa.toolkit.js?key=Fmjtd%7Cluur2hu229%2C8n%3Do5-9waxuu"></script>
<script src="/assets/js/mapquestsearch.js"></script>
<div class="large-12 columns">
    <?php echo form_open('location/search', array('id' => 'location-search-form')); ?>
    <div class="row ">
        <div class="large-12 columns location-search-form">
            <h3>Advance Massage  Network Locations</h3>
            <div class="search-form grey rounded large-12 columns">
                <div class="large-2 small-2 columns left">
                    <h3>Find a clinic</h3>
                </div>
                <div class="large-4 columns">
                    <input type="text" id="search_zipcode" name="search_zipcode" placeholder="Zip Code" value="" style="display: inline; width: 45%;"/>
                    &nbsp;
                    <select id="search_mile" name="search_mile" style="display: inline; width: 45%;">
                        <option value="">Miles</option>
                        <?php for ($i = 1; $i <= 25; $i++) { ?>
                            <option value="<?php echo $i ?>"><?php echo $i . ($i > 1 ? " miles" : " mile"); ?></option>
                        <?php } ?>
                    </select> 
                </div>
                <div class="large-4 columns">
                    Or&nbsp;
                    <select id="search_state" name="search_state" style="display: inline; width: 30%;">
                        <option value="">State</option>
                        <?php foreach ($usstates as $state) { ?>
                            <option value="<?php echo $state['state_code']; ?>">
                                <?php echo $state["state_code"]; ?>
                            </option>
                        <?php } ?>
                    </select>
                    &nbsp;
                    <select id="search_city" name="search_city" style="display: inline; width: 45%;">
                        <option value="">City</option>
                    </select>
                </div>
                <div class="large-2 columns">
                    <button id="search" type="submit" class="btn btn-primary" />Search</button>
                </div>
            </div>
        </div>
    </div>
    <?php foreach ($locations as $r) { ?>
        <div class="row">
            <div class="large-12 columns" style="padding: 5px 18px;">
                <h3>You searched for clinics in <?= $r[0]["state"]; ?></h3>
                <?php foreach ($r[1] as $c) { ?>
                    <a href="<?php echo site_url() ?>location/searchbycity?city=<?= $c["address_city"]; ?>&state=<?= $c["address_state"]; ?>" style="display: inline-block; margin-right: 30px; margin-bottom: 8px;">View a map of clinics in <?= $c["address_city"]; ?></a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if ($search_now == 1) { ?>
        <div class="row ">
            <div class="large-12 columns">
                <div class="divider no-border"></div>
                <h3 class="bold">Filter by Service</h3>
                <div class=" filter-service grey rounded left"> 
                    <?php
                    $i = 0;
                    $checked = NULL;
                    foreach ($services as $i => $service):
                        if (is_array($this->input->post('services'))) {
                            $checked = in_array($service->id, $this->input->post('services')) ? "checked" : NULL;
                        }
                        if ($i % 4 == 0):
                            ?> 
                        </div>
                        <div class="divider no-border"></div>   
                        <div class=" filter-service grey rounded left">
                        <?php endif; ?>     	
                        <div class="small-3  large-3 columns types-massage medical">
                            <input id="service-filter-<?php echo ($i + 1); ?>" 
                                   name="service-filter-<?php echo ($i + 1); ?>" type="checkbox" 
                                   value="<?php echo $service->id ?>" <?php echo $checked ?> name="services[]" />&nbsp;<?php echo $service->service ?>
                        </div>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </div>
            </div>        
        </div>
        <?php
    }
    echo form_close();
    ?>
    <div class="row">
        <div class="divider no-border"></div>
        <div class="large-7 columns left">
            <div id="searchmapstatus" style="font-weight: bold; font-size: 15px; padding: 20px;"></div>
            <div id="searchmapresults"></div>
        </div>
        <div class="large-5 columns right">
            <div id="search-map-border" style="display: none; border-radius: 10px; border: 1px solid gray; overflow: hidden;">
                <div id="search-map" style="display: block; width:450px; height:350px;"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    window.site_url="<?php echo site_url() ?>";
    window.mapResultData = null;
    window.mapquestkey = "Fmjtd%7Cluur2hu229%2C8n%3Do5-9waxuu";
    
    var filter = [false, false, false, false, false, false, false, false];

    $(document).ready(function () {

        $('#search_state').change(function () {
            $.ajax({
                url: '<?php echo site_url(); ?>location/getcities',
                type: "post",
                dataType: 'json',
                data: {state: $(this).val()},
                success: function (data) {
                    $('#search_city').empty();
                    if (data !== null && data.length > 1) {
                        data.forEach(function (c) {
                            $('#search_city').append($('<option/>', {
                                value: c.city, text: c.city
                            }));
                        });
                    } else {
                        $('#search_city').append($('<option/>', {
                            value: "", text: "City"
                        }));
                    }
                }
            });
        });

        if (<?php echo $search_now; ?> === 1) {
            searchMaps(<?php echo $search_type; ?>,
                    "<?php echo $zip_for_search; ?>", parseInt("<?php echo $miles_for_search; ?>"),
                    "<?php echo $city_for_search; ?>", "<?php echo $state_for_search; ?>",
                    "search-map", "searchmapresults", "searchmapstatus");
        }

        $('[id^="service-filter-"]').click(function () {
            var id = $(this).attr("id");
            var n = parseInt(id.substring(15)) - 1;
            filter[n] = $(this).prop("checked");
            if (window.mapResultData !== null) {
                showMaps(window.mapResultData, filter, "search-map", "searchmapresults", "searchmapstatus");
            }
        });
    });


</script>
