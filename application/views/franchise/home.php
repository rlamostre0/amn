<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>
            <?php
            if (isset($meta_title)) {
                echo $meta_title . " | ";
            }
            ?>Advanced Massage Network 
        </title>
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="description" content="<?php echo $meta_description ?>">
        <meta name="robots" content="index,nofollow"> 
        <link rel="stylesheet" href="/assets/css/foundation.css" />
        <link rel="stylesheet" href="/assets/css/fullcalendar.min.css" />
        <link rel="stylesheet" href="/assets/css/franchise.css" />
        <link rel="stylesheet" href="/assets/css/pikaday.css" />
        <link rel="stylesheet" type="text/css" href="/assets/js/aciTree/css/aciTree.css" media="all">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
        <script src="/assets/js/vendor/jquery.js"></script>
        <script src="/assets/js/foundation.min.js"></script>
        <script src="/assets/js/foundation/foundation.orbit.js"></script>
        <script src="/assets/js/vendor/modernizr.js"></script>
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/moment.min.js"></script>    
        <script src="/assets/js/fullcalendar.min.js"></script>   
        <script src="/assets/js/pikaday.js"></script>          
        <script src="/assets/js/aciTree/js/jquery.aciPlugin.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/assets/js/aciTree/js/jquery.aciTree.min.js"></script>				
        <script src="http://www.mapquestapi.com/sdk/js/v7.0.s/mqa.toolkit.js?key=Fmjtd%7Cluur2hu229%2C8n%3Do5-9waxuu"></script>
        <script src="/assets/js/mapquestsearch.js"></script>
        <script type="text/javascript">
            
            window.mapquestkey = "Fmjtd%7Cluur2hu229%2C8n%3Do5-9waxuu";

            $(document).ready(function () {                

                $(document).foundation();
                $('#franchise-slider').show();

                var lat = "<?php echo $latitude; ?>";
                var lng = "<?php echo $longitude; ?>";

                $.ajax({
                    url: 'http://www.mapquestapi.com/search/v2/radius',
                    dataType: 'jsonp', crossDomain: true,
                    data: {
                        key: decodeURIComponent(window.mapquestkey),
                        inFormat: "json",
                        json: JSON.stringify({
                            origin: lat + ", " + lng,
                            hostedDataList: [
                                {
                                    tableName: "mqap.142301_locations"
                                }
                            ]
                        })
                    },
                    success: function (data) {
                        if (typeof data.searchResults !== 'undefined') {
                            if (data.searchResults.length > 0) {
                                var pois = new MQA.ShapeCollection();
                                var mapData = data.searchResults[0];
                                var numberedPoi = new MQA.Icon('http://www.mapquestapi.com/staticmap/geticon?uri=poi-1.png', 20, 29);
                                var location = new MQA.Poi({lat: mapData.shapePoints[0], lng: mapData.shapePoints[1]});
                                location.setIcon(numberedPoi);
                                location.setRolloverContent('<span class="poi-title">' + mapData.name + '</span>');
                                location.setInfoContentHTML('<div class="poi-body"><div class="poi-title">' +
                                        mapData.name + '</div>' + mapData.fields.street + "</br>" + mapData.fields.city + ", " +
                                        mapData.fields.state + ", " + mapData.fields.zip_code + '<br /></div>');
                                pois.add(location);

                                window.map = new MQA.TileMap({
                                    elt: document.getElementById("location-map"),
                                    zoom: 10,
                                    collection: pois,
                                    bestFitMargin: 0
                                });
                                MQA.withModule('smallzoom', 'mousewheel', function () {
                                    map.addControl(new MQA.SmallZoom());
                                    map.enableMouseWheelZoom();
                                });

                                $('#header-location').html(mapData.fields.location);
                                $('#header-address').html(mapData.fields.street + "<br/>" + mapData.fields.city + ", " +
                                        mapData.fields.state + ", " + mapData.fields.zip_code);
                                $('#header-telno').html(mapData.fields.phone_no);
                                $('#location').html(mapData.fields.location);
                                $('#franchise-no').html(mapData.fields.franchise_no);
                                $('#license-no').html(mapData.fields.license_no);
                                $('#name').html(mapData.fields.name);
                                $('#address').html(mapData.fields.street + "<br/>" + mapData.fields.city + ", " +
                                        mapData.fields.state + ", " + mapData.fields.zip_code);
                                $('#location-desc').html(mapData.fields.location_description);

                                if (mapData.fields.is_medical_massage) {
                                    $('#service-red').show();
                                }
                                if (mapData.fields.is_spa_massage) {
                                    $('#service-green').show();
                                }
                                if (mapData.fields.is_fascia_manipulation) {
                                    $('#service-orange').show();
                                }
                                if (mapData.fields.is_trigger_point_therapy) {
                                    $('#service-blue').show();
                                }
                                if (mapData.fields.is_lymphatic_system) {
                                    $('#service-yellow').show();
                                }
                                if (mapData.fields.is_energy_work) {
                                    $('#service-darkblue').show();
                                }
                                if (mapData.fields.is_couples_massage) {
                                    $('#service-violet').show();
                                }
                                if (mapData.fields.is_Insurance) {
                                    $('#service-black').show();
                                }

                                $('#footer-location').html(mapData.fields.location);
                                $('#footer-address').html(mapData.fields.street + "<br/>" + mapData.fields.city + ", " +
                                        mapData.fields.state + ", " + mapData.fields.zip_code);
                                $('#footer-telno').html(mapData.fields.phone_no);
                            }
                        } else {
                            alert("error");
                        }
                    }
                });

            });

        </script>
    </head>
    <body>
        <div class="row header-franchise">
            <div class="large-3 columns">
                <a href="<?php echo site_url() ?>pages/home"><img src="/assets/img/logo2.png" alt="logo" title="Advance Massage Network"></a>  
            </div>
            <div class="large-9 columns" style="padding: 20px;">
                <span class="header-h1">Advanced Massage Network</span>
                <span id="header-location" class="header-h1"></span>
                <span id="header-address" class="header-h2"></span>
                <span id="header-telno" class="header-h3"></span>
            </div>
        </div>
        <div class="row menu-bar-franchise">
            <div class="large-12 columns">
                <ul>
                    <li><a>Home</a></li>
                    <li><a>Sessions</a></li>
                    <li><a>Pricing</a></li>
                    <li><a>Contact Us</a></li>
                </ul>
            </div>
        </div>
        <div class="row content-franchise">
            <div class="large-12 columns">
                <div class="row">
                    <div class="large-8 columns">
                        <div class="row">
                            <div class="large-4 columns">
                                <img src="/assets/img/franchise/main-img.png"/>
                            </div>
                            <div class="large-8 columns">
                                <span class="content-h1"></span>
                                <span id="location" class="content-h1"></span>
                                Franchise #: <span id="franchise-no"></span><br/>
                                License #: <span id="license-no"></span><br/>
                                <br/>
                                <span id="name" class="content-h2"></span>
                                <span id="address"></span><br/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <b>Location Description:</b><br/>
                                <span id="location-desc"></span><br/>
                                <br/>                                
                                <b>Distance From You:</b><br/>
                                <a href="#" class="map-direction"></a><br/>
                                <br/>
                                <b>Modalities This Location Practices:</b><br/>
                                <a id="service-red" href="#" class="service-offered" style="display: none;"><img class="mi-red" title="Medical Massage/Remedial Massage"/></a>
                                <a id="service-green" href="#" class="service-offered" style="display: none;"><img class="mi-green" title="Spa/Swedish/Deep Tissue"/></a>
                                <a id="service-orange" href="#" class="service-offered" style="display: none;"><img class="mi-orange" title="Fascial Manipulation/Myofascial Release"/></a>
                                <a id="service-blue" href="#" class="service-offered" style="display: none;"><img class="mi-blue" title="Trigger Point Therapy/Neuromuscular Therapy"/></a>
                                <a id="service-yellow" href="#" class="service-offered" style="display: none;"><img class="mi-yellow" title="Lympathic Massage/Lympathic Drainage"/></a>
                                <a id="service-darkblue" href="#" class="service-offered" style="display: none;"><img class="mi-darkblue" title="Energy Work/Reiki"/></a>
                                <a id="service-violet" href="#" class="service-offered" style="display: none;"><img class="mi-violet" title="Eastern Medicine"/></a>
                                <a id="service-black" href="#" class="service-offered" style="display: none;"><img class="mi-black" title="Insurance Approved Provider"/></a>
                                <br/>
                                <b><i>Connect With Us:</i></b><br/>
                                <a href="#" class="map-twitter"></a><a href="#" class="map-google"></a>
                                <br/>
                                <b><i>Connect With Us:</i></b><br/>
                                <a href="#" class="map-booksession"></a>
                                <a href="#" class="map-affiliateblogger"></a>
                                <a href="#" class="map-reviews"></a>
                                <br/>
                                <a href="#" class="map-schedulenow"></a>
                            </div>
                            <div class="row">
                                <div id="franchise-slider" class="large-12 columns">
                                    <ul class="example-orbit" data-orbit> 
                                        <li> 
                                            <img src="/assets/img/slide1.jpg" alt="slide 1" />
                                        </li> 
                                        <li> 
                                            <img src="/assets/img/slide2.jpg" alt="slide 2" />  
                                        </li> 
                                        <li> 
                                            <img src="/assets/img/slide3.jpg" alt="slide 3" /> 
                                        </li> 
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <p class="about-paragraph">
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus 
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">                                    
                                    <p class="about-paragraph">
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus 
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">                                    
                                    <p class="about-paragraph">
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="large-4 columns">                        
                        <div class="row">
                            <div class="large-12 columns">                                    
                                <div class="why-choice-us">
                                    <b>Why Choice Us</b><br/>
                                    <br/>
                                    <ul>
                                        <li>Pellentesque habitant morbi tristique senectus</li>
                                        <li>Pellentesque habitant morbi tristique senectus</li>
                                        <li>Pellentesque habitant morbi tristique senectus</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">                                    
                                <span>Location:</span>
                                <div class="location-map-container">
                                    <div id="location-map" class="location-map"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">                                    
                                <div class="business-hours">
                                    <b>Business Hours</b><br/>
                                    <br/>
                                    <table>
                                        <tr>
                                            <td>Mon-Fri</td>
                                            <td>0:00am - 0:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Sat</td>
                                            <td>0:00am - 0:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Sun</td>
                                            <td>0:00am - 0:00pm</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">                                    
                                <b>Where to park:</b><br/>
                                <br/>
                                <p>
                                    Vestibulum tortor quam, feugiat vitae malesuada fames ac turpis egestas.
                                </p>
                                <div class="where-to-park">
                                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae 
                                </div>
                                <br/>
                                <div class="separator"></div>
                                <br/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">                                    
                                <br/>
                                <b>Payment Methods:</b><br/>
                                <br/>
                                <div class="separator"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">                                    
                        <p class="about-paragraph">
                            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">                                    
                        <div class="video-player">
                            <a href="#" class="play"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-franchise">
            <div class="large-12 columns" style="text-align: center;">
                <a style="float: left;"><i>&LT;&LT;&LT;Back</i></a>
                <hr style="border: 1px solid #27aae0;"/>
                <span class="footer-h1"></span>
                <span id="footer-location" class="footer-h4"></span>
                <span id="footer-address" class="footer-h2"></span>
                <span id="footer-telno" class="footer-h3"></span>
            </div>
        </div>
    </body>
</html>
