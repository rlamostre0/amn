<link rel="stylesheet" href="/assets/css/soapnote.css" />
<div id="soapnote-section" class="large-12 columns">
    <?php if (isset($soapnoteerrors)) { ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $soapnoteerrors; ?>
            </div>            
        </div>
        <?php
    }
    echo form_open('soapnote/save');
    ?>
    <div class="row " style="padding: 10px 0px 10px 0px;">        
        <div class="large-12 columns">
            <h4>SOAP Notes</h4>
        </div>
    </div>
    <div class="row" style="padding: 25px;">
        <input name="therapistid" value="<?php echo $therapistid; ?>" type="hidden"/>
        <input name="customerid" value="<?php echo $customerid; ?>" type="hidden"/>
        <input name="therapist_fullname" value="<?php echo $therapist_fullname; ?>" type="hidden"/>
        <input name="therapist_positiontitle" value="<?php echo $therapist_positiontitle; ?>" type="hidden"/>
        <input name="franchise_location" value="<?php echo $franchise_location; ?>" type="hidden"/>
        <input name="franchise_locationdesc" value="<?php echo $franchise_locationdesc; ?>" type="hidden"/>
        <input name="franchise_phoneno" value="<?php echo $franchise_phoneno; ?>" type="hidden"/>
        <input name="franchise_licenseno" value="<?php echo $franchise_licenseno; ?>" type="hidden"/>
        
        <div class="large-6 small-6 columns" style="text-align: left;">
            <div class="row">
                <div class="large-12 small-12 columns"><label>Therapist</label></div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                    <span class="h-1"><?php echo $therapist_fullname; ?>&nbsp;-&nbsp;<?php echo $therapist_positiontitle; ?></span>            
                </div>
            </div>
            <?php if ($franchise_location != null && $franchise_location != "") { ?>
                <div class="row">
                    <div class="large-12 small-12 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                        <span class="h-2"><?php echo $franchise_location; ?></span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($franchise_locationdesc != null && $franchise_locationdesc != "") { ?>
                <div class="row">
                    <div class="large-12 small-12 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                        <span class="h-2"><?php echo $franchise_locationdesc; ?></span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($franchise_phoneno != null && $franchise_phoneno != "") { ?>
                <div class="row">
                    <div class="large-12 small-12 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                        <span class="h-2"><?php echo $franchise_phoneno; ?></span>
                    </div>
                </div>
            <?php } ?>            
            <?php if ($franchise_licenseno != null && $franchise_licenseno != "") { ?>
                <div class="row">
                    <div class="large-12 small-12 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">                                                
                        <span class="h-2"><?php echo $franchise_licenseno; ?></span>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="large-6 small-6 columns" style="text-align: left;">
            <input name="client_name" value="<?php echo $client_name; ?>" type="hidden"/>
            <input name="client_date" value="<?php echo $client_date; ?>" type="hidden"/>
            <input name="client_birthdate" value="<?php echo $client_birthdate; ?>" type="hidden"/>
            <input name="client_healthins" value="<?php echo $client_healthins; ?>" type="hidden"/>
            <input name="client_currentmeds" value="<?php echo $client_currentmeds; ?>" type="hidden"/>            
            <div class="row">
                <div class="large-12 small-12 columns"><label>Client's / patient's name</label></div>
            </div>
            <div class="row">
                <div class="large-2 columns"><label>Name:</label></div>
                <div class="large-10 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                    <span class="h-1"><?php echo $client_name; ?>&nbsp;</span>
                </div>
            </div>
            <div class="row">
                <div class="large-2 columns"><label>Date:</label></div>
                <div class="large-4 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                    <?php echo $client_date; ?>&nbsp;
                </div>
                <div class="large-2 columns"><label>Date of Birth:</label></div>
                <div class="large-4 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                    <?php echo $client_birthdate; ?>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="large-6 columns"><label>Health Insurance Company Provider:</label></div>
                <div class="large-6 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                    <?php echo $client_healthins; ?>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="large-3 columns"><label>Current Meds:</label></div>
                <div class="large-9 columns" style="border-bottom: 1px solid #000000; padding: 4px 0px 4px 0px;">
                    <?php echo $client_currentmeds; ?>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline;">Additional notes to other AMN therapists about this client"</label>
                    <textarea id="addl-client-info" name="addl_client_info" rows="3" cols="100%"><?php echo set_value('addl_client_info'); ?></textarea>
                </div>
            </div>
        </div>
    </div>    
    <div class="row">
        <div class="large-6 columns">
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline; font-size: 35px; font-weight: bold; margin-right: 6px;">S</label>
                    <label style="display: inline;">CLIENT GOALS/UPDATE</label>                    
                    <textarea id="question-1" name="question_1" rows="3" cols="100%"><?php echo set_value('question_1', ''); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline;">LOCATION/SYMPTOMS/INTENSITY/FREQUENCY/DURATION/ONSET</label>
                    <textarea id="question-2" name="question_2" rows="3" cols="100%"><?php echo set_value('question_2'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline;">AGGRAVATIONS/RELIEVING CIRCUMSTANCES REGARDING ACTIVITIES OF DAILY LIVING</label>
                    <br/>
                    <label>A:</label>
                    <textarea id="question-3" name="question_3" rows="2" cols="100%"><?php echo set_value('question_3'); ?></textarea>
                    <label>R:</label>
                    <textarea id="question-4" name="question_4" rows="2" cols="100%"><?php echo set_value('question_4'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline; font-size: 35px; font-weight: bold; margin-right: 6px;">O</label>
                    <label style="display: inline;">HYPERTONICITIES-INTENSITY, VISUAL/PALPABLE OBSERVATIONS, ADDITIONAL TESTS</label>
                    <textarea id="question-5" name="question_5" rows="3" cols="100%"><?php echo set_value('question_5'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline;">TREATMENT GOALS</label>
                    <textarea id="question-6" name="question_6" rows="3" cols="100%"><?php echo set_value('question_6'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline; font-size: 35px; font-weight: bold; margin-right: 6px;">A</label>
                    <label style="display: inline;">MASSAGE</label>
                    <textarea id="question-7" name="question_7" rows="3" cols="100%"><?php echo set_value('question_7'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline;">CHANGES DUE TO MASSAGE</label>
                    <textarea id="question-8" name="question_8" rows="3" cols="100%"><?php echo set_value('question_8'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline; font-size: 35px; font-weight: bold; margin-right: 6px;">P</label>
                    <label style="display: inline;">SUGGESTED TREATMENT PLAN</label>
                    <textarea id="question-9" name="question_9" rows="3" cols="100%"><?php echo set_value('question_9'); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
                    <label style="display: inline;">HOMEWORK</label>
                    <textarea id="question-10" name="question_10" rows="3" cols="100%"><?php echo set_value('question_10'); ?></textarea>
                </div>
            </div>            
            <div class="row">
                <div class="large-12 small-12 columns">
                    <p>
                        Is your assessment finished and correct?&nbsp;&nbsp;&nbsp;<input type="checkbox" class="final_agree"/>
                    </p>
                    <br/>
                    <button type="submit">Submit</button>
                </div>
            </div>
        </div>
        <div class="large-6 columns" style="margin-top: 30px;">
            <div class="row">
                <div class="large-12 small-12 columns">
                    <div id="human-body-map1"></div>
                </div>
            </div>            
            <div class="row">
                <div class="large-12 small-12 columns">
                    <div id="human-body-map2"></div>
                </div>
            </div>            
            <div class="row" style="margin-top: 30px;">
                <div class="large-12 small-12 columns">
                    <div id="mark-legend" style="border: 1px solid gray; border-radius: 5px; padding: 15px; font-size: 14px;">
                        <div class="row">
                            <div class="large-4 small-4 columns" style="padding: 2px;">
                                <img src="/assets/img/marker-red.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Adhesion
                            </div>
                            <div class="large-4 small-4 columns" style="padding: 2px;">
                                <img src="/assets/img/marker-orange.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Pain
                            </div>
                            <div class="large-4 small-4 columns" style="padding: 2px;">                                
                                <img src="/assets/img/marker-yellow.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Lack of ROM
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-4 small-4 columns" style="padding: 2px;">                                
                                <img src="/assets/img/marker-green.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Trigger Points
                            </div>
                            <div class="large-4 small-4 columns" style="padding: 2px;">                                
                                <img src="/assets/img/marker-blue.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Inflammation
                            </div>
                            <div class="large-4 small-4 columns" style="padding: 2px;">                                
                                <img src="/assets/img/marker-darkblue.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Typical Tender Point
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-4 small-4 columns" style="padding: 2px;">                                
                                <img src="/assets/img/marker-violet.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Spasm
                            </div>
                            <div class="large-8 small-8 columns" style="padding: 2px;">                                
                                <img src="/assets/img/marker-black.png" style="width: 30px; height: 30px;"/>&nbsp;&nbsp;Common Hypertonic Areas
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>    
    <?php echo form_close(); ?>    

    <div id="pain-description" style="display: none;">
        <table>
            <tr>
                <td width="25%">
                    <span>Modality:</span>
                </td>
                <td width="75%">
                    <select id="modality-selection" name="modality-selection">
                        <option value="0"> Adhesion</option>
                        <option value="1"> Pain</option>
                        <option value="2"> Lack of ROM</option>
                        <option value="3"> Trigger Points</option>
                        <option value="4"> Inflammation</option>
                        <option value="5"> Typical Tender Point</option>
                        <option value="6"> Spasm</option>
                        <option value="7"> Common Hypertonic Areas</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button id="save" class="modality-selection-button">Save</button>&nbsp;&nbsp;
                    <button id="discard" class="modality-selection-button">Discard</button>&nbsp;&nbsp;
                    <button id="delete" class="modality-selection-button" type="submit">Delete</button>
                </td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">

    var locations = [];
    var lcount = 0;

    var isDisplayed = false;
    var humanFrontLocs = [[111, 0], [158, 0], [168, 50], [158, 92], [239, 140], [263, 334], [223, 342], [233, 290], [197, 204], [204, 366],
        [206, 595], [159, 591], [134, 361], [101, 594], [59, 591], [59, 361], [73, 205], [38, 334], [0, 338], [34, 131],
        [109, 93], [95, 40], [111, 0]];
    var humanBackLocs = [[408, 0], [442, 26], [437, 79], [511, 119], [550, 345], [512, 346], [481, 199], [458, 235], [476, 378], [475, 592],
        [436, 597], [407, 366], [380, 599], [335, 596], [338, 354], [367, 223], [340, 196], [304, 347], [270, 344], [279, 219], [312, 111],
        [385, 76], [375, 21], [408, 0]];

    var markedIcons = [false, false, false, false, false, false, false];
    var selectedMarker = null;

    $(document).ready(function () {

        var isMarkerHit = false;
        var bodyMapSelected = 0;
        var isNew = false;

        $('#human-body-map1, #human-body-map2').click(function (e) {
            if (isMarkerHit) {
                isMarkerHit = false;
                return;
            }
            if ($(this).attr("id") === "human-body-map1") {
                bodyMapSelected = 1;
            } else {
                bodyMapSelected = 2;
            }

            var t = e.pageY - 15;
            var l = e.pageX - 5;
            lcount += 1;
            var id = "pain-marker-" + lcount;
            var infoId = "pain-info-" + lcount;
            selectedMarker = lcount - 1;
            var cornery = parseInt($(this).offset().top);
            var cornerx = parseInt($(this).offset().left);
            var thisElt = $(this);
            if (pointInPolygon(humanFrontLocs, e.pageX, e.pageY, cornerx, cornery)
                    || pointInPolygon(humanBackLocs, e.pageX, e.pageY, cornerx, cornery)) {
                var currentDate = new Date();
                var s = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear().toString();
                thisElt.append('<div id="' + id + '" class="pain-marker modality-marker-red"></div>');
                thisElt.append('<div id="' + infoId + '" class="pain-marker-info"></div>');

                $('#' + id).css("top", (t - cornery) + "px");
                $('#' + id).css("left", (l - cornerx) + "px");
                $('#' + infoId).css("top", (t - cornery + 10) + "px");
                $('#' + infoId).css("left", (l - cornerx + 45) + "px");

                locations.push({indexno: selectedMarker,
                    x: (l - cornerx), y: (t - cornery),
                    modalitytype: 0, mapno: bodyMapSelected});

                var tt = e.pageY - $('#header').height() + 10;
                var ll = e.pageX;
                if (ll + $('#pain-description').width() > $(window).width()) {
                    $('#pain-description').css("left", (ll - $('#pain-description').width()) + "px");
                } else {
                    $('#pain-description').css("left", ll + "px");
                }
                $('#pain-description').css("top", tt + "px");
                $('#modality-selection').val(0);
                $('#pain-description #delete').hide();
                $('#pain-description').show();

                $('#pain-info-' + (selectedMarker + 1)).html($('#modality-date').val());
                $('#pain-info-' + (selectedMarker + 1)).show();

                isNew = true;

                $('.pain-marker').unbind("click");
                $('.pain-marker').click(function (e) {
                    var id = $(this).attr("id");
                    var n = parseInt(id.substring(id.lastIndexOf("-") + 1)) - 1;
                    selectedMarker = n;
                    isMarkerHit = true;
                    isNew = false;
                    var m = getLocationsIndex(n);

                    var tt = e.pageY - $('#header').height() + 10;
                    var ll = e.pageX;
                    if (ll + $('#pain-description').width() > $(window).width()) {
                        $('#pain-description').css("left", (ll - $('#pain-description').width()) + "px");
                    } else {
                        $('#pain-description').css("left", ll + "px");
                    }
                    $('#pain-description').css("top", tt + "px");
                    $('#modality-selection').val(locations[m].modalitytype);
                    $('#pain-description #delete').show();
                    $('#pain-description').show();
                });
            }
        });

        $('#pain-description #save').click(function () {
            var m = getLocationsIndex(selectedMarker);
            locations[m].modalitytype = $('#modality-selection').val();

            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-red");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-green");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-orange");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-blue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-yellow");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-darkblue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-violet");

            var p = parseInt($('#modality-selection').val());
            if (p === 0) {
                $('#pain-info-' + (selectedMarker + 1)).html("Adhesion");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-red");
            } else if (p === 1) {
                $('#pain-info-' + (selectedMarker + 1)).html("Pain");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-orange");
            } else if (p === 2) {
                $('#pain-info-' + (selectedMarker + 1)).html("Lack of ROM");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-yellow");
            } else if (p === 3) {
                $('#pain-info-' + (selectedMarker + 1)).html("Trigger Points");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-green");
            } else if (p === 4) {
                $('#pain-info-' + (selectedMarker + 1)).html("Inflammation");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-blue");
            } else if (p === 5) {
                $('#pain-info-' + (selectedMarker + 1)).html("Typical Tender Point");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-darkblue");
            } else if (p === 6) {
                $('#pain-info-' + (selectedMarker + 1)).html("Spasm");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-violet");
            } else if (p === 7) {
                $('#pain-info-' + (selectedMarker + 1)).html("Common Hypertonic Areas");
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-black");
            }
            $('#pain-description').hide();
        });

        $('#pain-description #discard').click(function () {
            if (isNew) {
                $('#pain-description #delete').click();
            } else {
                $('#pain-description').hide();
            }
        });

        $('#pain-description #delete').click(function () {
            $('#pain-marker-' + (selectedMarker + 1)).remove();
            $('#pain-info-' + (selectedMarker + 1)).remove();
            locations.splice(selectedMarker, 1);
            $('#pain-description').hide();
        });

        $(window).resize(function () {
            $('#pain-description').hide();
        });

    });

    function getLocationsIndex(indexNo) {
        var retValue = null;
        for (var i = 0; i < locations.length; i++) {
            if (locations[i].indexno === indexNo) {
                retValue = i;
                break;
            }
        }
        return retValue;
    }

</script>