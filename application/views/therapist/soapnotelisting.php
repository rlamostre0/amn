<link rel="stylesheet" href="/assets/css/soapnote.css" />
<div id="soapnote-listing-section" class="large-12 columns">
    <div class="row " style="padding: 10px 0px 10px 0px;">        
        <div class="large-12 columns">
            <h4>SOAP Notes Listing</h4>
        </div>
    </div>
    <div class="row" style="padding: 25px;">
        <div class="large-12 columns">
            <table style="width: 100%;">
                <tr>
                    <th style="width: 32%;">CLIENT</th>
                    <th style="width: 20%;">DATE</th>
                    <th style="width: 20%;">BIRTH DATE</th>
                    <th style="width: 30%;">THERAPIST</th>
                    <th style="width: 8%;">&nbsp;</th>
                </tr>
                <?php foreach ($recordlist as $record) { ?>
                    <tr>
                        <td><?= $record["client_firstname"] . " " . $record["client_lastname"]; ?>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td>
                            <?= $record["therapist_firstname"] . " " . $record["therapist_lastname"]; ?>&nbsp;
                        </td>
                        <td>
                            <a href="#"><img src="/assets/img/view-document.png"/></a>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>Christopher Esteves&nbsp;</td>
                    <td>12/21/2014&nbsp;</td>
                    <td>11/27/78&nbsp;</td>
                    <td>Alexis Servy&nbsp;</td>
                    <td>
                        <a href="<?php echo site_url() ?>soapnote?id=6"><img src="/assets/img/view-document.png"/></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>