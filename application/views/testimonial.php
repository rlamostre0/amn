<link rel="stylesheet" href="/assets/css/testimonial.css" />
<input type="hidden" name="mode" value="<?php echo $mode; ?>"/>
<?php if ($mode == "") { ?>
    <div class = "row">
        <div class = "large-7 columns">
            <h4>Testimonials</h4>
        </div>
        <div class = "large-5 columns" style = "text-align: right; margin-top: 15px;">
            &nbsp;
            <?php
            if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) {
                ?>
                <a href="<?php echo site_url() ?>testimonial/add" style="margin-right: 40px;">Write a Review</a>
            <?php } ?>    
        </div>    
    </div>
    <?php foreach ($recordlist as $record) { ?>
        <div class="row" style="padding: 20px;">
            <div class="large-3 columns">
                <div class="row"> 
                    <div class="large-5 columns">
                        <img src="<?php echo "http://advancedmassagepractitioner.com/assets/img/photos/" . $record[0]['profilephoto']; ?>" style="width: 100px; height: 100px;">
                    </div>
                    <div class="large-7 columns" style="padding-left: 0px;">
                        <p class="testimonial-author">
                            <b><?= $record[0]["firstname"] . " " . $record[0]["lastname"]; ?></b>
                            <span><?= $record[0]["cemail"]; ?></span>
                            <span><?= $record[0]["datetime"]; ?></span>
                        </p> 
                    </div>
                </div>
            </div>    
            <div class="large-9 columns">
                <div class="large-12 small-12 columns">
                    <div class="row">
                        <div class="large-12 small-12 columns">
                            <blockquote class="bubble-review"> 
                                <h4><?= $record[0]["title"]; ?></h4>
                                <p><?= $record[0]["content"]; ?></p>
                            </blockquote>             
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 small-12 columns" style="text-align: left;">
                            Was this review helpful?&nbsp;&nbsp;
                            <a href="#">Yes</a>&nbsp;&nbsp;<span><?= $record[0]["yescount"]; ?></span>&nbsp;&nbsp;
                            <a href="#">No</a>&nbsp;&nbsp;<span><?= $record[0]["nocount"]; ?></span>&nbsp;&nbsp;
                            <a href="#">Report</a>
                        </div>
                        <div class="large-6 small-12 columns" style="text-align: right;">
                            <a href="<?php echo site_url() ?>testimonial/reply?id=<?= $record[0]['id']; ?>">Reply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php foreach ($record[1] as $comment) { ?>
            <div class="row" style="padding: 20px;">
                <div class="large-4 columns">
                    <p class="testimonial-author" style="float: right;">
                        <?php if ($comment["customerid"] != null && intval($comment["customerid"]) > 0) { ?>
                            <b><?= $comment["firstname"] . " " . $comment["lastname"]; ?></b>
                            <span style="display: block;"><?= $comment["cemail"]; ?></span>
                            <span><?= $comment["datetime"]; ?></span>
                        <?php } else { ?>
                            <b><?= $comment["name"]; ?></b>
                            <span style="display: block;"><?= $comment["email"]; ?></span>
                            <span><?= $comment["datetime"]; ?></span>
                        <?php } ?>
                    </p>
                </div>
                <div class="large-8 columns">                    
                    <blockquote class="bubble-comment"> 
                        <h4><?= $comment["title"]; ?></h4>
                        <p><?= $comment["content"]; ?></p>
                    </blockquote>             
                </div>
            </div>
            <?php
        }
    }
}

if ($mode == "add") {
    if (isset($adderrors)) {
        ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $adderrors; ?>
            </div>            
        </div>
        <?php
    }
    echo form_open('testimonial/add');
    ?>
    <div class="row">
        <div class="large-12 columns" style="margin-top: 20px;">
            <h4>Create New Review</h4>
        </div>    
    </div>
    <div class="row">
        <div class="large-3 columns">            
            Review Title
        </div>    
        <div class="large-9 columns">
            <input type="text" name="title"/>
        </div>    
    </div>
    <div class="row">
        <div class="large-3 columns">            
            Comment
        </div>    
        <div class="large-9 columns">
            <textarea name="content" rows="4"></textarea>
        </div>    
    </div>
    <div class="row">
        <div class="large-12 columns" style="margin-top: 20px;">
            <button type="submit">Submit</button>&nbsp;&nbsp;&nbsp;
            <button type="button" onclick="parent.location = '<?php echo site_url() ?>testimonial'">Cancel</button>
        </div>    
    </div>
    <?php
    echo form_close();
}

if ($mode == "reply") {
    if (isset($replyerrors)) {
        ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $replyerrors; ?>
            </div>            
        </div>
        <?php
    }
    echo form_open('testimonial/reply');
    ?>
    <input type="hidden" name="parentid" value="<?= $reviewId; ?>"/>
    <div class="row">
        <div class="large-3 columns" style="margin-top: 20px;">
            <div class="row"> 
                <div class="large-5 columns">
                    <img src="http://placehold.it/100x100">
                </div>
                <div class="large-7 columns" style="padding-left: 0px;">
                    <p class="testimonial-author">
                        <?= $review["firstname"] . " " . $review["lastname"]; ?>
                        <span><?= $review["email"]; ?></span>
                        <span>&nbsp;</span>
                        <span><?= $review["datetime"]; ?></span>
                    </p> 
                </div>
            </div>
        </div>    
        <div class="large-9 columns" style="margin-top: 20px;">
            <blockquote class="bubble-review"> 
                <h4><?= $review["title"]; ?></h4>
                <p><?= $review["content"]; ?></p>
            </blockquote>             
        </div>    
    </div>
    <div class="row">
        <div class="large-12 columns">
            <h4>Your Comment</h4>
        </div>    
    </div>

    <?php
    if (!($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0))) {
        ?>
        <div class="row">
            <div class="large-3 columns">            
                Name
            </div>    
            <div class="large-9 columns">
                <input type="text" name="name"/>
            </div>    
        </div>
        <div class="row">
            <div class="large-3 columns">            
                Email
            </div>    
            <div class="large-9 columns">
                <input type="text" name="email"/>
            </div>    
        </div>
    <?php } ?>    
    <div class="row">
        <div class="large-3 columns">            
            Comment Title
        </div>    
        <div class="large-9 columns">
            <input type="text" name="title"/>
        </div>    
    </div>
    <div class="row">
        <div class="large-3 columns">            
            Your Comment
        </div>    
        <div class="large-9 columns">
            <textarea name="content" rows="4"></textarea>
        </div>    
    </div>
    <div class="row">
        <div class="large-12 columns" style="margin-top: 20px;">
            <button type="submit">Submit</button>&nbsp;&nbsp;&nbsp;
            <button type="button" onclick="parent.location = '<?php echo site_url() ?>testimonial'">Cancel</button>
        </div>    
    </div>
    <?php
    echo form_close();
}


