<div class="row">
    <div class="large-12 columns">
        <h4>Frequently Asked Question</h4>
        <div class="faq-wrap">
            <dl class="accordion" data-accordion>  
                <dd> 
                    <a href="#panel1" id="panel1">Why Is Massage good for you?</a> 
                    <div id="panel1" class="content"> The most important benefit of massage is the stimulation of the lymphatic system. You will sweat out more metabolic waste than usual, which helps detoxify the body.<br/><br/>

Massage is also good for treating the soft tissue and muscular system, which affects the skeletal and nervous system. When trigger points grow larger you lose range of motion and your fascia tissue will start harden. This affects your gait, posture, and overall physical appearance, but it can be corrected with the proper massage modalities. These include Muscle Exonero Technique, Medical Massage, Neuromuscular Therapy, Orthopedic massage, Rolfing, etc.<br/><br/>

Massage releases feel-good chemicals and hormones, tilting the balance away form stress and toward relaxation and healing.<br/><br/>

Massage is absolutely one of the best things you can do to balance yourself in this high-stress and toxic world.

                    </div> 
                </dd> 
                <dd> 
                    <a href="#panel2" id='panel2'>Health Benefits of Massage?</a> 
                    <div id="panel2" class="content"> Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    </div> 
                </dd> 
                <dd> 
                    <a href="#panel3" id='panel3'>The truth about the massage industry</a> 
                    <div id="panel3" class="content"> Only a select few individuals are educated in advanced massage modalities. Due to lack of time, the majority of massage schools only prepare therapists to pass the national board exam and teach spa massage modalities.  It is left up to the individual after their massage school education to pursue the more advanced massage modalities. Unfortunately only about 5% - 20% take the initiative to further educate themselves and 5% - 10% master Medical Massage.<br/><br/>

Medical Massage education takes significantly more time to master than the typical standard Spa education. The massage industry and large franchises typically pay about 30% - 40% of the session price to the therapist and encourage unnecessary tips. Advanced Massage Network is proud pay out a minimum of 70% to our therapists, which is why we are able to recruit the very best the industry has to offer in your area.<br/><br/>

Advanced Massage Network’s goal is to innovate and change the stagnant massage industry by treating the client with the appropriate modalities customized to their problems. Advanced Massage Network consists of the most elite independent massage therapists in the nation. Check out our innovative scheduler, which speeds up the booking process and effectively suggests the most appropriate modality for your issue.<br/><br/>
                    </div> 
                </dd> 
                <dd> 
                    <a href="#panel4" id='panel4'>How many massage therapists practice Medical Massage, and how much does it usually cost?</a> 
                    <div id="panel4" class="content"> Medical massage is only practiced by 20% of all massage therapists in the United States. Only a select few know more advanced modalities designed to treat acute and chronic pain.<br/><br/>

All of the Advanced Massage Network therapists are trained in the Muscle Exonero Technique, an advanced neuromuscular therapy technique that can begin to alleviate trigger points in one session.<br/><br/>

These advanced medical massage services typically cost between $120 - $200 for a 90 minute session.<br/><br/>
                    </div> 
                </dd> 
                <dd> 
                    <a href="#panel5" id='panel5'>How many therapist practice Medical Massage</a> 
                    <div id="panel5" class="content "> Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    </div> 
                </dd> 
                <dd> 
                    <a href="#panel6" id='panel6'>How many therapist practice Medical Massage</a> 
                    <div id="panel6" class="content"> Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    </div> 
                </dd> 
                <dd> 
                    <a href="#panel7" id='panel7'>How many therapist practice Medical Massage</a> 
                    <div id="panel7" class="content"> Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    </div> 
                </dd> 
            </dl>
        </div>
    </div>
</div>
<script type='text/javascript'>
var url = document.URL;
  url = url.split('#')[1]
$(document).ready(function(){
    $('a#'+url).trigger('click');
});
</script>
<script>
$(document).foundation('accordion');
</script>