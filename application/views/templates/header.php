<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>
            <?php
            if (isset($meta_title)) {
                echo $meta_title . " | ";
            }
            ?>Advanced Massage Network 
        </title>
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="description" content="<?php echo $meta_description ?>">
        <meta name="robots" content="index,nofollow"> 
        <link rel="stylesheet" href="/assets/css/foundation.css" />
        <link rel="stylesheet" href="/assets/css/foundation-icons/foundation-icons.css" /> 
        <link rel="stylesheet" href="/assets/css/fullcalendar.min.css" />
        <link rel="stylesheet" href="/assets/css/main.css" />
        
        <link rel="stylesheet" href="/assets/css/pikaday.css" />
				<link rel="stylesheet" type="text/css" href="/assets/js/aciTree/css/aciTree.css" media="all">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
        <script src="/assets/js/vendor/jquery.js"></script>
        <script src="/assets/js/foundation.min.js"></script>
        <script src="/assets/js/foundation/foundation.orbit.js"></script>
        <script src="/assets/js/vendor/modernizr.js"></script>
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/moment.min.js"></script>    
        <script src="/assets/js/fullcalendar.min.js"></script>   
        <script src="/assets/js/pikaday.js"></script>    
				<script src="/assets/js/aciTree/js/jquery.aciPlugin.min.js" type="text/javascript"></script>
				<script type="text/javascript" src="/assets/js/aciTree/js/jquery.aciTree.min.js"></script>				
        <script type="text/javascript">

            $(document).ready(function () {

                $(document).foundation();
                $('#slider').show();

                $('#customer-login, #customer-password').keydown(function (e) {
                    if (e.keyCode === 13) {
                        $('#login-form').submit();
                    }
                });

                $('#controlpanel-toggle').click(function () {
                    if ($('#control-panel').is(":visible")) {
                        $('#control-panel').hide();
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url() ?>customer/getcontrolpanel",
                            dataType: "json",
                            data: {
                            },
                            success: function (data) {
                                $('#info-table #name').text(data.customer.firstname + " " + data.customer.lastname);
                                $('#info-table #age').text();
                                if (data.customer.gender === 1) {
                                    $('#info-table #gender').text("Male");
                                } else {
                                    $('#info-table #gender').text("Female");
                                }
                                if (data.customer.birth_date !== "" && data.customer.birth_date !== "0000-00-00") {
                                    $('#info-table #birthday').text(data.customer.birth_date);
                                }
                                $('#info-table #address').text(data.customer.address_street + ", " + data.customer.address_city + ", " + data.customer.address_state
                                        + " " + data.customer.address_zip);
                                $('#info-table #email').text(data.customer.email);
                                $('#info-table #contact-home').text(data.customer.contact_home);
                                $('#info-table #contact-work').text(data.customer.contact_work);
                                $('#control-panel').css("left", $('.global-nav-top-bar').offset().left + "px");
                                $('#control-panel').show();
                            }
                        });
                    }
                });
                $('#therapist-controlpanel-toggle').click(function () {
                    if ($('#therapist-control-panel').is(":visible")) {
                        $('#therapist-control-panel').hide();
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url() ?>customer/getcontrolpanel",
                            dataType: "json",
                            data: {
                            },
                            success: function (data) {
                                $('#info-table #name').text(data.customer.firstname + " " + data.customer.lastname);
                                $('#info-table #age').text();
                                if (data.customer.gender === 1) {
                                    $('#info-table #gender').text("Male");
                                } else {
                                    $('#info-table #gender').text("Female");
                                }
                                if (data.customer.birth_date !== "" && data.customer.birth_date !== "0000-00-00") {
                                    $('#info-table #birthday').text(data.customer.birth_date);
                                }
                                $('#info-table #address').text(data.customer.address_street + ", " + data.customer.address_city + ", " + data.customer.address_state
                                        + " " + data.customer.address_zip);
                                $('#info-table #email').text(data.customer.email);
                                $('#info-table #contact-home').text(data.customer.contact_home);
                                $('#info-table #contact-work').text(data.customer.contact_work);
                                $('#therapist-control-panel').css("left", $('.global-nav-top-bar').offset().left + "px");
                                $('#therapist-control-panel').show();
                            }
                        });
                    }
                });

                $(window).resize(function () {
                    $('#control-panel').css("left", $('.global-nav-top-bar').offset().left + "px");
                }).resize();

            });

        </script>
    </head>
    <body>
        <div id="header">
            <div class="row" style="background-color: transparent;">
                <div class="large-12 columns" id="top-bar">
                    <div class="util-bar left-col logo">
                        <a href="<?php echo site_url() ?>pages/home"><img src="/assets/img/logo2.png" alt="logo" title="Advance Massage Network"></a>  
                        <!--<div class="links">
                            <ul>
                                <li><a href="<?php echo site_url() ?>blog">Our Blog</a></li>
                                <li><a href="<?php echo site_url() ?>testimonial">Client Reviews</a></li>
                            </ul>
                        </div>-->
                    </div>
                    <div class="util-bar right-col">
                        <div class="utilities">
                            <div class="social-icons">
                                <ul>
                                    <li><a href="http://advancedmassagenetwork.blogspot.com/" class="socialnet socialnet-blogger"></a></li>
                                    <li><a href="https://www.facebook.com/AdvancedMassageNetwork" class="socialnet socialnet-facebook"></a></li>
                                    <li><a href="https://plus.google.com/117538830858033078555" class="socialnet socialnet-googleplus"></a></li>
                                    <li><a href="https://www.linkedin.com/company/advanced-massage-network-llc-" class="socialnet socialnet-linkedin"></a></li>
                                    <li><a href="#" class="socialnet socialnet-mashable"></a></li>
                                    <li><a href="https://www.pinterest.com/advmassagenet/" class="socialnet socialnet-pinterest"></a></li>
                                    <li><a href="#" class="socialnet socialnet-stumbleupon"></a></li>
                                    <!-- <li><a href="#" class="socialnet socialnet-thumbtacks"></a></li> -->
                                    <li><a href="https://twitter.com/massagenet" class="socialnet socialnet-twitter"></a></li>
                                    <!-- <li><a href="#" class="socialnet socialnet-yellowpages"></a></li> -->
                                    <!-- <li><a href="#" class="socialnet socialnet-yelp"></a></li> -->
                                    <li><a href="#" class="socialnet socialnet-youtube"></a></li>
                                </ul>
                            </div>
                            <div class="links" style="text-align: right;">
                                <nav data-topbar>
                                    <section class="top-link-section">
                                        <ul>
                                            <li><a href="<?php echo site_url() ?>pages/underconstruction">Own a Franchise for free!</a></li>
                                            <li><a href="<?php echo site_url() ?>pages/underconstruction">Help</a></li>
                                            <li><a href="<?php echo site_url() ?>sitemap">Site Map</a></li>
                                            <li><a href="<?php echo site_url() ?>blog">Our Blog</a></li>
                                            <li><a href="<?php echo site_url() ?>testimonial">Client Reviews</a></li>
                                        </ul>
                                    </section>
                                </nav>
                            </div>
                        </div>
                        <div class="phone">
                            <h3>1-844-MED-MASS</h3>
                            <h3><span>633-6277</span></h3>
                        </div>
                    </div>

                </div>
            </div>
            <?php if (!$is_hide_header_slide) { ?>
                <div class="row">
                    <div class="large-8 columns" id="slider" style="display: none;">
                        <ul class="example-orbit" data-orbit> 
                            <li> 
                                <img src="/assets/img/slide1.jpg" alt="slide 1" />
                            </li> 
                            <li> 
                                <img src="/assets/img/slide2.jpg" alt="slide 2" />  
                            </li> 
                            <li> 
                                <img src="/assets/img/slide3.jpg" alt="slide 3" /> 
                            </li> 
                        </ul>
                    </div>
                    <div id="ads-section" class="large-4 columns">
                        <img src="/assets/img/pricinginfo.png" class="pricinginfo"/>
                    </div>
                </div>
            <?php } ?>
            <div class="row global-nav-top-bar">
                <div class="large-12 columns">
                    <nav class="top-bar" data-topbar>
                        <ul class="title-area">
                            <li class="name"></li>
                            <li class="toggle-topbar menu-icon"><a href=""><span>Menu</span></a></li>
                        </ul> 
                        <section class="top-bar-section">
                            <ul class="left">
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pages/home">
                                        <img src="/assets/img/welcome-gn.png" border="0"/><span>Welcome</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pages/modalities">
                                        <img src="/assets/img/modality-gn.png" border="0"/><span>Massage Modalities</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>location">
                                        <img src="/assets/img/location-gn.png" border="0"/><span>Locations</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pricingpage/index">
                                        <img src="/assets/img/pricemembership-gn.png" border="0"/><span>Pricing &amp; Memberships</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pages/gift">
                                        <img src="/assets/img/gift-gn.png" border="0"/><span>Gift Certificates</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="right">
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pages/book">
                                        <span style="margin-left: 0px; margin-right: 4px;">Book Now!</span>
                                        <img src="/assets/img/book-gn.png" border="0" style="margin-right: 0px;"/>
                                    </a>
                                </li>
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pages/join">
                                        <span>Join Now!</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="global-nav-button" href="<?php echo site_url() ?>pages/join">
                                        <span>Contact Us</span>
                                    </a>
                                </li>
                            </ul>
                        </section>
                    </nav>
                </div>
            </div>
            <div class="row global-nav-top-bar-heading">
                <div class="large-9and0point12 small-12 columns">
                    <div class="row">
                        <div class="large-9 hide-for-medium-down columns" style="text-align: right; padding: 0px;">
                            &nbsp;
                        </div>
                        <div class="large-3 small-12 columns" style="text-align: right; padding: 0px;">
                            <div class="global-nav-contactus-section">
                                <p class='phone2'>
                                    <img src="/assets/img/callus.png"/>
                                    Contact Us<br/>1-844-MED-MASS<br/>
                                     1-844-<span>633-</span>6277
                                </p>
                            </div>
                        </div>                        
                    </div>
                    <div class="row" style="margin-bottom: -10px;">
                        <div class="large-12 small-12 small-centered medium-centered columns" style="margin-left: 0px; padding-left: 0px;">
                            <div class="global-nav-contact-mainmenu-section">
                                <ul>
                                    <li>
                                        <a href="<?php echo site_url() ?>pages/first_visit" class="global-nav-menu-button" style="min-width: 142px;">
                                            <span>Your First Visit</span>
                                            <img src="/assets/img/modality-icon.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url() ?>testimonial" class="global-nav-menu-button" style="min-width: 128px;">
                                            <span>Testimonials</span>
                                            <img src="/assets/img/modality-icon.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url() ?>pages/faq" class="global-nav-menu-button" style="min-width: 83px;">
                                            <span>FAQs</span>
                                            <img src="/assets/img/modality-icon.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url() ?>blog" class="global-nav-menu-button" style="min-width: 78px;">
                                            <span>Blog</span>
                                            <img src="/assets/img/modality-icon.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url() ?>blog" class="global-nav-menu-button" style="min-width: 268px;">
                                            <span>Pains and Structural Body Problems</span>
                                            <img src="/assets/img/modality-icon.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url() ?>pricingpage/click_about_us" class="global-nav-menu-button" style="min-width: 50px;">
                                            <span>About Us</span>
                                            <img src="/assets/img/modality-icon.png"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="large-0point12 hide-for-medium-down columns" style="padding: 0px; margin: 0px;">
                    <div class="v-separator columns"></div>
                </div>                    
                <div class="large-2and0point88 small-12 columns" style="padding-left: 0px; margin-left: 0px;">
                    <div class="global-nav-user-login-section">
                        <?php if ($this->session->userdata('is_admin') != null && $this->session->userdata('is_admin') == true) { ?>                        
                            <div class="row" style="color: #ffffff; margin: 0px;">
                                <div class="large-5 columns" style="padding: 8px 8px 8px 13px;">
                                    <?php if ($this->session->userdata('currentcustomerprofilephoto') != "") { ?>
                                        <img src="<?php echo "http://advancedmassagepractitioner.com/assets/img/photos/" . $this->session->userdata('currentcustomerprofilephoto'); ?>" style="width: 90px; height: 90px;">
                                    <?php } else { ?>
                                        <img src="http://advancedmassagepractitioner.com/assets/img/nophoto.gif" style="width: 90px; height: 90px;">
                                    <?php } ?>    
                                </div>
                                <div class="large-7 columns" style="padding: 10px 10px 18px 10px;">
                                    Welcome <?= $this->session->userdata('user_name'); ?>&nbsp;&nbsp;
                                    <br/><br/>
                                    <a href="<?php echo site_url() ?>customer/uploadimages" style="display: block; margin-bottom: 5px;">Setting</a>
                                    <a href="<?php echo site_url() ?>customer/logout" style="display: block; margin-bottom: 5px;">Logout</a>  
                                </div>
                            </div>                        
                        <?php } else if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) { ?>                        
                            <div class="row" style="color: #ffffff; margin: 0px;">
                                <div class="large-5 columns" style="padding: 8px 8px 8px 13px;">
                                    <?php if ($this->session->userdata('currentcustomerprofilephoto') != "") { ?>
                                        <img src="<?php echo "http://advancedmassagepractitioner.com/assets/img/photos/" . $this->session->userdata('currentcustomerprofilephoto'); ?>" style="width: 90px; height: 90px;">
                                    <?php } else { ?>
                                        <img src="http://advancedmassagepractitioner.com/assets/img/nophoto.gif" style="width: 90px; height: 90px;">
                                    <?php } ?>    
                                </div>
                                <div class="large-7 columns" style="padding: 10px 10px 18px 10px;">
                                    Welcome <?= $this->session->userdata('currentcustomerfirstname'); ?>&nbsp;&nbsp;
                                    <br/><br/>
                                    <a href="<?php echo site_url() ?>customer/uploadimages" style="display: block; margin-bottom: 5px;">Setting</a>
                                    <a href="<?php echo site_url() ?>customer/logout" style="display: block; margin-bottom: 5px;">Logout</a>  
                                </div>
                            </div>                        
                        <?php } else { ?>
                            <?php
                            echo form_open('customer/login', array('id' => 'login-form', 'style' => 'margin: 0px;'));
                            ?>
                            <div class="row">
                                <div class="large-8 small-8 columns" style="text-align: left; margin-bottom: 4px;">
                                    Member Login
                                </div>
                                <div class="large-4 small-4 columns" style="text-align: right; margin-bottom: 4px;">
                                    <a href="<?php echo site_url() ?>customer/signup" style="font-style: italic; color: rgb(33, 175, 228);font-size:14px;">Sign up!</a>
                                </div>
                            </div>
                            <?php if (isset($loginerrors)) { ?>
                                <div class="row">
                                    <div class="large-12 columns" style="margin-top: 10px; margin-bottom: 10px;">
                                        <?php echo $loginerrors; ?>
                                    </div>            
                                </div>
                            <?php }
                            ?>
                            <div class="row">
                                <div class="large-12 columns">
                                    <table>
                                        <tr>
                                            <td width="86%">
                                                <input type="text" id="customer-login" name="customerloginid"/>
                                            </td>
                                            <td width="13%">
                                                <img src="/assets/img/userid.png" style="display: block; margin-left: 7px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="password" id="customer-password" name="customerpassword"/>
                                            </td>
                                            <td>
                                                <img src="/assets/img/password.gif" style="margin-left: 10px; display: block;"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row global-nav-bottom-bar">
                <?php if ($this->session->userdata('is_admin') != null && $this->session->userdata('is_admin') == true) { ?>
                    <div class="large-12 small-12 columns" style="text-align: right;background-color: rgba(47, 47, 47, 0.5)">
                        <a id="therapist-controlpanel-toggle" href="javascript: void(0);">
                            <img src="/assets/img/toggle.png"/>
                        </a>
                    </div>                    
                <?php }else if ($this->session->userdata('currentcustomerid') != null && $this->session->userdata('currentcustomerid') != "" && intval($this->session->userdata('currentcustomerid') > 0)) { ?>
                    <div class="large-12 small-12 columns" style="text-align: right;">
                        <a id="controlpanel-toggle" href="javascript: void(0);">
                            <img src="/assets/img/toggle.png"/>
                        </a>
                    </div>                    
                <?php } ?>
            </div>
        </div>            
        <div id="control-panel" class="row" style="display: none;">
            <div class="row">
                <div class="large-12 small-12 columns">
                    <a href="#" class="help"></a>
                </div>
            </div>
            <div class="row">
                <div class="large-3and0point75 small-12 columns">
                    <span class="topic-title-a">VIP Points System</span>
                    <span class="topic-title-b">Earn Free Services</span>
                    <div class="pointsystem">                    
                    </div>
                </div>
                <div class="large-2and0point25 small-12 columns">
                    <span class="topic-title-a">Your Favorite Locations</span>
                    <select multiple class="favoritelocation">
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                        <option value="">Location Date</option>
                    </select>
                    <a href="#" class="rockbotcom"></a>
                    <span class="topic-title-a">Preferred Practitioner</span>
                    <select multiple class="preferredpractitioner">
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                        <option value="">Practitioner</option>
                    </select>
                    <a href="#" class="button-b" style="width: 300px; margin-top: 10px;">Become an Affiliate and Earn Money!</a>
                </div>
                <div class="large-3 small-12 columns">
                    <span class="topic-title-a">Talk to Our Operator</span>
                    <div class="operatorconversation"></div>
                    <input class="operatorconversation-talk"/>
                    <a href="<?php echo site_url() ?>" class="button-a" style="width: 100px; margin: 8px 0px 0px 25px;">Send</a>
                    <a href="<?php echo site_url() ?>" class="button-a" style="width: 177px; margin: 8px 0px 0px 25px;">Update Payment Info</a>
                    <a href="#" class="getafree"></a>
                </div>
                <div class="large-3 small-12 columns">
                    <div class="client-info">
                        <h1>Client Information</h1>
                        <img id="photo" src="<?php echo "http://advancedmassagepractitioner.com/assets/img/photos/" . $this->session->userdata('currentcustomerprofilephoto'); ?>" alt=""/>
                        <table id="info-table">
                            <tr>
                                <td valign="top">Name</td>
                                <td valign="top">
                                    <span id="name"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Age</td>
                                <td valign="top">
                                    <span id="age"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Gender</td>
                                <td valign="top">
                                    <span id="gender"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Birthday</td>
                                <td valign="top">
                                    <span id="birthday"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Address</td>
                                <td valign="top">
                                    <span id="address"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Email</td>
                                <td valign="top">
                                    <span id="email"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Contact</td>
                                <td valign="top"></td>
                            </tr>
                            <tr>
                                <td valign="top"></td>
                                <td valign="top">Home</td>
                                <td valign="top">
                                    <span id="contact-home"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"></td>
                                <td valign="top">Work</td>
                                <td valign="top">
                                    <span id="contact-work"></span>
                                </td>
                            </tr>
                        </table>
                        <a href="<?php echo site_url() ?>" class="button-a" style="width: 100%; margin-bottom: 5px;">Update Contact Info</a>
                        <a href="<?php echo site_url() ?>" class="button-a" style="width: 100%; margin-bottom: 5px;">Update Medical History</a>
                        <a href="<?php echo site_url() ?>" class="button-a" style="width: 100%; margin-bottom: 5px;">Book an Appointment</a>
                    </div>
                    <a href="#" class="button-b" style="width: 400px; float: right; margin-top: 10px;">Recommend a Healthcare Practitioner for a Free Franchise</a>
                </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns" style="text-align: center;">
                    <div class="previous-next">
                        <a href="#" class="previous"></a>
                        <a href="#" class="next"></a>
                    </div>
                </div>
            </div>
        </div>
	<div id="therapist-control-panel" class="row" style="display: none;">
		<div class="row">
				<div class="large-12 small-12 columns">
						<a href="#" class="help"></a>
				</div>
		</div>
		<div class="row">
				<div class="large-3and0point75 small-12 columns">
						<span class="topic-title-a">VIP Points System</span>
						<span class="topic-title-b">Earn Free Services</span>
						<div class="pointsystem">                    
						</div>
				</div>
				<div class="large-2and0point25 small-12 columns">
						<span class="topic-title-a">Your Favorite Locations</span>
						<select multiple class="favoritelocation">
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
							<option value="">Location Date</option>
						</select>
						<a href="#" class="rockbotcom"></a>
						<span class="topic-title-a">Preferred Practitioner</span>
						<select multiple class="preferredpractitioner">
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
								<option value="">Practitioner</option>
						</select>
						<a href="#" class="button-b" style="width: 300px; margin-top: 10px;">Become an Affiliate and Earn Money!</a>
				</div>
				<div class="large-3 small-12 columns">
						<span class="topic-title-a">Talk to Our Operator</span>
						<div class="operatorconversation"></div>
						<input class="operatorconversation-talk"/>
						<a href="<?php echo site_url() ?>" class="button-a" style="width: 100px; margin: 8px 0px 0px 25px;">Send</a>
						<a href="<?php echo site_url() ?>" class="button-a" style="width: 177px; margin: 8px 0px 0px 25px;">Update Payment Info</a>
						<a href="#" class="getafree"></a>
				</div>
				<div class="large-3 small-12 columns">
						<div class="client-info">
								<h1>Therapist Control Panel</h1>
								<img id="photo" src="<?php echo "http://advancedmassagepractitioner.com/assets/img/photos/" . $this->session->userdata('currentcustomerprofilephoto'); ?>" alt=""/>
								<table id="info-table">
										<tr>
												<td valign="top">Name</td>
												<td valign="top">
														<span id="name"></span>
												</td>
										</tr>
										<tr>
												<td valign="top">Age</td>
												<td valign="top">
														<span id="age"></span>
												</td>
										</tr>
										<tr>
												<td valign="top">Gender</td>
												<td valign="top">
														<span id="gender"></span>
												</td>
										</tr>
										<tr>
												<td valign="top">Birthday</td>
												<td valign="top">
														<span id="birthday"></span>
												</td>
										</tr>
										<tr>
												<td valign="top">Address</td>
												<td valign="top">
														<span id="address"></span>
												</td>
										</tr>
										<tr>
												<td valign="top">Email</td>
												<td valign="top">
														<span id="email"></span>
												</td>
										</tr>
										<tr>
												<td valign="top">Contact</td>
												<td valign="top"></td>
										</tr>
										<tr>
												<td valign="top"></td>
												<td valign="top">Home</td>
												<td valign="top">
														<span id="contact-home"></span>
												</td>
										</tr>
										<tr>
												<td valign="top"></td>
												<td valign="top">Work</td>
												<td valign="top">
														<span id="contact-work"></span>
												</td>
										</tr>
								</table>
								<a href="<?php echo site_url() ?>" class="button-a" style="width: 100%; margin-bottom: 5px;">Update Contact Info</a>
								<a href="<?php echo site_url() ?>" class="button-a" style="width: 100%; margin-bottom: 5px;">Update Medical History</a>
								<a href="<?php echo site_url() ?>" class="button-a" style="width: 100%; margin-bottom: 5px;">Book an Appointment</a>
						</div>
						<a href="#" class="button-b" style="width: 400px; float: right; margin-top: 10px;">Recommend a Healthcare Practitioner for a Free Franchise</a>
				</div>
		</div>
		<div class="row">
				<div class="large-12 small-12 columns" style="text-align: center;">
						<div class="previous-next">
								<a href="#" class="previous"></a>
								<a href="#" class="next"></a>
						</div>
				</div>
		</div>
	</div>
        <div id="content">

