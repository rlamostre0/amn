 </div>
<div id="footer">
    <div class="row">
        <div class="divider no-border"></div>
    </div>
    <div class="row">
        <hr/>
        <div class="quickLinks">
            <div class="large-12 columns">
                <h3 class="italic"> Quick Links</h3>
                <div class="small-4  columns">  
                    <div class="box military">
                        <h5>Massage Discounts</h5>     
                        <ul>
                            <li><a href="<?php site_url()?>/pricingpage/click_membership"> Senior Citizen Discount </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_membership"> Military Discount </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_membership"> Healthcare Practitioner Discount </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_membership"> Student Discount </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_vip_point_system"> VIP Point System </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-4  columns">  
                    <div class="box military">
                        <h5>Affiliate Programs</h5> 
                        <ul>
                            <li><a href="<?php site_url()?>/pricingpage/click_become_a_chair_massage_affiliate"> Blogger Affiliate </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_become_a_chair_massage_affiliate"> Healthcare Practitioner </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_become_a_chair_massage_affiliate"> Hotel Concierge </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-4  columns">  
                    <div class="box military">
                        <h5>Types of massage</h5> 
                        <ul>
                            <li>Massage for Pain Relief</li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel1"> Medical Massage </a></li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel1"> Orthopedic Massage </a></li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel4"> Neuromuscular Therapy </a></li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel1"> Muscle Exonero Technique </a></li>
                            <li><a href="#"> Sports Massage </a></li>
                            <li><a href="#"> Ayurvedic Massage </a></li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel7"> Thai Massage </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-4  columns">  
                    <div class="box military">
                        <ul>
                            <li>Spa Massage for Relaxation</li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel2"> Swedish Massage </a></li>
                            <li><a href="<?php site_url()?>/pages/modalities#panel2"> Deep Tissue Massage </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-4  columns">  
                    <div class="box military">
                        <ul>
                            <li>Benefits of Massage</li>
                            <li><a href="<?php site_url()?>/pricingpage/click_problems_and_solutions"> Pain Management </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_problems_and_solutions"> Trigger Point Therapy </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_problems_and_solutions"> Increase Range of Motion </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_problems_and_solutions"> Relieve Stress </a></li>
                            <li><a href="<?php site_url()?>/pricingpage/click_problems_and_solutions"> Lower Blood Pressure </a></li>
                            <li><a href="<?php site_url()?>/pages/faqs"> FAQs </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-4  columns">  
                    <div class="box military">
                        <h5>New to Advanced Massage Network</h5>     
                        <ul>
                            <li><a href="<?php site_url(); ?>/pages/faq#panel1"> What is the difference between Massage Therapy and Medical Massage Therapy? </a></li>
                            <li><a href="<?php site_url(); ?>/pages/faq#panel2"> Benefits of Massage </a></li>
                            <li><a href="<?php site_url(); ?>/pricingpage/click_membership"> Membership Benefits </a></li>
                            <li><a href="<?php site_url(); ?>/pages/faq#panel4"> Flexible Spending Account What is the difference between Therapy and Medical Massage Therapy? </a></li>
                            <li><a href="<?php site_url(); ?>/pages/faq#panel5"> Health Savings Account </a></li>
                            <li><a href="<?php site_url(); ?>/pages/faq#panel6"> Health Insurance Providers and Massage </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-4  columns">
                    <div class="box military">
                        <h5>Advanced Massage Network Locations by State</h5>
                        <ul>
                            <li><a href="<?php site_url()?>/search/clinic/co#colorado"> Colorado </a></li>
                            <li><a href="<?php site_url()?>/search/clinic/fl#florida"> Florida </a></li>
                            <li><a href="<?php site_url()?>/search/clinic/ga#georgia"> Georgia </a></li>
                            <li><a href="<?php site_url()?>/location"> North Carolina </a></li>
                        </ul>
                    </div>
                </div>
                <div class="small-4  columns">
                    <div class="box military">
                        <h5>Advanced Massage Network by Major Metro Areas</h5>     
                        <ul>
                            <li><a href="<?php site_url()?>/location" data-state> Atlanta, GA. </a></li>
                            <li><a href="<?php site_url()?>"> Charlotte, NC. </a></li>
                            <li><a href="<?php site_url()?>"> Raleigh - Durham, NC. </a></li>
                            <li><a href="<?php site_url()?>/location/searchbycity?city=Denver&state=CO"> Denver, CO.</a></li>
                            <li><a href="<?php site_url()?>"> Jacksonville, FL.</a></li>
                            <li><a href="<?php site_url()?>"> Miami, FL.</a></li>
                            <li><a href="<?php site_url()?>"> Tampa Bay, FL.</a></li>
                        </ul>     
                    </div>
                </div>
            </div>
        </div>
        <div class="divider no-border"></div>
        <div class="large-12 columns grey  search">
            <div class='large-6 columns'>
                 
            <ul class='footer_links'>
                                    <li><a target='_blank' href="https://www.linkedin.com/company/advanced-massage-network-llc-" class="socialnet socialnet-linkedin"></a></li>
                                    <li><a target='_blank' href="#" class="socialnet socialnet-youtube"></a></li>
                                    <li><a target='_blank' href="https://www.facebook.com/AdvancedMassageNetwork" class="socialnet socialnet-facebook"></a></li>
                                    <li><a target='_blank' href="https://twitter.com/massagenet" class="socialnet socialnet-twitter"></a></li>
                                    <li><a target='_blank' href="#" class="socialnet socialnet-mashable"></a></li>
                                    <li><a target='_blank' href="#" class="socialnet socialnet-stumbleupon"></a></li>
                                    <li><a target='_blank' href="#" class="socialnet socialnet-reddit"></a></li>
                                    <li><a target='_blank' href="#" class="socialnet socialnet-diggit"></a></li>
                                    <li><a target='_blank' href="https://www.pinterest.com/advmassagenet/" class="socialnet socialnet-pinterest"></a></li>
                                    <li><a target='_blank' href="http://advancedmassagenetwork.blogspot.com/" class="socialnet socialnet-blogger"></a></li>
            </ul>
            </div>
            <div class="right"> <input type="text" placeholder="Search" class="rounded" /></div>
        </div>
        <div class="divider no-border"></div>
        <div class="large-12 columns menu center">
            <span class='city_location' >
                <a href='http://www.kneadingbodyworks.com' class='city_links'><span>kneadingbodyworks</span></a>
                <a href='http://www.bodyequilibrium.net' class='city_links'><span>bodyequilibrium</span></a>
                <a href='http://skinatlanta.com' class='city_links'><span>skinatlanta</span></a>
                <a href='http://www.coloradorolfer.com' class='city_links'><span>coloradorolfer</span></a>
                <a href='http://www.dgilmorecmt.massagetherapy.com' class='city_links'><span>dgilmorecmt.massagetherapy</span></a>
                <a href='http://www.littleton.massagetherapy.com' class='city_links'><span>littleton.massagetherapy</span></a>
                <a href='http://www.intelligentchiropractic.com' class='city_links'><span>intelligentchiropractic</span></a>
            </span>
            <ul>
                <li><a href="<?php echo site_url() ?>location/directory" class="footer-link">Clinics Directory</a></li>
                <li><a href="<?php site_url() ?>/aboutus" class="footer-link"> Why We Are the Best </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_problems_and_solutions" class="footer-link"> Types of Massages </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_problems_and_solutions" class="footer-link"> Help with Types of Health Problems </a></li>
                <li><a href="<?php site_url() ?>/location" class="footer-link"> Massage Clinic Directory </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_health_care_practitioners" class="footer-link"> Own a Massage Franchise for Free </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_therapist" class="footer-link"> Become a Network Massage Therapist </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_become_a_chair_massage_affiliate" class="footer-link"> Affiliate Program </a></li>
                <li><a href="<?php site_url() ?>" class="footer-link"> Press Room </a></li>
                <li><a href="<?php site_url() ?>/testimonial" class="footer-link"> Client Reviews </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_terms_of_use" class="footer-link"> Membership & Website Terms of Use </a></li>
                <li><a href="<?php site_url() ?>/pricingpage/click_privacy_policy" class="footer-link"> Privacy Policy </a></li>
                <li><a href="<?php site_url() ?>/sitemap" class="footer-link"> Site Map </a></li>
                <li><a href="<?php site_url() ?>" class="footer-link"> Text Only </a></li>
            </ul>
        </div>
        <div class="divider no-border"></div>
        <div class="large-12 columns copyright">
            <center>            ©2015 Advanced Massage Network, LLC. All rights reserved. Advanced 

Massage Network® and the Advanced Massage Network logo are 

Registered Trademarks of Advanced Massage Network, LLC.

The Advanced Massage Network trade name and logo may not be 

reproduced by any means or in any form whatsoever without express 

written permission from Advanced Massage Network, LLC.

1016 Piedmont Ave. NE, Atlanta, Georgia, 30309. 1-844-238-6277

Advanced Massage Network is a national sales organization of 

independently owned and operated franchised locations.</center>
        </div>
        <div class="divider no-border"></div>
    </div>   
</div> 
</body>
</html>