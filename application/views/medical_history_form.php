<link rel="stylesheet" href="/assets/css/intakeform.css" />
<div id="intake-section" class="large-12 columns">
    <?php if (isset($intakeformerrors)) { ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $intakeformerrors; ?>
            </div>            
        </div>
        <?php
    }
    echo form_open('intakeform/add');
    ?>
    <div class="row " style="padding: 10px 0px 10px 0px;">        
        <div class="large-12 columns">
            <a href="#" class="button info" data-reveal-id="myModal">Skip</a>
            <h4>Medical History</h4>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Areas of focus?</div>
        <div class="large-9 columns">
            <input name="q14" type="text" value="<?php echo set_value('q14'); ?>"/>
        </div>
    </div>    
    <div class="row">
        <div class="large-3 columns">What kind of pressure do you prefer?</div>
        <div id="fieldKindofPressure" class="large-9 columns">
            <input name="q15" type="radio" value="0"/> Light&nbsp;&nbsp;
            <input name="q15" type="radio" value="1"/> Medium&nbsp;&nbsp;
            <input name="q15" type="radio" value="2"/> Firm&nbsp;&nbsp;
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">What causes discomfort?</div>
        <div class="large-9 columns">
            <input name="q16" type="text" value="<?php echo set_value('q16'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Level of discomfort?</div>
        <div class="large-9 columns">
            <input name="q17" type="radio"/>1&nbsp;
            <input name="q17" type="radio"/>2&nbsp;
            <input name="q17" type="radio"/>3&nbsp;
            <input name="q17" type="radio"/>4&nbsp;
            <input name="q17" type="radio"/>5&nbsp;
            <input name="q17" type="radio"/>6&nbsp;
            <input name="q17" type="radio"/>7&nbsp;
            <input name="q17" type="radio"/>8&nbsp;
            <input name="q17" type="radio"/>9&nbsp;
            <input name="q17" type="radio"/>10&nbsp;
        </div>
    </div>   
    <div class="row">
        <div class="large-3 columns">Under a physician care?</div>
        <div class="large-9 columns">
            <input name="q18" type="radio"/> Yes <input name="q18" type="radio"/> No,
            if Yes, then who are they? <input name="q18a" type="text" value="<?php echo set_value('q18a'); ?>" style="display: inline-block; width: 65%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">For which condition(s)?</div>
        <div class="large-9 columns">
            <input name="q19" type="text" value="<?php echo set_value('q19'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Any diagnosis?</div>
        <div class="large-9 columns">
            <input name="q20" type="text" value="<?php echo set_value('q20'); ?>"/>
        </div>
    </div>       
    <div class="row">
        <div class="large-3 columns required-field">Medications/suppliments?</div>
        <div class="large-9 columns">
            <input id='medi_supp_val' name="q21" type="radio"/> Yes <input name="q21" type="radio"/> No,
            if Yes, what are the names? <input name="q21a" type="text" value="<?php echo set_value('q21a'); ?>" style="display: inline-block; width: 65%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns required-field">Are you pregnant?</div>
        <div id="fieldPregnant" class="large-5 columns">
            <input name="q22" type="radio"/> Yes <input name="q22" type="radio"/> No,
            if Yes, what trimester 1 2 or 3? <input name="q22a" type="text" value="<?php echo set_value('q22a'); ?>" style="display: inline-block; width: 20%;"/>
        </div>
        <div class="large-1 columns">Due date?</div>
        <div class="large-3 columns">
            <input name="q22b" type="text" value="<?php echo set_value('q22b'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns required-field">Suffer from epilepsy or seizures?</div>
        <div id="fieldSeizures" class="large-9 columns">
            <input name="q23" type="radio"/> Yes <input name="q23" type="radio"/> No,
            if Yes, how often? <input name="q23a" type="text" value="<?php echo set_value('q23'); ?>" style="display: inline-block; width: 20%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns required-field">Diabetic?</div>
        <div id="fieldDiabetic" class="large-9 columns">
            <input name="q24" type="radio"/> Yes <input name="q24" type="radio"/> No,
            if Yes, Type I or Type II? <input name="q24a" type="text" value="<?php echo set_value('q24a'); ?>" style="display: inline-block; width: 20%;"/>
        </div>
    </div>    
    <div class="row">
        <div class="large-3 columns required-field">Surgeries?</div>
        <div id="fieldSurgeries" class="large-9 columns">
            <input name="q25" type="radio"/> Yes <input name="q25" type="radio"/> No,
            when? <input name="q25a" type="text" value="<?php echo set_value('q25a'); ?>" style="display: inline-block; width: 20%;"/>
            and what? <input name="q25b" type="text" value="<?php echo set_value('q25b'); ?>" style="display: inline-block; width: 20%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns required-field">Do you have nut allergies?</div>
        <div id="fieldNutAllergies" class="large-9 columns">
            <input name="q26" type="radio"/> Yes <input name="q26" type="radio"/> No
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Other allergies?</div>
        <div id="fieldAllergies" class="large-9 columns">
            <input name="q27" type="radio"/> Yes <input name="q27" type="radio"/> No,
            if Yes, what kind? <input name="q27a" type="text" value="<?php echo set_value('q27a'); ?>" style="display: inline-block; width: 65%;"/>
        </div>
    </div>    
    <div class="row required-field">
        <div class="large-3 columns">Contagious pathologies?</div>
        <div class="large-9 columns">
            <input id='cont_path_val' name="q28" type="radio"/> Yes <input name="q28" type="radio"/> No,
            if Yes, then what kind? <input name="q28a" type="text" value="<?php echo set_value('q28a'); ?>"/>
        </div>
    </div>
    <div class="row required-field">
        <div class="large-3 columns">HIV?</div>
        <div class="large-9 columns">
            <input id='hiv_val' name="q29" type="radio"/> Yes <input name="q29" type="radio"/> No
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Low blood pressure?</div>
        <div class="large-9 columns">
            <input name="q30" type="radio"/> Yes <input name="q30" type="radio"/> No
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">High blood pressure?</div>
        <div class="large-9 columns">
            <input name="q31" type="radio"/> Yes <input name="q31" type="radio"/> No
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns required-field">Recent accident?</div>
        <div id="fieldAccident" class="large-9 columns">
            <input name="q32" type="radio"/> Yes <input name="q32" type="radio"/> No,
            if Yes, then when and how? <input name="q32a" type="text" value="<?php echo set_value('q32a'); ?>" style="display: inline-block; width: 65%;"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Motor vehicle accident?</div>
        <div id="fieldVehicleAccident" class="large-9 columns">
            <input name="q33" type="radio"/> Yes <input name="q33" type="radio"/> No,
            if Yes, then where you hit? <input name="q33a" type="text" value="<?php echo set_value('q33a'); ?>" style="display: inline-block; width: 30%;"/>
            Front? <input name="q33b" type="checkbox"/>&nbsp;
            Front/Side? <input name="q33c" type="checkbox"/>&nbsp;
            Side? <input name="q33d" type="checkbox"/>&nbsp;
            Side/Back? <input name="q33e" type="checkbox"/>&nbsp;
            Back? <input name="q33f" type="checkbox"/>&nbsp;
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Other accident?</div>
        <div class="large-9 columns">
            <input id="fieldOtherAccident" name="q34" type="text" value="<?php echo set_value('q34'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Any other health conditions?</div>
        <div class="large-9 columns">
            <input name="q35" type="text" value="<?php echo set_value('q35'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-6 columns">
            <div class="row" style="padding: 10px 0px 10px 0px;">
                <div class="large-12 columns">Do you have a history of the following?</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h1" type="checkbox"/> accident</div>
                <div class="large-4 columns"><input name="is_h2" type="checkbox"/> spains</div>
                <div class="large-4 columns"><input name="is_h3" type="checkbox"/> mastectomy</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h4" type="checkbox"/> neck pain</div>
                <div class="large-4 columns"><input name="is_h5" type="checkbox"/> whiplash</div>
                <div class="large-4 columns"><input name="is_h6" type="checkbox"/> breast augmentation</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h7" type="checkbox"/> headache</div>
                <div class="large-4 columns"><input name="is_h8" type="checkbox"/> abdominal pain</div>
                <div class="large-4 columns"><input name="is_h9" type="checkbox"/> stomach issues</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h10" type="checkbox"/> shoulder pain</div>
                <div class="large-4 columns"><input name="is_h11" type="checkbox"/> nervous tension</div>
                <div class="large-4 columns"><input name="is_h12" type="checkbox"/> varicose vains</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h13" type="checkbox"/> upper back pain</div>
                <div class="large-4 columns"><input name="is_h14" type="checkbox"/> arthritis, bursitis or gout</div>
                <div class="large-4 columns"><input name="is_h15" type="checkbox"/> circulation problem</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h16" type="checkbox"/> mid back pain</div>
                <div class="large-4 columns"><input name="is_h17" type="checkbox"/> tension</div>
                <div class="large-4 columns"><input name="is_h18" type="checkbox"/> stroke</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h19" type="checkbox"/> low back pain</div>
                <div class="large-4 columns"><input name="is_h20" type="checkbox"/> wear contacts</div>
                <div class="large-4 columns"><input name="is_h21" type="checkbox"/> heart attack</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h22" type="checkbox"/> joint ache</div>
                <div class="large-4 columns"><input name="is_h23" type="checkbox"/> scoliosis</div>
                <div class="large-4 columns"><input name="is_h24" type="checkbox"/> cancer</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h25" type="checkbox"/> decreased range of motion</div>
                <div class="large-4 columns"><input name="is_h26" type="checkbox"/> fibrorriyalgia</div>
                <div class="large-4 columns"><input name="is_h27" type="checkbox"/> colitis</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h28" type="checkbox"/> broken bones</div>
                <div class="large-4 columns"><input name="is_h29" type="checkbox"/> carpal tunnel syndrome</div>
                <div class="large-4 columns"><input name="is_h30" type="checkbox"/> migraines</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h31" type="checkbox"/> digestion problems</div>
                <div class="large-4 columns"><input name="is_h32" type="checkbox"/> respiratory problems</div>
                <div class="large-4 columns"><input name="is_h33" type="checkbox"/> sinus problems</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h34" type="checkbox"/> constipation</div>
                <div class="large-4 columns"><input name="is_h35" type="checkbox"/> kidney</div>
                <div class="large-4 columns"><input name="is_h36" type="checkbox"/> chronic fatigue</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h37" type="checkbox"/> diarrhea</div>
                <div class="large-4 columns"><input name="is_h38" type="checkbox"/> bladder</div>
                <div class="large-4 columns"><input name="is_h39" type="checkbox"/> spinal</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h40" type="checkbox"/> insomia</div>
                <div class="large-4 columns"><input name="is_h41" type="checkbox"/> heart problems</div>
                <div class="large-4 columns"><input name="is_h43" type="checkbox"/> gall bladder problems</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h44" type="checkbox"/> arthritis</div>
                <div class="large-4 columns"><input name="is_h45" type="checkbox"/> aneurysm</div>
                <div class="large-4 columns"><input name="is_h46" type="checkbox"/> arrhythmia</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_h47" type="checkbox"/> multiple sclerosis (MS)</div>
                <div class="large-4 columns"><input name="is_h48" type="checkbox"/> sciatica</div>
                <div class="large-4 columns">
                    <input name="is_h49" type="checkbox"/>
                    <input name="h49" type="text" value="<?php echo set_value('h49'); ?>"/></div>
            </div>
            <div class="row" style="padding: 10px 0px 10px 0px;">
                <div class="large-12 columns">Do you have any of the following recently?</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_t1" type="checkbox"/> sunburn</div>
                <div class="large-4 columns"><input name="is_t2" type="checkbox"/> open cuts</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_t3" type="checkbox"/> inflammation</div>
                <div class="large-4 columns"><input name="is_t4" type="checkbox"/> irritated skin rash</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_t5" type="checkbox"/> severe pain</div>
                <div class="large-4 columns"><input name="is_t6" type="checkbox"/> poison ivy</div>
            </div>
            <div class="row">
                <div class="large-4 columns"><input name="is_t7" type="checkbox"/> headache</div>
                <div class="large-4 columns"><input name="is_t8" type="checkbox"/> cold/flu</div>
            </div>
        </div>
        <div class="large-6 columns">
            <div id="human-body-map"></div>
        </div>
    </div>    
    <div class="row" style="margin-top: 45px;">
        <div class="large-12 columns">
            <p>
                I understand that the massage therapist does not diagnose illness, disease or any physical or mental disorder. Therefore, the massage therapist prescribes neither medical treatment, nor pharmaceutical, nor performs any spinal manupulations. It has been made to me that this massage therapy is not a substitute for medical examination and/or diagnosis that is recommended that i see a physician for any physical aliment that I might have. 
            </p>
            <p>
                I understand that massage therapy involves hands on application of pressure to muscle tension. I have no condition that would contradict the practition has my permission to apply massage therapy techniques.
            </p>
            <br/><br/>
            <button type="submit">Submit</button>
        </div>
    </div>
    <?php echo form_close(); ?>    

    <div id="pain-description" style="display: none;">
        <table>
            <tr>
                <td width="20%">Modality:</td>
                <td width="80%">
                    <select id="modality-selection" name="modality-selection">
                        <option value="0"> Medical Massage / Remedial Massage</option>
                        <option value="1"> Spa / Swedish / Deep Tissue</option>
                        <option value="2"> Fascia Manipulation / Myofascial Release</option>
                        <option value="3"> Trigger Point Therapy / Neuromuscular Therapy</option>
                        <option value="4"> Lymphatic Massage / Lymphatic Drainage</option>
                        <option value="5"> Energy Work / Reiki</option>
                        <option value="6"> Eastern Medicine massage</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="modality-date" name="modality-date" type="text"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button id="save" class="modality-selection-button">Save</button>&nbsp;&nbsp;
                    <button id="discard" class="modality-selection-button">Discard</button>&nbsp;&nbsp;
                    <button id="delete" class="modality-selection-button" type="submit">Delete</button>
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="myModal" class="reveal-modal small" data-reveal>
    <p style="color: red;"> By clicking the skip button you agree that all your information below is correct and still current, and no editing is necessary. </p>
    <a href="#" class="close-reveal-modal">&#215;</a>
    <a href="#" class="button right" style="padding: 8px; padding-top: 0;"><span style="font-size: 14px;">Ok, Skip</span></a>
    </div>
</div>

<script type="text/javascript">

    var locations = [];
    var lcount = 0;

    var isDisplayed = false;
    var humanFrontLocs = [[111, 0], [158, 0], [168, 50], [158, 92], [239, 140], [263, 334], [223, 342], [233, 290], [197, 204], [204, 366],
        [206, 595], [159, 591], [134, 361], [101, 594], [59, 591], [59, 361], [73, 205], [38, 334], [0, 338], [34, 131],
        [109, 93], [95, 40], [111, 0]];
    var humanBackLocs = [[408, 0], [442, 26], [437, 79], [511, 119], [550, 345], [512, 346], [481, 199], [458, 235], [476, 378], [475, 592],
        [436, 597], [407, 366], [380, 599], [335, 596], [338, 354], [367, 223], [340, 196], [304, 347], [270, 344], [279, 219], [312, 111],
        [385, 76], [375, 21], [408, 0]];

    var markedIcons = [false, false, false, false, false, false, false];
    var selectedMarker = null;

    $(document).ready(function () {

        $('[id^="marker-icon-"]').hover(function () {
            if (!isDisplayed) {
                var id = $(this).attr("id");
                var n = id.substring(id.length - 1);
                $('#marker-desc-' + n).css("top", (parseInt($(this).offset().top) - $('#header').height() + 23) + "px");
                $('#marker-desc-' + n).css("left", (parseInt($(this).offset().left)
                        - $('.row:has(#marker-icon-' + n + '):last').offset().left) + "px");
                $('#marker-desc-' + n).show();
                isDisplayed = true;
            }
        }, function () {
            var id = $(this).attr("id");
            var n = id.substring(id.length - 1);
            $('#marker-desc-' + n).hide();
            isDisplayed = false;
        });

        $('[id^="marker-icon-"]').click(function () {
            var id = $(this).attr("id");
            var n = parseInt(id.substring(id.length - 1)) - 1;
            if (!markedIcons[n]) {
                $(this).css("border", "1px dotted red");
                markedIcons[n] = true;
                if (n === 0) {
                    $('#q8').val(1);
                } else if (n === 1) {
                    $('#q8a').val(1);
                } else if (n === 2) {
                    $('#q8b').val(1);
                } else if (n === 3) {
                    $('#q8c').val(1);
                } else if (n === 4) {
                    $('#q8d').val(1);
                } else if (n === 5) {
                    $('#q8e').val(1);
                } else if (n === 6) {
                    $('#q8f').val(1);
                }
            } else {
                $(this).css("border", "none");
                markedIcons[n] = false;
                if (n === 0) {
                    $('#q8').val(0);
                } else if (n === 1) {
                    $('#q8a').val(0);
                } else if (n === 2) {
                    $('#q8b').val(0);
                } else if (n === 3) {
                    $('#q8c').val(0);
                } else if (n === 4) {
                    $('#q8d').val(0);
                } else if (n === 5) {
                    $('#q8e').val(0);
                } else if (n === 6) {
                    $('#q8f').val(0);
                }
            }
        });

        var isMarkerHit = false;

        $('#human-body-map').click(function (e) {
            if (isMarkerHit) {
                isMarkerHit = false;
                return;
            }
            var t = e.pageY - 20;
            var l = e.pageX - 5;
            lcount += 1;
            var id = "pain-marker-" + lcount;
            var dateId = "pain-date-" + lcount;
            selectedMarker = lcount - 1;
            var cornery = parseInt($('#human-body-map').offset().top);
            var cornerx = parseInt($('#human-body-map').offset().left);
            if (pointInPolygon(humanFrontLocs, e.pageX, e.pageY, cornerx, cornery)
                    || pointInPolygon(humanBackLocs, e.pageX, e.pageY, cornerx, cornery)) {
                var currentDate = new Date();
                var s = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear().toString();
                $('#human-body-map').append('<div id="' + id + '" class="pain-marker modality-marker-red"></div>');
                $('#human-body-map').append('<div id="' + dateId + '" class="pain-marker-date"></div>');


                $('#' + id).css("top", (t - cornery) + "px");
                $('#' + id).css("left", (l - cornerx) + "px");
                $('#' + dateId).css("top", (t - cornery + 10) + "px");
                $('#' + dateId).css("left", (l - cornerx + 45) + "px");

                locations.push({indexno: selectedMarker,
                    x: (l - cornerx), y: (t - cornery),
                    modalitytype: 0, date: s});

                var tt = e.pageY - $('#header').height();
                var ll = e.pageX - $('.row:has(#human-body-map):last').offset().left - 18;
                $('#pain-description').css("top", tt + "px");
                $('#pain-description').css("left", ll + "px");
                $('#modality-selection').val(0);
                $('#modality-date').val(s);
                $('#pain-description').show();
                $('#pain-date-' + (selectedMarker + 1)).html($('#modality-date').val());
                $('#pain-date-' + (selectedMarker + 1)).show();

                $('.pain-marker').unbind("click");
                $('.pain-marker').click(function () {
                    var id = $(this).attr("id");
                    var n = parseInt(id.substring(id.lastIndexOf("-") + 1)) - 1;
                    selectedMarker = n;
                    isMarkerHit = true;
                    var m = getLocationsIndex(n);
                    var tt = $(this).offset().top + 20 - $('#header').height();
                    var ll = $(this).offset().left - $('.row:has(#human-body-map):last').offset().left;
                    $('#pain-description').css("top", tt + "px");
                    $('#pain-description').css("left", ll + "px");
                    $('#modality-selection').val(locations[m].modalitytype);
                    $('#modality-date').val(locations[m].date);
                    $('#pain-description').show();
                });
            }
        });

        $(window).resize(function () {
            $('#pain-description').hide();
        });

        $('#pain-description #save').click(function () {
            var m = getLocationsIndex(selectedMarker);
            locations[m].modalitytype = $('#modality-selection').val();
            locations[m].date = $('#modality-date').val();
            $('#pain-date-' + (selectedMarker + 1)).html($('#modality-date').val());

            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-red");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-green");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-orange");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-blue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-yellow");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-darkblue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-violet");

            var p = parseInt($('#modality-selection').val());
            if (p === 0) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-red");
            } else if (p === 1) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-green");
            } else if (p === 2) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-orange");
            } else if (p === 3) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-blue");
            } else if (p === 4) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-yellow");
            } else if (p === 5) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-darkblue");
            } else if (p === 6) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-violet");
            }
            $('#pain-description').hide();
        });

        $('#pain-description #discard').click(function () {
            $('#pain-description').hide();
        });

        $('#pain-description #delete').click(function () {
            $('#pain-marker-' + (selectedMarker + 1)).remove();
            $('#pain-date-' + (selectedMarker + 1)).remove();
            locations.splice(selectedMarker, 1);
            $('#pain-description').hide();
        });

        $('#intake-section form').submit(function (e) {
            e.preventDefault();
            $('<input />').attr("type", "hidden").attr("name", "painmarker")
                    .attr("value", JSON.stringify(locations)).appendTo(this);
            $('<input />').attr("type", "hidden").attr("name", "schedulePost")
                    .attr("value", 'medicalhistory').appendTo(this);
            /*$(this).submit();*/
                        if($('#fieldName').val()==''){
                            console.log('Name is required.');
                            scrollToId('#fieldName');
                            alert('Name is required.');
                        }else if($('#fieldAddress').val()==''){
                            console.log('Address is required.');
                            scrollToId('#fieldAddress');
                            alert('Address is required.');
                        }else if($('#fieldCity').val()==''){
                            console.log('City is required.');
                            scrollToId('#fieldCity');
                            alert('City is required.');
                        }else if($('#fieldState').val()==''){
                            console.log('State is required.');
                            scrollToId('#fieldState');
                            alert('State is required.');
                        }else if($('#birth_date').val()==''){
                            console.log('Date of Birth is required.');
                            scrollToId('#birth_date');
                            alert('Date of Birth is required.');
                        }else if($('#fieldGender').val()==''){
                            console.log('Gender is required.');
                            scrollToId('#fieldGender');
                            alert('Gender is required.');
                        }else if(!($('input[name=q21]').is( ":checked" ))){
                            console.log('Medications / Supplements is required.');
                            scrollToId('#medi_supp_val');
                            alert('Medications / Supplements is required.');
                        }else if(!($('input[name=q22]').is( ":checked" ))){
                            //#fieldPregnant
                            console.log('Field "Are you pregnant?" is required.');
                            scrollToId('#fieldPregnant');
                            alert('Field "Are you pregnant?" is required.');
                        }else if(!($('input[name=q23]').is( ":checked" ))){
                            //#fieldSeizures
                            console.log('Field "Suffer from epilepsy or seizures?" is required.');
                            scrollToId('#fieldSeizures');
                            alert('Field "Suffer from epilepsy or seizures?" is required.');
                        }else if(!($('input[name=q24]').is( ":checked" ))){
                            //#fieldDiabetic
                            console.log('Field "Diabetic?" is required.');
                            scrollToId('#fieldDiabetic');
                            alert('Field "Diabetic?" is required.');
                        }else if(!($('input[name=q25]').is( ":checked" ))){
                            //#fieldSurgeries
                            console.log('Field "Surgeries?" is required.');
                            scrollToId('#fieldSurgeries');
                            alert('Field "Surgeries?" is required.');
                        }else if(!($('input[name=q26]').is( ":checked" ))){
                            //#fieldSurgeries
                            console.log('Field "Nut Allergies?" is required.');
                            scrollToId('#fieldNutAllergies');
                            alert('Field "Nut Allergies?" is required.');
                        }else if(!($('input[name=q28]').is( ":checked" ))){
                            console.log('Field "Contagious Pathologies" is required.');
                            scrollToId('#cont_path_val');
                            alert('Field "Contagious Pathologies" is required.');
                        }else if(!($('input[name=q29]').is( ":checked" ))){
                            console.log('Field "HIV" is required.');
                            scrollToId('#hiv_val');
                            alert('Field "HIV" is required.');
                        }else if(!($('input[name=q26]').is( ":checked" ))){
                            //#fieldNutAllergies
                            console.log('Field "Do you have nut allergies?" is required.');
                            scrollToId('#fieldNutAllergies');
                            alert('Field "Do you have nut allergies?" is required.');
                        }else if(!($('input[name=q32]').is( ":checked" ))){
                            //#fieldAccident
                            console.log('Field "Recent Accident?" is required.');
                            scrollToId('#fieldAccident');
                            alert('Field "Recent Accident?" is required.');
                        }else{
                            var dataString = $("#intake-section form").serialize();
                            $.ajax({
                                    type: "POST",
                                    // url: "<?php echo site_url() . "intakeform/add"; ?>",
                                    url: "<?php echo site_url() . "schedule/save"; ?>",
                                    data: dataString,
                                    cache: false,
                                    success: function (result) {
                                            if (result == "true") {
                                                    $('body').append(
                                                                    $('<form>')
                                                                    .attr('id', 'redirectForm')
                                                                    .attr('action', '<?php echo site_url() . "booking/contract"; ?>').attr('method', 'post')
                                                                    .append($('<input>').attr('name', 'name').attr('value', $('input[name="name"]').val()))
                                                                    .append($('<input>').attr('name', 'address_street').attr('value', $('input[name="address_street"]').val()))
                                                                    .append($('<input>').attr('name', 'address_city').attr('value', $('input[name="address_city"]').val()))
                                                                    .append($('<input>').attr('name', 'address_state').attr('value', $('input[name="address_state"]').val()))
                                                                    .append($('<input>').attr('name', 'address_zip').attr('value', $('input[name="address_zip"]').val()))
                                                                    );
                                                    $('#redirectForm').submit();
                                            }
                                    }
                            });
                        }
        });
            function scrollToId(target){
                $('html, body').animate({
                    scrollTop: $(target).offset().top
                }, 2000);
            }
    });

    function getLocationsIndex(indexNo) {
        var retValue = null;
        for (var i = 0; i < locations.length; i++) {
            if (locations[i].indexno === indexNo) {
                retValue = i;
                break;
            }
        }
        return retValue;
    }

</script>