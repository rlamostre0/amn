<div id="diagnosis-section" class="large-12 columns">
    <div class="row ">
        <div class="large-12 columns ">
            <div class=" large-5 columns left">
                <h3>Symptoms / Diagnosis</h3>
                <dl class="accordion" data-accordion> 
                    <dd> <a href="#panel1">Bulging Disk</a> 
                        <div id="panel1" class="content active"> 


                        </div> 
                    </dd> 
                    <dd> 
                        <a href="#panel2">Herniated Disk</a> 
                        <div id="panel2" class="content "> Your standard spa modalities, which incorporate Swedish, Deep Tissue, and Prenatal Massage as well as bodyworks, including body wraps.

                        </div> 
                    </dd> 
                    <dd> 
                        <a href="#panel3">Sciatica</a> 
                        <div id="panel3" class="content"> Modalities that manipulate the fascial system in order to eliminate restrictions, and structural imbalance.

                        </div> 
                    </dd> 
                    <dd> 
                        <a href="#panel4">Pelvic Tilt</a> 
                        <div id="panel4" class="content "> Modalities that treat the trigger points, aka “knots”, in the muscles, which have a tendency to pull on the skeletal body and cause pain and discomfort.

                        </div> 
                    </dd> 
                    <dd> 
                        <a href="#panel5">Tennis Elbow</a> 
                        <div id="panel5" class="content "> Modalities that encourage natural drainage of lymphatic fluid within body tissue. This helps with swelling and inflammation.
                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel6">Golf Elbow</a> 
                        <div id="panel6" class="content "> Bodywork used to balance an individual’s flow of energy by combining it with universal energy to open pathways of healing. Recommended for patients undergoing chemotherapy.

                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel7">Migrane Headaches</a> 
                        <div id="panel7" class="content "> Various forms of popular and effective Eastern massage modalities. Includes Shiatsu, Qi Gong, Thai Massage, and Acupressure

                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel8">Lack of Range of Motion</a> 
                        <div id="panel8" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>
                    <dd>
                        <a href="#panel9">Frozen Shoulder</a> 
                        <div id="panel9" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel10">Frozen Hip</a> 
                        <div id="panel10" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>
                    <dd>
                        <a href="#panel11">Plantar Facetious</a> 
                        <div id="panel11" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel12">Carpal Tunnel</a> 
                        <div id="panel12" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>  
                    <dd>
                        <a href="#panel13">Muscle Sprains</a> 
                        <div id="panel13" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel4">Muscle Strains</a> 
                        <div id="panel4" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd> 
                    <dd>
                        <a href="#panel5">Muscle Tears</a> 
                        <div id="panel5" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>   
                    <dd>
                        <a href="#panel6">Additional Stress</a> 
                        <div id="panel6" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>

                    <dd>
                        <a href="#panel6">Atrophy</a> 
                        <div id="panel6" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>  

                    <dd>
                        <a href="#panel6">Bone Spur</a> 
                        <div id="panel6" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>

                    <dd>
                        <a href="#panel6">More Problems lead to more problems.</a> 
                        <div id="panel6" class="content "> These massage therapists are approved by medical insurance companies as providers, including Blue Cross Blue Shield, Cigna, Aetna, Kaiser Permanente, Humana, and/or UnitedHealth Group. Therapists have a U.S. National Provider Identification Number.

                        </div> 
                    </dd>  
                </dl>
            </div>
            <div class=" right large-7 columns">
                <div class="row">
                    <h4>Benefits of Medical Massage Therapy</h4>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                </div>
                <div class="row">
                    <div id="human-body-map"></div>
                </div>
            </div>
        </div>
    </div>
