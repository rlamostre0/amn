<center>

<form id="block-form">

<table class="table table-striped">
<thead>
	<tr>
		<th colspan="2"><center>Block Schedule</center></th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Block Description</td>
		<td> 
			<textarea name="description" rows="2" cols="30" style="resize: none;"></textarea> 
		</td>
	</tr>
	<tr>
		<td>Type</td>
		<td> 
			<input type="radio" name="blocktype_id" class="blocktype_id" value="1" checked> Time Range<br>
			<input type="radio" name="blocktype_id" class="blocktype_id" value="2"> All Day
		</td>
	</tr>
	<tr class="time-option">
		<td>Time</td>
		<td class="time-dropdown">  
		</td>
	</tr>
	<tr>
		<td>Date</td>
		<td> 
			start: <input name="start" type="text" id="start"><br>
			end: <input name="end" type="text" id="end">
		</td>
	</tr>
	<tr>
		<td>Repeat Options</td>
		<td> 
			<input type="radio" name="recurring_id" value="1" checked> Once<br>
			<input type="radio" name="recurring_id" value="2"> Daily<br>
			<input type="radio" name="recurring_id" value="3"> Weekly<br>
			<input type="radio" name="recurring_id" value="4"> Monthly<br>
			<input type="radio" name="recurring_id" value="5"> Yearly<br>
		</td>
	</tr>
</tbody>
</table>
<input type="hidden" name="clinic_id" id="clinic_id">
</form>
<input type="submit" class="button radius submit" tabindex="0" value="Save">
</center>
<script>
	$(function() {
		var clinic_id='<?php echo $_POST['clinic_id']; ?>';
		var picker = new Pikaday(
		{
				field: document.getElementById('start'),
				firstDay: 1,
				minDate: new Date('2000-01-01'),
				maxDate: new Date('2020-12-31'),
				yearRange: [2000,2020]
		});
		var picker = new Pikaday(
		{
				field: document.getElementById('end'),
				firstDay: 1,
				minDate: new Date('2000-01-01'),
				maxDate: new Date('2020-12-31'),
				yearRange: [2000,2020]
		});
		$('#clinic_id').val(clinic_id);
		$('.time-dropdown').html(timepickers());
		function timepickers(){
			var 
				return_var='',
				hours='',
				minutes='',
				meridiem='';
			meridiem+='<option value="AM">am</option>';
			meridiem+='<option value="PM">pm</option>';
			for (i = 1; i <= 12; i++) {
				var digit=(i<=9?'0':'');
				var selected=(i==12?'selected':'');
				hours+='<option value="'+digit+i+'" '+selected+'>'+digit+i+'</option>';
			}
			for (i = 0; i <= 59; i++) {
				var digit=(i<=9?'0':'');
				minutes+='<option value="'+digit+i+'">'+digit+i+'</option>';
			}
			return_var+='start: <select name="startHour" style="width: 80px">';
			return_var+=hours;
			return_var+='</select>';
			return_var+='<select name="startMinutes" style="width: 80px">';
			return_var+=minutes;
			return_var+='</select>';
			return_var+='<select name="startMeridiem" style="width: 80px">';
			return_var+=meridiem;
			return_var+='</select>';
			return_var+='<br/>';
			return_var+='end: <select name="endHour" style="width: 80px">';
			return_var+=hours;
			return_var+='</select>';
			return_var+='<select name="endMinutes" style="width: 80px">';
			return_var+=minutes;
			return_var+='</select>';
			return_var+='<select name="endMeridiem" style="width: 80px">';
			return_var+=meridiem;
			return_var+='</select>';
			return return_var;
		}
		$('body').on( "click", ".blocktype_id", function() {
			if($( this ).val()=='1')
				$('.time-option').show();
			else
				$('.time-option').hide();
		});
		$('body').on( "click", ".submit", function() {
			$.ajax({
			type: "POST",
			url: "<?php echo site_url().'schedule/block_event'; ?>",
			data: $('#block-form').serialize()
			})
			.done(function( result ) {
				result=jQuery.parseJSON(result);
				if(result['success']){
					calendar.fullCalendar('renderEvent',
						{
							title: result['eventTitle'],
							start: result['start'],
							end: result['end'],
						},
						true // make the event "stick"
					);
					$('#myModal').foundation('reveal', 'close');
				}else{
					alert(result['msg']);
				}
			});
		});
	});
</script>