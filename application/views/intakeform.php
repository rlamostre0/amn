<link rel="stylesheet" href="/assets/css/intakeform.css" />
<div id="intake-section" class="large-12 columns form-inputs">
    <?php if (isset($intakeformerrors)) { ?>
        <div class="row">        
            <div class="large-12 columns" style="margin-top: 10px;">
                <?php echo $intakeformerrors; ?>
            </div>            
        </div>
        <?php
    }
    echo form_open('intakeform/add');
    ?>
    

    <div class="row " style="padding: 10px 0px 10px 0px;">        
        <div class="large-12 columns">
            <a href="#" class="button info" data-reveal-id="myModal">Skip</a>
            <h4>Contact Information</h4>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field">Name</div>
        <div class="large-10 columns">
            <input id="fieldName" name="name" type="text" value="<?php echo set_value('name'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field">Address</div>
        <div class="large-10 columns">
            <input id="fieldAddress" name="address_street" type="text" value="<?php echo set_value('address_street'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field">City</div>
        <div class="large-2 columns">
            <input id="fieldCity" name="address_city" type="text" value="<?php echo set_value('address_city'); ?>"/>
        </div>
        <div class="large-2 column required-field">State</div>
        <div class="large-2 columns">
            <input id="fieldState" name="address_state" type="text" value="<?php echo set_value('address_state'); ?>"/>
        </div>
        <div class="large-2 columns required-field">Zip</div>
        <div class="large-2 columns">
            <input id='address_zip_val' name="address_zip" type="text" value="<?php echo set_value('address_zip'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns">Contact No.</div>
        <div class="large-10 columns">
            <div class="row">
                <div class="large-1 columns required-field">Cellphone</div>
                <div class="large-3 columns"><input id='cell_number_val' name="cell_number" type="text" value="<?php echo set_value('contact_home'); ?>"/></div>
                <div class="large-1 columns required-field">Home</div>
                <div class="large-3 columns"><input id='contact_home_val' name="contact_home" type="text" value="<?php echo set_value('contact_home'); ?>"/></div>
                <div class="large-1 columns">Work</div>
                <div class="large-3 columns end"><input name="contact_work" type="text" value="<?php echo set_value('contact_work'); ?>"/></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field">Email</div>
        <div class="large-10 columns">
            <input id='email_val' name="email" type="text" value="<?php echo set_value('email'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns required-field">Date of Birth</div>
        <div class="large-2 columns">
            <input name="birth_date" id="birth_date" type="text" value="<?php echo set_value('birth_date'); ?>"/>
        </div>
        <div class="large-2 columns">Age</div>
        <div class="large-2 columns">
            <input name="age" type="text" value="<?php echo set_value('age'); ?>"/>
        </div>
        <div class="large-2 columns">Gender</div>
        <div class="large-2 columns">
            <select id="fieldGender" name="gender" style="padding: 3px;">
                <option value="0" <?php echo set_select('gender', '0'); ?>>Male</option>
                <option value="1" <?php echo set_select('gender', '1'); ?>>Female</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns">Height</div>
        <div class="large-2 columns">
            <input name="height" type="text" value="<?php echo set_value('height'); ?>"/>
        </div>
        <div class="large-1 columns">Weight</div>
        <div class="large-2 columns">
            <input name="weight" type="text" value="<?php echo set_value('weight'); ?>"/>
        </div>
        <div class="large-1 columns" style='line-height:18px'>Marital Status</div>
        <div class="large-2 columns">
            <select name="civil_status" style="padding: 3px;">
                <option value="0" <?php echo set_select('civil_status', '0'); ?>>Single</option>
                <option value="1" <?php echo set_select('civil_status', '1'); ?>>Married</option>
                <option value="2" <?php echo set_select('civil_status', '2'); ?>>Widowed</option>
                <option value="3" <?php echo set_select('civil_status', '3'); ?>>Devorsed</option>
            </select>
        </div>
        <div class="large-1 columns" style='line-height:18px'>No. of Children</div>
        <div class="large-1 columns">
            <input name="children_count" type="text" value="<?php echo set_value('children_count'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-2 columns">Occupation</div>
        <div class="large-10 columns">
            <input name="occupation" type="text" value="<?php echo set_value('occupation'); ?>"/>
            <font size="3" class='required-field'>If you are: in the military, a healthcare practitioner, a senior citizen, or a student you receive a membership discount.</font>
        </div>
    </div>
    <div class="row" style="padding: 15px 0px 15px 0px;">
        <div class="large-12 columns"><center><b>General Information:</b></center></div>
    </div>
    <div class="row" style="padding: 15px 0px 15px 0px;">
        <div class="large-12 columns">
            <!--Please take a moment to carefully read the following and sign where indicated. If you have a specific medical condition or specific symptoms, massage/bodywork may be contraindicated. A referral from your primary care provider may be required prior to service being provided.-->
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Referred By:</div>
        <div class="large-9 columns">
            a member? <input name="q1" type="radio" value="<?php echo set_value('q1'); ?>"/> Yes 
            <input name="q1" type="radio" value="<?php echo set_value('q1'); ?>"/> No, 
            if Yes, an affiliate? <input name="q1a" type="radio" value="<?php echo set_value('q1a'); ?>"/> Yes <input name="q1a" type="radio" value="<?php echo set_value('q1a'); ?>"/> No,
            if Yes, who? <input name="q1b" type="text" value="<?php echo set_value('q1b'); ?>" style="display: inline-block; width: 20%;"/> 
            an AMN Therapist? <input name="q1c" type="radio" value="<?php echo set_value('q1c'); ?>"/> Yes <input name="q1c" type="radio" value="<?php echo set_value('q1c'); ?>"/> No,
            if Yes, who? <input name="q1d" type="text" value="<?php echo set_value('q1d'); ?>" style="display: inline-block; width: 20%;"/> 
            an internet member? <input name="q1e" type="radio" value="<?php echo set_value('q1e'); ?>"/> Yes <input name="q1e" type="radio" value="<?php echo set_value('q1e'); ?>"/> No,
            if Yes, where? <input name="q1f" type="text" value="<?php echo set_value('q1f'); ?>" style="display: inline-block; width: 20%;"/> 
        </div>
    </div>    
    <div class="row">
        <div class="large-3 columns">In case of emergency contact</div>
        <div class="large-5 columns">
            <input name="q2" type="text" value="<?php echo set_value('q2'); ?>"/>
        </div>
        <div class="large-1 columns">phone no.</div>
        <div class="large-3 columns">
            <input name="q2a" type="text" value="<?php echo set_value('q2a'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">&nbsp;</div>
        <div class="large-1 columns">Email</div>
        <div class="large-8 columns">
            <input name="q2b" type="text" value="<?php echo set_value('q2b'); ?>"/>
        </div>
    </div>    
    <div class="row">
        <div class="large-3 columns">Describe your health</div>
        <div class="large-9 columns">
            <input name="q3" type="radio" value="<?php echo set_value('q3'); ?>"/> Excellent&nbsp;&nbsp;
            <input name="q3" type="radio" value="<?php echo set_value('q3'); ?>"/> Very Good&nbsp;&nbsp;
            <input name="q3" type="radio" value="<?php echo set_value('q3'); ?>"/> Good&nbsp;&nbsp;
            <input name="q3" type="radio" value="<?php echo set_value('q3'); ?>"/> Fair&nbsp;&nbsp;
            <input name="q3" type="radio" value="<?php echo set_value('q3'); ?>"/> Poor
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">What self care do you do?</div>
        <div class="large-9 columns">
            <input name="q4" type="text" value="<?php echo set_value('q4'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">How do you exercise?</div>
        <div class="large-9 columns">
            <input name="q5" type="text" value="<?php echo set_value('q5'); ?>" style="display: inline-block; width: 60%;"/>&nbsp;&nbsp;&nbsp;&nbsp;
            on your side? <input name="q5a" type="checkbox" value="<?php echo set_value('q5a'); ?>"/>
            back? <input name="q5b" type="checkbox" value="<?php echo set_value('q5b'); ?>"/>
            face down? <input name="q5c" type="checkbox" value="<?php echo set_value('q5c'); ?>"/>
        </div>
    </div>    
    <div class="row">
        <div class="large-3 columns">How do you sleep?</div>
        <div class="large-9 columns">
            <input name="q6" type="text" value="<?php echo set_value('q6'); ?>"/>
        </div>
    </div>        
    <div class="row">
        <div class="large-3 columns">Have you ever had a professional massage before?</div>
        <div class="large-9 columns">
            <input name="q7" type="radio"/> Yes
            <input name="q7" type="radio" style='margin-left:20px'/> No
        </div>
    </div>
    <div class="row">        
        <div class="large-3 columns">What type of massage do you prefer?</div>
        <div class="large-9 columns">            
            <input type="hidden" id="q8" name="q8" value="<?php echo set_value('q8'); ?>"/>
            <input type="hidden" id="q8a" name="q8a" value="<?php echo set_value('q8a'); ?>"/>
            <input type="hidden" id="q8b" name="q8b" value="<?php echo set_value('q8b'); ?>"/>
            <input type="hidden" id="q8c" name="q8c" value="<?php echo set_value('q8c'); ?>"/>
            <input type="hidden" id="q8d" name="q8d" value="<?php echo set_value('q8d'); ?>"/>
            <input type="hidden" id="q8e" name="q8e" value="<?php echo set_value('q8e'); ?>"/>
            <input type="hidden" id="q8f" name="q8f" value="<?php echo set_value('q8f'); ?>"/>
            <div id="marker-icon-1" class="modality-marker modality-marker-red"></div> Medical Massage / Remedial Massage <br/>
            <div id="marker-icon-2" class="modality-marker modality-marker-green"></div>  Spa / Swedish / Deep Tissue <br/>
            <div id="marker-icon-3" class="modality-marker modality-marker-orange"></div>  Fascia Manipulation / Myofascial Release <br/>
            <div id="marker-icon-4" class="modality-marker modality-marker-blue"></div>  Trigger Point Therapy / Neuromuscular Therapy <br/>
            <div id="marker-icon-5" class="modality-marker modality-marker-yellow"></div>  Lymphatic Massage / Lymphatic Drainage <br/>
            <div id="marker-icon-6" class="modality-marker modality-marker-darkblue"></div>  Energy Work / Reiki <br/>
            <div id="marker-icon-7" class="modality-marker modality-marker-violet"></div>  Eastern Medicine massage or massage modalities from Asia <br/>
            <!--<div id="marker-icon-8" class="modality-marker modality-marker-black"></div>  Insurance Approved Provider <br/>-->
        </div>
    </div>
    <p id="marker-desc-1" class="modality-desc" style="display: none;">
        Modalities that find and treat the specific cause of the disorder and symptoms. These therapists typically offer both Myofascial Release and Neuromuscular Therapy within the session?  Musculexonero Technique or Muscle Exonero Technique, Medical Massage, Orthopedic Massage, and stretching.
    </p>
    <p id="marker-desc-2" class="modality-desc" style="display: none;">
        Your standard spa modalities, which incorporate Swedish, Deep Tissue, and Prenatal Massage as well as bodyworks, including body wraps.
    </p>
    <p id="marker-desc-3" class="modality-desc" style="display: none;">
        Modalities that manipulate the fascial system in order to eliminate restrictions, and structural imbalance.
    </p>
    <p id="marker-desc-4" class="modality-desc" style="display: none;">
        Modalities that treat the trigger points, aka “knots“?, in the muscles, which have a tendency to pull on the skeletal body and cause pain and discomfort.
    </p>
    <p id="marker-desc-5" class="modality-desc" style="display: none;">
        Modalities that encourage natural drainage of lymphatic fluid within body tissue. This helps with swelling and inflammation.
    </p>
    <p id="marker-desc-6" class="modality-desc" style="display: none;">
        Bodywork used to balance an individual’s flow of energy by combining it with universal energy to open pathways of healing. Recommended for patients undergoing chemotherapy.
    </p>
    <p id="marker-desc-7" class="modality-desc" style="display: none;">
        Various forms of popular and effective Eastern massage modalities. Includes Ayurvedic, Shiatsu, Qi Gong, Thai Massage, and Acupressure
    </p>
    <div class="row" style="margin-top: 20px;">
        <div class="large-3 columns">Primary care physician?</div>
        <div class="large-9 columns">
            <input name="q9" type="text" value="<?php echo set_value('q9'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Chiropractor?</div>
        <div class="large-9 columns">
            <input name="q10" type="text" value="<?php echo set_value('q10'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Acupuncturist?</div>
        <div class="large-9 columns">
            <input name="q11" type="text" value="<?php echo set_value('q11'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Physical therapist?</div>
        <div class="large-9 columns">
            <input name="q12" type="text" value="<?php echo set_value('q12'); ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">Nutritionist?</div>
        <div class="large-9 columns">
            <input name="q13" type="text" value="<?php echo set_value('q13'); ?>"/>
        </div>
    </div>
    <div class="row" style="margin-top: 45px;">
        <div class="large-12 columns">
            <button type="submit">Save and Continue</button>
        </div>
    </div>
    <?php echo form_close(); ?>    

    <div id="pain-description" style="display: none;">
        <table>
            <tr>
                <td width="20%">Modality:</td>
                <td width="80%">
                    <select id="modality-selection" name="modality-selection">
                        <option value="0"> Medical Massage / Remedial Massage</option>
                        <option value="1"> Spa / Swedish / Deep Tissue</option>
                        <option value="2"> Fascia Manipulation / Myofascial Release</option>
                        <option value="3"> Trigger Point Therapy / Neuromuscular Therapy</option>
                        <option value="4"> Lymphatic Massage / Lymphatic Drainage</option>
                        <option value="5"> Energy Work / Reiki</option>
                        <option value="6"> Eastern Medicine massage</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="modality-date" name="modality-date" type="text"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button id="save" class="modality-selection-button">Save</button>&nbsp;&nbsp;
                    <button id="discard" class="modality-selection-button">Discard</button>&nbsp;&nbsp;
                    <button id="delete" class="modality-selection-button" type="submit">Delete</button>
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="myModal" class="reveal-modal small" data-reveal>
        <p style="color: red;"> By clicking the skip button you agree that all your information below is correct and still current, and no editing is necessary. </p>
        <a href="#" class="close-reveal-modal">&#215;</a>
        <a href="#" class="button right" style="padding: 8px; padding-top: 0;"><span style="font-size: 14px;">Ok, Skip</span></a>
        </div>
    </div>

<script type="text/javascript">

    var locations = [];
    var lcount = 0;

    var isDisplayed = false;
    var humanFrontLocs = [[111, 0], [158, 0], [168, 50], [158, 92], [239, 140], [263, 334], [223, 342], [233, 290], [197, 204], [204, 366],
        [206, 595], [159, 591], [134, 361], [101, 594], [59, 591], [59, 361], [73, 205], [38, 334], [0, 338], [34, 131],
        [109, 93], [95, 40], [111, 0]];
    var humanBackLocs = [[408, 0], [442, 26], [437, 79], [511, 119], [550, 345], [512, 346], [481, 199], [458, 235], [476, 378], [475, 592],
        [436, 597], [407, 366], [380, 599], [335, 596], [338, 354], [367, 223], [340, 196], [304, 347], [270, 344], [279, 219], [312, 111],
        [385, 76], [375, 21], [408, 0]];

    var markedIcons = [false, false, false, false, false, false, false];
    var selectedMarker = null;

    new Pikaday({
        field: document.getElementById('birth_date'),
        firstDay: 1,
        format: 'L',
        setDefaultDate: true,
        minDate: new Date('1920-01-01'),
        maxDate: new Date('2050-12-31'),
        yearRange: [1910, 2020]
    });

    $(document).ready(function () {

        $('[id^="marker-icon-"]').hover(function () {
            if (!isDisplayed) {
                var id = $(this).attr("id");
                var n = id.substring(id.length - 1);
                $('#marker-desc-' + n).css("top", (parseInt($(this).offset().top) - $('#header').height() + 23) + "px");
                $('#marker-desc-' + n).css("left", (parseInt($(this).offset().left)
                        - $('.row:has(#marker-icon-' + n + '):last').offset().left) + "px");
                $('#marker-desc-' + n).show();
                isDisplayed = true;
            }
        }, function () {
            var id = $(this).attr("id");
            var n = id.substring(id.length - 1);
            $('#marker-desc-' + n).hide();
            isDisplayed = false;
        });

        $('[id^="marker-icon-"]').click(function () {
            var id = $(this).attr("id");
            var n = parseInt(id.substring(id.length - 1)) - 1;
            if (!markedIcons[n]) {
                $(this).css("border", "1px dotted red");
                markedIcons[n] = true;
                if (n === 0) {
                    $('#q8').val(1);
                } else if (n === 1) {
                    $('#q8a').val(1);
                } else if (n === 2) {
                    $('#q8b').val(1);
                } else if (n === 3) {
                    $('#q8c').val(1);
                } else if (n === 4) {
                    $('#q8d').val(1);
                } else if (n === 5) {
                    $('#q8e').val(1);
                } else if (n === 6) {
                    $('#q8f').val(1);
                }
            } else {
                $(this).css("border", "none");
                markedIcons[n] = false;
                if (n === 0) {
                    $('#q8').val(0);
                } else if (n === 1) {
                    $('#q8a').val(0);
                } else if (n === 2) {
                    $('#q8b').val(0);
                } else if (n === 3) {
                    $('#q8c').val(0);
                } else if (n === 4) {
                    $('#q8d').val(0);
                } else if (n === 5) {
                    $('#q8e').val(0);
                } else if (n === 6) {
                    $('#q8f').val(0);
                }
            }
        });

        var isMarkerHit = false;

        $('#human-body-map').click(function (e) {
            if (isMarkerHit) {
                isMarkerHit = false;
                return;
            }
            var t = e.pageY - 20;
            var l = e.pageX - 5;
            lcount += 1;
            var id = "pain-marker-" + lcount;
            var dateId = "pain-date-" + lcount;
            selectedMarker = lcount - 1;
            var cornery = parseInt($('#human-body-map').offset().top);
            var cornerx = parseInt($('#human-body-map').offset().left);
            if (pointInPolygon(humanFrontLocs, e.pageX, e.pageY, cornerx, cornery)
                    || pointInPolygon(humanBackLocs, e.pageX, e.pageY, cornerx, cornery)) {
                var currentDate = new Date();
                var s = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear().toString();
                $('#human-body-map').append('<div id="' + id + '" class="pain-marker modality-marker-red"></div>');
                $('#human-body-map').append('<div id="' + dateId + '" class="pain-marker-date"></div>');


                $('#' + id).css("top", (t - cornery) + "px");
                $('#' + id).css("left", (l - cornerx) + "px");
                $('#' + dateId).css("top", (t - cornery + 10) + "px");
                $('#' + dateId).css("left", (l - cornerx + 45) + "px");

                locations.push({indexno: selectedMarker,
                    x: (l - cornerx), y: (t - cornery),
                    modalitytype: 0, date: s});

                var tt = e.pageY - $('#header').height();
                var ll = e.pageX - $('.row:has(#human-body-map):last').offset().left - 18;
                $('#pain-description').css("top", tt + "px");
                $('#pain-description').css("left", ll + "px");
                $('#modality-selection').val(0);
                $('#modality-date').val(s);
                $('#pain-description').show();
                $('#pain-date-' + (selectedMarker + 1)).html($('#modality-date').val());
                $('#pain-date-' + (selectedMarker + 1)).show();

                $('.pain-marker').unbind("click");
                $('.pain-marker').click(function () {
                    var id = $(this).attr("id");
                    var n = parseInt(id.substring(id.lastIndexOf("-") + 1)) - 1;
                    selectedMarker = n;
                    isMarkerHit = true;
                    var m = getLocationsIndex(n);
                    var tt = $(this).offset().top + 20 - $('#header').height();
                    var ll = $(this).offset().left - $('.row:has(#human-body-map):last').offset().left;
                    $('#pain-description').css("top", tt + "px");
                    $('#pain-description').css("left", ll + "px");
                    $('#modality-selection').val(locations[m].modalitytype);
                    $('#modality-date').val(locations[m].date);
                    $('#pain-description').show();
                });
            }
        });

        $(window).resize(function () {
            $('#pain-description').hide();
        });

        $('#pain-description #save').click(function () {
            var m = getLocationsIndex(selectedMarker);
            locations[m].modalitytype = $('#modality-selection').val();
            locations[m].date = $('#modality-date').val();
            $('#pain-date-' + (selectedMarker + 1)).html($('#modality-date').val());

            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-red");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-green");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-orange");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-blue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-yellow");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-darkblue");
            $('#pain-marker-' + (selectedMarker + 1)).removeClass("modality-marker-violet");

            var p = parseInt($('#modality-selection').val());
            if (p === 0) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-red");
            } else if (p === 1) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-green");
            } else if (p === 2) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-orange");
            } else if (p === 3) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-blue");
            } else if (p === 4) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-yellow");
            } else if (p === 5) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-darkblue");
            } else if (p === 6) {
                $('#pain-marker-' + (selectedMarker + 1)).addClass("modality-marker-violet");
            }
            $('#pain-description').hide();
        });

        $('#pain-description #discard').click(function () {
            $('#pain-description').hide();
        });

        $('#pain-description #delete').click(function () {
            $('#pain-marker-' + (selectedMarker + 1)).remove();
            $('#pain-date-' + (selectedMarker + 1)).remove();
            locations.splice(selectedMarker, 1);
            $('#pain-description').hide();
        });

        $('#intake-section form').submit(function (e) {
            e.preventDefault();
            $('<input />').attr("type", "hidden").attr("name", "painmarker")
                    .attr("value", JSON.stringify(locations)).appendTo(this);
            $('<input />').attr("type", "hidden").attr("name", "schedulePost")
                    .attr("value", 'intakeform').appendTo(this);
            /*$(this).submit();*/
                        if($('#fieldName').val()==''){
                            console.log('Name is required.');
                            scrollToId('#fieldName');
                            alert('Name is required.');
                        }else if($('#fieldAddress').val()==''){
                            console.log('Address is required.');
                            scrollToId('#fieldAddress');
                            alert('Address is required.');
                        }else if($('#fieldCity').val()==''){
                            console.log('City is required.');
                            scrollToId('#fieldCity');
                            alert('City is required.');
                        }else if($('#fieldState').val()==''){
                            console.log('State is required.');
                            scrollToId('#fieldState');
                            alert('State is required.');
                        }else if($('#birth_date').val()==''){
                            console.log('Date of Birth is required.');
                            scrollToId('#birth_date');
                            alert('Date of Birth is required.');
                        }else if($('#address_zip_val').val()==''){
                            console.log('Zip code is required.');
                            scrollToId('#address_zip_val');
                            alert('Zip code is required.');
                        }else if($('#contact_home_val').val()==''){
                            console.log('Home number is required.');
                            scrollToId('#contact_home_val');
                            alert('Home number is required.');
                        }else if($('#email_val').val()==''){
                            console.log('Email is required.');
                            scrollToId('#email_val');
                            alert('Email is required.');
                        }else if($('#cell_number_val').val()==''){
                            console.log('Cellphone number is required.');
                            scrollToId('#cell_number_val');
                            alert('Cellphone number is required.');
                        }else{
                            var dataString = $("#intake-section form").serialize();
                            $.ajax({
                                    type: "POST",
                                    // url: "<?php echo site_url() . "intakeform/add"; ?>",
                                    url: "<?php echo site_url() . "schedule/save"; ?>",
                                    data: dataString,
                                    cache: false,
                                    success: function (result) {
                                            if (result == "true") {
                                                    $('body').append(
                                                                    $('<form>')
                                                                    .attr('id', 'redirectForm')
                                                                    .attr('action', '<?php echo site_url() . "booking/medical_history"; ?>').attr('method', 'post')
                                                                    .append($('<input>').attr('name', 'name').attr('value', $('input[name="name"]').val()))
                                                                    .append($('<input>').attr('name', 'address_street').attr('value', $('input[name="address_street"]').val()))
                                                                    .append($('<input>').attr('name', 'address_city').attr('value', $('input[name="address_city"]').val()))
                                                                    .append($('<input>').attr('name', 'address_state').attr('value', $('input[name="address_state"]').val()))
                                                                    .append($('<input>').attr('name', 'address_zip').attr('value', $('input[name="address_zip"]').val()))
                                                                    );
                                                    $('#redirectForm').submit();
                                            }
                                    }
                            });
                        }
        });
            function scrollToId(target){
                $('html, body').animate({
                    scrollTop: $(target).offset().top
                }, 2000);
            }
    });

    function getLocationsIndex(indexNo) {
        var retValue = null;
        for (var i = 0; i < locations.length; i++) {
            if (locations[i].indexno === indexNo) {
                retValue = i;
                break;
            }
        }
        return retValue;
    }

</script>