<?php $this->load->view('includes/header'); ?>


<?php

if (isset($page_var)) {
	$data = $page_var;
} else {
	$data = NULL;
}
$this->load->view($main_content, $data); ?>

<?php $this->load->view('includes/footer'); ?>