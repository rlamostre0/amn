<div id="footer">
    <hr>
    <div class="inner">
        <div class="container">
            <p class="right"><a href="#">Back to top</a></p>
            <p>
            </p>
        </div>
    </div>
</div>


<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/admin.min.js"></script>
<script src="/assets/js/moment.min.js"></script>    
<script src="/assets/js/fullcalendar.min.js"></script>   
<script src="/assets/js/pikaday.js"></script>          
<script src="/assets/js/aciTree/js/jquery.aciPlugin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/js/aciTree/js/jquery.aciTree.min.js"></script>	

<?php
if(isset($scripts)){
	foreach($scripts as $script){
		echo '
			<script src="/assets/js/'.$script.'.js"></script>
		';
	}
}
?>

</body>
</html>