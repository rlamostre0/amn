<!DOCTYPE html> 
<html lang="en-US">
    <head>
        <title>AMN Adminstration Panel</title>
        <meta charset="utf-8">
        

        <script src="/assets/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="/assets/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript" charset="utf-8">

        $(document).ready( function () {
          $('#dtable').dataTable( {
            "bSort": false
          } );
        } );
    </script>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
                  "bSearchable" : true
                }
            );

            
        </script>
        <link href="/assets/css/admin/global.css"  rel="stylesheet" type="text/css">
				<link rel="stylesheet" href="/assets/css/fullcalendar.min.css" />				
				<link rel="stylesheet" type="text/css" href="/assets/js/aciTree/css/aciTree.css" media="all">
                <link rel="stylesheet" type="text/css" href="/assets/datatables/dataTables.bootstrap.css">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container"> 
                    <a class="brand" style="color:#fff">Advance Massage Network - Admin Panel</a>
                    <ul class="nav">
                        <li><?php echo anchor('/admin/clinics', 'Clinics');?></li>
                        <li><?php echo anchor('/admin/scheduler', 'Scheduler');?></li>
                        <li class="dropdown">
                          <a class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                            Blog
                            <b class="caret"></b>
                          </a>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li><?php echo anchor('/admin/blog', 'Blog');?></li>
                            <li><?php echo anchor('/admin/categories', 'Categories');?></li>
                          </ul>
                        </li>
                        <li><?php echo anchor('/admin/testimonials', 'Testimonials');?></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">System <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><?php echo anchor('/admin/logout', 'Logout');?></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>	
