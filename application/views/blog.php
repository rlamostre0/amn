<link rel="stylesheet" href="/assets/css/blog.css" />
<div class="row">
    <div class="large-12 columns">
        <h4>Blogs</h4>
    </div>
</div>
<div class="row">
    <div class="large-9 columns">
      <?php foreach ($recordlist as $record): ?>
          <div class="row" style="padding: 20px;">
              <div class="large-12 columns">
                  <div class="row">
                      <div class="large-12 small-12 columns">
                          <blockquote style="border: none;"> 
                              <h4><?php echo $record["title"]; ?></h4>
                              <p><?php echo $record["content"]; ?></p>
                          </blockquote>
                      </div>
                  </div>
              </div>
          </div>
      <?php endforeach;?>
      <div class="row" style="padding: 20px;">
            <?php echo $this->pagination->create_links();?>
      </div>
    </div>
    <div class="large-3 columns">
        <div class="row" style="padding: 20px;">
            <div class="large-12 columns">
                <h4>FOLLOW ME</h4>
                <ul>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Google+</a></li>
                </ul>
            </div>
        </div>
        <div class="row" style="padding: 20px;">
            <div class="large-12 columns">
                <h4>CATEGORIES</h4>
                <ul>
                    <?php foreach ($categories as $category):?>
                        <li><?php echo anchor("/categories/{$category['slug']}", $category['name'])?></li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</div>
