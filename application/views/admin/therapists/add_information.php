
<div class="container top">
    
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo site_url("admin"); ?>">
                <?php echo ucfirst($this->uri->segment(1)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
        <li>
            <a href="<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>">
                <?php echo ucfirst($this->uri->segment(2)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
        <li class="active">
            <a href="#">Update</a>
        </li>
    </ul>

    <div class="page-header">
        <h2>
            Updating New Therapist Information
        </h2>
    </div>

    <div class="span12">
        <div class="row-fluid">
            <form method="post" action="/admin/clinics/add_therapist_info" class="form-horizontal" style="margin-left: 0em">
                <div class="span6" style="margin-left: 0em">
                    <div class="control-group">
                        <label class="control-label" >First Name</label>
                        <div class="controls">
                            <input type="text"  name="firstname" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Middle Name</label>
                        <div class="controls">
                            <input type="text"  name="middlename" placeholder="Middle Name" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Last Name</label>
                        <div class="controls">
                            <input type="text"  name="lastname" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Suffix</label>
                        <div class="controls">
                            <input type="text"  name="suffix" placeholder="Suffix" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >License Number</label>
                        <div class="controls">
                            <input type="text"  name="license_number" placeholder="License Number" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Birthdate</label>
                        <div class="controls">
                            <input type="number"  name="birthdate_day" style="width: 15%" min="1" max="31" placeholder="dd" required> /
                            <input type="number"  name="birthdate_month" style="width: 15%" min="1" max="12" placeholder="mm" required> /
                            <input type="number"  name="birthdate_year" style="width: 25%" min="1950" max="2014" placeholder="yyyy" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Gender</label>
                        <div class="controls">
                            <input type="text"  name="gender" placeholder="Gender">
                        </div>
                    </div>
                </div>

                <div class="span6" style="margin-left: 0em;">
                    <div class="control-group">
                        <label class="control-label" >Home Address</label>
                        <div class="controls">
                            <textarea class="form-control" style="height: 17em;" name="address" placeholder="Home Address" required></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" >Contact Number</label>
                        <div class="controls">
                            <input type="text"  name="contact_number" placeholder="Contact Number" required>
                        </div>
                    </div>

                    <div class="control-group"  style="margin-left: 1em;">
                        <hr>
                        <div class="controls">
                            <button type="submit" name="submit" value="submit" class="btn btn-primary">Save Information</button>
                        </div>
                    </div>
                    <input type="hidden" name="clinic_id" value="<?php echo $clinic_id; ?>">
                    <input type="hidden" name="therapist_info_id" value="<?php echo $therapist_info_id; ?>">
                </div>
            </form>
        </div>
    </div>

</div>