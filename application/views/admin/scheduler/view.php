<?php
	// $clinic=array(
		// "id"=>4,
		// "name"=>"Kneading Bodyworks"
	// );
	$customer=array(
		"id"=>$this->session->userdata('currentcustomerid'),
		"fname"=>$this->session->userdata('currentcustomerfirstname'),
		"lname"=>$this->session->userdata('currentcustomerlastname')
	);
	// $therapist=array(
		// "id"=>4,
		// "fname"=>"Amy",
		// "lname"=>"Whittle"
	// );
	$userid=$this->session->userdata('currentcustomerid');
	$isLoggedIn=($userid==false?$userid:true);
?>
<div class="row">
	<div class="large-12 columns">
		<div class="panel" style="height:120px;"> 
			<div class="large-6 columns">
				<h3 ><?php echo $clinic["name"]; ?></h3> <h6 >Customer Schedule</h6>
			</div>
			<div class="large-3 columns">
				&nbsp;
			</div>
			<div class="large-3 columns">
				<input type="text" placeholder="search" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="large-4 columns">
		<dl data-accordion="" class="accordion">
			<dd class="accordion-navigation" >
				<a href="#panel1b">Calendar</a>
				<div class="content active" id="panel1b" style="border:1px solid #efefef">
					<div id='small-calendar'></div>
				</div>
			</dd>
		</dl>
		<br/>
		<dl data-accordion="" class="accordion">
			<dd class="accordion-navigation">
				<a href="#panel2b">Therapists</a>
				<div class="content active" id="panel2b" style="border:1px solid #efefef">
					<div id="tree" class="aciTree">
							<!-- the tree structure goes here -->
					</div>
					<br/>
				</div>
			</dd>
		</dl>
	</div>
	<div class="large-8 columns">
		<script>
			function formatDate(dateObject) {
				var d = new Date(dateObject);
				var day = d.getDate();
				var month = d.getMonth() + 1;
				var year = d.getFullYear();
				if (day < 10) {
						day = "0" + day;
				}
				if (month < 10) {
						month = "0" + month;
				}
				var date = year + "/" + month + "/" + day;
				
				return date;
			};
			$(document).ready(function() {
				var /*-- global vars --*/
				reservationPrompt=false,
				reservationStart=0,
				reservationEnd=0,
				therapist=[],
				therapists=jQuery.parseJSON('<?php echo json_encode($therapists); ?>'),
				clinic=jQuery.parseJSON('<?php echo json_encode($clinic); ?>'),
				customer=jQuery.parseJSON('<?php echo json_encode($customer); ?>'),
				isLoggedIn=jQuery.parseJSON('<?php echo json_encode($isLoggedIn); ?>'),
				therapistid=0;
				function setTherapistId(id){
					therapistid=id;
				}
				$('#tree').aciTree({
					ajax:{
						url: '/schedule/get_therapists',
						type: "POST",
						data:{franchiseNo:clinic['franchiseno']}
					},
					fullRow: true,
					// define the columns
					columnData: [
						{
							props: 'therapists'
						}
					],
					radio: true,
					unique: true
				});
				$('#tree').on('acitree', function(event, api, item, eventName, options) {
					// tell what tree it is
					var index = ($(this).attr('id') == 'tree') ? 0 : 1;
					switch (eventName) {
						case 'init':
							// at init open the first folder
							api.open(api.first(), {
									success: function(item) {
										if (index == 0) {
											// select first file for #tree1
											//this.select(this.first(item));
											this.check(this.first(item));
											therapist['id']=api.getId(this.first(item));
											$('#therapist_id').val(therapist['id']);
											$.each( therapists, function( key, value ) {
												if(value['therapist_id']==api.getId(item)){
													therapist['firstname']=value['firstname'];
													therapist['lastname']=value['lastname'];
												}
											});
											$( "#myModal" ).removeClass( "small medium large" );
											$( "#myModal" ).addClass("tiny");
											$('.modalTitle').text("");
											$('.modalBody').text('You have assigned '+therapist['firstname']+' '+therapist['lastname']+' as your therapist');
											$('#myModal').foundation('reveal', 'open');
											calendar.fullCalendar( 'refetchEvents' );
											smallCalendar.fullCalendar( 'refetchEvents' );
										} else {
											// select second file for #tree2
											//this.select(this.next(this.first(item)));
										}
									}
							});
							// set focus so we can use the keyboard already
							$('#tree1').focus();
							break;
						case 'checked':
							therapist['id']=api.getId(item); 
							$('#therapist_id').val(therapist['id']);
							$.each( therapists, function( key, value ) {
								if(value['therapist_id']==api.getId(item)){
									therapist['firstname']=value['firstname'];
									therapist['lastname']=value['lastname'];
								}
							});
							$( "#myModal" ).removeClass( "small medium large" );
							$( "#myModal" ).addClass("tiny");
							$('.modalTitle').text("");
							$('.modalBody').text('You have assigned '+therapist['firstname']+' '+therapist['lastname']+' as your therapist');
							$('#myModal').foundation('reveal', 'open');
							calendar.fullCalendar( 'refetchEvents' );
							smallCalendar.fullCalendar( 'refetchEvents' );
							break;
					}
				});
				//-- remove later

				//-- end remove later
				
				var calendar=$('#calendar').fullCalendar({
					header: {
						left: 'prev, today',
						center: 'title',
						right: 'next'
					},
					selectable: true,
					selectHelper: true,
					select: function(start, end, allDay) {
						cutomerString=(isLoggedIn?customer['fname']+' '+customer['lname']:"[Requires Login]");
						reservationStart=start;
						reservationEnd=end;
						scheduleString=reservationStart.format('hh:mma')+' to '+reservationEnd.format('hh:mma')+' of '+reservationStart.format('MMM D, YYYY');
						if (therapist['firstname'] !== undefined && therapist['lastname']!== undefined) {
							var title="Reservation Details";
							var body='<p>Therapist: <b>'+therapist['firstname']+' '+therapist['lastname']+'</b><br>Schedule:  <b>'+scheduleString+'</b><br/>Reserved by:  <b>'+cutomerString+'</b></p>';
							body+='<center><p style="font-size: .8em;">*Reservations made still need to be approved by therapist<br>*You can select therapists from panel</p></center>';
							body+='<center>Do you want to proceed with your reservation with these details?<br/><br/>';
							body+='<a class="button radius proceedReservation" href="#">Yes</a> <a class="button alert radius closeModal" href="#">No</a></center>';
							resetModalSize("tiny");
							$('.modalTitle').html(title);
							$(".modalBody").html(body);
							$('#myModal').foundation('reveal', 'open');
						}else{
							var title="Alert";
							var body='<p>You have not selected a therapist for your reservation or there are no therapists available at the moment</p>';
							resetModalSize("tiny");
							$('.modalTitle').html(title);
							$(".modalBody").html(body);
							$('#myModal').foundation('reveal', 'open');
						}

					},
					editable: false,
					defaultView: 'agendaWeek',
					/*hiddenDays:[0,1,2,4,5,6],*/
					height: 800,
					slotDuration: '00:30:00',
					snapDuration:"01:00",
					contentHeight: 750,
					minTime: "06:00:00",
					maxTime: "21:00:00",
					eventLimit: true, // allow "more" link when too many events
					allDaySlot:false,
					/*// events: <?php //echo $get_schedules; ?>*/
					events: {
						url: '<?php echo site_url().'schedule/get_schedule'; ?>',
						type:'POST',
						data: function() { // a function that returns an object
							return {
								franchiseNo: clinic['franchiseno'],
								therapist_id:therapist['id'],
							};
						}
					}
				});
				
				var smallCalendar=$('#small-calendar').fullCalendar({
					header: {
						left: 'title',
						center: '',
						right: 'prev,next'
					},
					selectable: true,
					selectHelper: true,
					select: function(start, end, allDay) {
						calendar.fullCalendar( 'gotoDate', start );
					},
					aspectRatio: 1,
					height: "auto",
					defaultView: 'month',
					editable: false,
					eventLimit: true, // allow "more" link when too many events
					allDaySlot:false,
					events: {
						url: '<?php echo site_url().'schedule/get_schedule'; ?>',
						type:'POST',
						data: function() { // a function that returns an object
							return {
								franchiseNo: clinic['franchiseno'],
								therapist_id:therapist['id'],
							};
						}
					}
				});
				$("body").on( "click",".block-time", function(e) {
					e.preventDefault();
					$( "#block-modal" ).removeClass( "small medium large" );
					$( "#block-modal" ).addClass("small");
					var title="";
					$('.block-modalTitle').html(title);
					$('#block-modal').foundation('reveal', 'open');
				});
				$("body").on( "click",".set-schedule", function(e) {
					e.preventDefault();
				});
				$("body").on( "click",".day-1", function(e) {
					e.preventDefault();
					calendar.fullCalendar('option','hiddenDays',[0]);
				});
				$("body").on( "click",".day-3", function(e) {
					e.preventDefault();
					$('#calendar').fullCalendar({hiddenDays:[0,1,2]});
				});
				$("body").on( "click",".day-7", function(e) {
					e.preventDefault();
					$('#calendar').fullCalendar({hiddenDays:[0,1,2,3,4,5,6]});
				});
				$("body").on( "click",".day-14", function(e) {
					e.preventDefault();
					$('#calendar').fullCalendar({hiddenDays:[0]});
				});
				$("body").on( "click",".proceedReservation", function(e) {
					e.preventDefault();
					eventTitle=(isLoggedIn?customer['lname']+", "+customer['fname']:"reserved");
					calendar.fullCalendar('renderEvent',
						{
							title: eventTitle,
							start: reservationStart,
							end: reservationEnd,
						},
						true // make the event "stick"
					);
					calendar.fullCalendar('unselect');
					$('#myModal').foundation('reveal', 'close');
					var scheduleData={
						start: reservationStart.format('YYYY-MM-DD HH:mm:ss'), 
						end: reservationEnd.format('YYYY-MM-DD HH:mm:ss'),
						assigned_therapist: therapist['id'],
						clinic_id:clinic["id"]
					};
					scheduleData=JSON.stringify(scheduleData);
					$('body').append($('<form id="submitScheduleData" name="submitScheduleData" action="<?php echo site_url()."schedule/save"; ?>" method="post">')
						.append('<input type="hidden" name="scheduleData" id="scheduleData">')
						.append('<input type="hidden" name="schedulePost" id="schedulePost">')
						.append('<input type="hidden" name="franchisno" id="franchisno">')
					);
					$('#scheduleData').val(scheduleData);
					$('#schedulePost').val("schedule");
					$('#franchisno').val(clinic['franchiseno']);
					$('#submitScheduleData').submit();
				});
				$("body").on( "click",".closeModal", function(e) {
					e.preventDefault();
					calendar.fullCalendar('unselect');
					$('#myModal').foundation('reveal', 'close');
				});
				function resetModalSize(size){
					$('#myModal').removeClass( "tiny" );
					$('#myModal').removeClass( "small" );
					$('#myModal').removeClass( "medium" );
					$('#myModal').removeClass( "large" );
					$('#myModal').removeClass( "xlarge" );
					$('#myModal').removeClass( "full" );
					$('#myModal').addClass(size);
				}
				var picker1 = new Pikaday(
				{
					field: document.getElementById('start'),
					firstDay: 1,
					minDate: new Date('2000-01-01'),
					maxDate: new Date('2020-12-31'),
					yearRange: [2000,2020]
				});
				var picker2 = new Pikaday(
				{
					field: document.getElementById('end'),
					firstDay: 1,
					minDate: new Date('2000-01-01'),
					maxDate: new Date('2020-12-31'),
					yearRange: [2000,2020]
				});
				$('#clinic_id').val(clinic['id']);
				$('.time-dropdown').html(timepickers());
				function timepickers(){
					var 
						return_var='',
						hours='',
						minutes='',
						meridiem='';
					meridiem+='<option value="AM">am</option>';
					meridiem+='<option value="PM">pm</option>';
					for (i = 1; i <= 12; i++) {
						var digit=(i<=9?'0':'');
						var selected=(i==12?'selected':'');
						hours+='<option value="'+digit+i+'" '+selected+'>'+digit+i+'</option>';
					}
					for (i = 0; i <= 59; i++) {
						var digit=(i<=9?'0':'');
						minutes+='<option value="'+digit+i+'">'+digit+i+'</option>';
					}
					return_var+='start: <select name="startHour" style="width: 80px">';
					return_var+=hours;
					return_var+='</select>';
					return_var+='<select name="startMinutes" style="width: 80px">';
					return_var+=minutes;
					return_var+='</select>';
					return_var+='<select name="startMeridiem" style="width: 80px">';
					return_var+=meridiem;
					return_var+='</select>';
					return_var+='<br/>';
					return_var+='end: <select name="endHour" style="width: 80px">';
					return_var+=hours;
					return_var+='</select>';
					return_var+='<select name="endMinutes" style="width: 80px">';
					return_var+=minutes;
					return_var+='</select>';
					return_var+='<select name="endMeridiem" style="width: 80px">';
					return_var+=meridiem;
					return_var+='</select>';
					return return_var;
				}
				$('body').on( "click", ".blocktype_id", function() {
					if($( this ).val()=='1')
						$('.time-option').show();
					else
						$('.time-option').hide();
				});
				$('body').on( "click", ".submit-block-modal", function() {
					$.ajax({
					type: "POST",
					url: "<?php echo site_url().'schedule/block_event'; ?>",
					data: $('#block-form').serialize()
					})
					.done(function( result ) {
						result=jQuery.parseJSON(result);
						if(result['success']){
							/*calendar.fullCalendar('renderEvent',
								{
									title: result['eventTitle'],
									start: result['start'],
									end: result['end'],
									color:'#5c071a',
									borderColor:'#fa5c1a'
								},
								true // make the event "stick"
							);
							smallCalendar.fullCalendar('renderEvent',
								{
									title: result['eventTitle'],
									start: result['start'],
									end: result['end'],
									color:'#5c071a',
									borderColor:'#fa5c1a'
								},
								true // make the event "stick"
							);*/
							calendar.fullCalendar('refetchEvents');
							smallCalendar.fullCalendar('refetchEvents');
							$('#myModal').foundation('reveal', 'close');
						}else{
							alert(result['msg']);
						}
					});
				});
			});

		</script>
		<style>
			#calendar h2{
				font-size:20pt;
			}
			#small-calendar h2{
				font-size:14pt;
			}
			.accordion dd > a {
				border-top-left-radius:5px;
				border-top-right-radius:5px;
			}
		</style>
		<div class="clearfix" style="line-height: 15px;padding:0px;margin:0px;">
			<div class="left">
				<a class="small blue button radius day-1" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-calendar small"></i> 1 day</a>
				<a class="small blue button radius day-3" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-calendar small"></i> 3 days</a>
				<a class="small blue button radius day-7" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-calendar small"></i> 7 days</a>
				<a class="small blue button radius day-14" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-calendar small"></i> 14 days</a>
			</div>
			<div class="right">
					<?php if($showBlockBtn=true){ ?>
						<a class="small blue button radius block-time" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-lock medium"></i> Block</a>
					<?php }if($showScheduleBtn=true){ ?>
						<a class="small blue button radius set-schedule" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-plus medium"></i> Set Schedule</a>
					<?php } ?>
					<a class="small blue button radius" style="padding:5px 10px 5px;margin:0px;" href="#"><i class="fi-print medium"></i> Print</a><br>
					<ul style="display:inline;line-height: 15px;">
						<li style="color:#3a87ad;list-style-type: square;font-size:20px;"><span style="color:#000000">Reserved</span></li>
						<li style="color:#5c071a;list-style-type: square;font-size:20px;"><span style="color:#000000">Blocked</span></li>
					</ul>
			</div>
		</div>
		<div id='calendar'></div>
	</div>
</div>
<div id="myModal" class="reveal-modal medium" data-reveal>
  <h2 class="modalTitle"></h2>
  <div class="modalBody"></div>
  <a class="close-reveal-modal">&#215;</a>
</div>
<div id="block-modal" class="reveal-modal" data-reveal>
  <h2 class="block-modalTitle"></h2>
  <div class="block-modalBody">
		<center>
		<form id="block-form">
		<table class="table table-striped">
		<thead>
			<tr>
				<th colspan="2"><center>Block Schedule</center></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Block Description</td>
				<td> 
					<textarea name="description" rows="2" cols="30" style="resize: none;"></textarea> 
				</td>
			</tr>
			<tr>
				<td>Type</td>
				<td> 
					<input type="radio" name="blocktype_id" class="blocktype_id" value="1" checked> Time Range<br>
					<input type="radio" name="blocktype_id" class="blocktype_id" value="2"> All Day
				</td>
			</tr>
			<tr class="time-option">
				<td>Time</td>
				<td class="time-dropdown">  
				</td>
			</tr>
			<tr>
				<td>Date</td>
				<td> 
					start: <input name="start" type="text" id="start"><br>
					end: <input name="end" type="text" id="end">
				</td>
			</tr>
			<tr>
				<td>Repeat Options</td>
				<td> 
					<input type="radio" name="recurring_id" value="1" checked> Once<br>
					<input type="radio" name="recurring_id" value="2"> Daily<br>
					<input type="radio" name="recurring_id" value="3"> Weekly<br>
					<input type="radio" name="recurring_id" value="4"> Monthly<br>
					<input type="radio" name="recurring_id" value="5"> Yearly<br>
				</td>
			</tr>
		</tbody>
		</table>
		<input type="hidden" name="clinic_id" id="clinic_id">
		<input type="hidden" name="therapist_id" id="therapist_id">
		</form>
		<input type="submit" class="button radius submit-block-modal" tabindex="0" value="Save">
		</center>
	</div>

  <a class="close-reveal-modal">&#215;</a>
</div>