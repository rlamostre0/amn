<?php
//prints the therapist's name
foreach($therapists as $t){ 
    if ($t['id'] == $therapist_id) {
        echo "<h4>Therapist: ".$t['firstname']." ".$t['lastname']."</h4>";
    }
}


echo "<h4>Date: ".$the_date."</h4>";

?>
<hr>

<?php 
$message = $this->session->flashdata('message');
if (!empty($message)) { ?>
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Success!</strong> <?php echo $message; ?>
</div>
<?php } ?>

<?php if (!empty($query)) { ?>
<table id="dtable" class="table table-bordered table-striped dataTable" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th style="width: 20%">Time</th>
            <th style="width: 34%">Client</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody class="tableData">
        <?php
            if (!empty($query)) { 
                foreach ($query as $q) {
        ?>
                <tr>
                    <td>
                        <?php
                            $start = explode(" ", $q['start']);
                            echo substr($start[1], 0, 5), " - ";
                            $end = explode(" ", $q['end']);
                            echo substr($end[1], 0, 5);
                            $the_date = $this->schedule_model->convert_date($query);
                        ?>
                    </td>
                    <td>
                        <?php echo $q['name']; ?>
                    </td>
                    <td>
                        <a href="<?php echo '/admin/view_intake_form/'.$q['intake_form_id']; ?>" class="btn btn-warning">view intake form</a>
                        <a href="#" class="btn btn-info">change schedule</a>
                        <a href="#myModal" role="button" class="btn btn-danger btnClientCancel" data-toggle="modal" data-clientStart="<?php echo $q['start']; ?>" data-clientEnd="<?php echo $q['end']; ?>" data-intakeId="<?php echo $q['intake_form_id'] ?>" data-clientName="<?php echo $q['name']; ?>" data-clientSched="<?php echo $the_date.', '.substr($start[1], 0, 5).' - '.substr($end[1], 0, 5); ?>">cancel</a>
                    </td>
                </tr>
        <?php
                }
            }
        ?>
    </tbody>
</table>
<?php }
else {
?>
<table class="table table-bordered table-striped dataTable" cellspacing="0" width="100%">
<thead>
    <thead>
        <tr>
            <th style="width: 20%">Time</th>
            <th style="width: 34%">Client</th>
            <th>Action</th>
        </tr>
    </thead>
</thead>
<tbody class="tableData">
    <tr>
    <td colspan="3"><p>There are currently no reservations for this date.</p></td>
    </tr>
</tbody>
</table>
<?php } ?>
 
<!-- Modal -->
<form id="deleteSched" method="post" action="/admin/delete_sched/">
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Do you want to cancel this appointment?</h3>
    </div>
    <div class="modal-body">
        <h4>Details</h4>
        <p style="font-size: 12px;">
            Client: <span class="clientName"></span><br>
            Schedule: <span class="clientSched"></span><br>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <a href="#" class="btn btn-danger clientDelete" data-clientId="">Delete</a>
        <input class="client_intakeId" type="hidden" name="id" value="">
        <input class="client_start" type="hidden" name="client_start" value="">
        <input class="client_end" type="hidden" name="client_end" value="">
    </div>
</div>
</form>


<script>
//$('.btnClientCancel').on( "click", function() {
$( "body" ).on( "click", ".btnClientCancel", function() {
    $('.clientSched').text( $(this).attr('data-clientSched'));
    $('.clientName').text( $(this).attr('data-clientName'));
    $('.intakeId').text( $(this).attr('data-intakeId'));
    $('.clientStart').text( $(this).attr('data-clientStart'));
    $('.clientEnd').text( $(this).attr('data-clientEnd'));
    $('.clientDelete').attr('data-clientId', $(this).attr('data-clientId'));
    $('.client_intakeId').val($(this).attr('data-intakeId'));
    $('.client_start').val($(this).attr('data-clientStart'));
    $('.client_end').val($(this).attr('data-clientEnd'));
});

$('.clientDelete').on( "click", function() {
    $('#deleteSched').submit();
});
</script>