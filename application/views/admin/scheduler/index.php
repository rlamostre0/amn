<script type="text/javascript">
    $(window).load(function(){
        $('#myModal').modal('show');
    });
</script>

<!-- Modal -->
<div class="modal hide fade" id="myModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Information</h3>
  </div>
  <div class="modal-body">
    <p>
	<?php
	//prints the therapist's name
	foreach($therapists as $t){ 
	    if ($t['id'] == $therapist_id) {
	        echo "You are viewing the schedule of <strong>".$t['firstname']." ".$t['lastname']."</strong> of <strong>".$clinic[0]['name']."</strong>.";
	    }
	}
	?>
	</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">OK</button>
  </div>
</div>


<div class="container top">
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo site_url("admin"); ?>">
                <?php echo ucfirst($this->uri->segment(1)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
        <li>
            <a href="<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>">
                <?php echo ucfirst($this->uri->segment(2)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
    </ul>

    <div class="page-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?>
        </h2>
    </div>
<div class="row">

</div>
<div class="row">
	<div class="span3">
		<div id='small-calendar'></div>
		<hr>
			<div>
				<h2>Therapists</h2>
				<ul>
					<?php
						foreach($therapists as $t) {
							$link = base_url('admin')."/scheduler/".$this->uri->segment(3)."/".$t['id'];
							echo "<li><a href='".$link."'>".$t['firstname']." ".$t['lastname']."</a></li>";
						} 
					?>
				</ul>
			</div>
	</div>
	<div class="span9">

		<?php
		$data = array(
			'therapist_id' => $therapist_id,
			'query' => $query,
			'the_date' => $the_date
		);
		$this->load->view('admin/scheduler/timetable.php', $data); ?>
		<a class='btn btn-success'>view SOAP</a>
	</div>
</div>
</div>
<?php
	$customer=array(
		"id"=>$this->session->userdata('currentcustomerid'),
		"fname"=>$this->session->userdata('currentcustomerfirstname'),
		"lname"=>$this->session->userdata('currentcustomerlastname')
	);
	$userid=$this->session->userdata('currentcustomerid');
	$isLoggedIn=($userid==false?$userid:true);
?>
<script>
		window.reservationPrompt=false;
		window.reservationStart=0;
		window.reservationEnd=0;
		window.therapists=jQuery.parseJSON('<?php echo json_encode($therapists); ?>');
		window.the_date=jQuery.parseJSON('<?php echo json_encode($the_date); ?>');
		window.therapist_id=jQuery.parseJSON('<?php echo json_encode($therapist_id); ?>');
		window.clinic=jQuery.parseJSON('<?php echo json_encode($clinic); ?>');
		window.customer=jQuery.parseJSON('<?php echo json_encode($customer); ?>');
		window.isLoggedIn=jQuery.parseJSON('<?php echo json_encode($isLoggedIn); ?>');
		window.therapistid=0;

		function setTherapistId(id){
			window.therapistid=id;
		}
</script>
<input type="hidden" name="clinic" value="">