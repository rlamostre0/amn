<div class="container top">
    <ul class="breadcrumb">
        <li>
            <?php echo anchor("admin/{$this->uri->rsegment(0)}", 'Admin');?>
            <span class="divider">/</span>
        </li>
        <li>
            <?php echo anchor('/admin/testimonials', 'Testimonial');?>
            <span class="divider">/</span>
        </li>
        <li class="active">
            New
        </li>
    </ul>
    <div class="page-header">
        <h2>Adding Testimonial</h2>
    </div>
    <?php echo $this->load->view('includes/error_messages');?>
    <?php echo form_open('/admin/testimonials/add', array('id' => '', 'class' => 'form-horizontal'));?>
      <fieldset>
          <div class="control-group ">
              <label for="" class="control-label">Title</label>
              <div class="controls">
                  <input type="text" id="" name="title" class='form-control' value="<?php echo set_value('title'); ?>" >
              </div>
          </div>
          <div class="control-group ">
              <label for="" class="control-label">Content</label>
              <div class="controls">
                  <textarea name="content" class='height200 form-control'><?php echo set_value('content'); ?></textarea>
              </div>
          </div>

          <div class="form-actions">
              <button class="btn btn-primary" type="submit">Save changes</button>
              <button class="btn" type="reset">Cancel</button>
          </div>
      </fieldset>

    <?php echo form_close(); ?>

</div>
