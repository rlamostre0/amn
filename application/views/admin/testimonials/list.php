<div class="container top">
    <ul class="breadcrumb">
        <li><?php echo anchor("admin/{$this->uri->rsegment(0)}", 'Admin');?><span class="divider">/</span></li>
        <li class="active">Testimonials</li>
    </ul>
    <div class="page-header users-header">
        <h2>Testimonials<?php echo anchor('admin/testimonials/add', 'Add A New', array('class' => 'btn btn-success'));?></h2>
    </div>
    <div class="row">
        <div class="span12 columns">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th class="header">#</th>
                        <th class="yellow header headerSortDown">Name</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($testimonials as $row):?>
                  <tr>
                    <td><?php echo $row['id'];?></td>
                    <td><?php echo $row['title'];?></td>
                    <td class="crud-actions">
                      <?php echo anchor("/testimonials/update/{$row['id']}", 'view & edit', array('class' => 'btn btn-info'));?>
                      <?php echo anchor("/testimonials/delete/{$row['id']}", 'delete', array('class' => 'btn btn-danger'));?>
                    </td>
                  </tr>
                  <?php endforeach;?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links();?>
        </div>
    </div>
</div>