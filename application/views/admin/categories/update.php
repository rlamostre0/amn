<div class="container top">
    <ul class="breadcrumb">
        <li>
            <?php echo anchor("admin/{$this->uri->rsegment(0)}", 'Admin');?>
            <span class="divider">/</span>
        </li>
        <li>
            <?php echo anchor('/admin/categories', 'Categories');?>
            <span class="divider">/</span>
        </li>
        <li class="active">
            Edit
        </li>
    </ul>
    <div class="page-header">
        <h2>Edit Category</h2>
    </div>

    <?php echo $this->load->view('includes/error_messages');?>
    <?php echo form_open('/admin/categories/update', array('id' => '', 'class' => 'form-horizontal'));?>
      <fieldset>
          <div class="control-group ">
              <label for="" class="control-label">Name</label>
              <div class="controls">
                  <input type="text" id="" name="name" class='form-control' value="<?php echo set_value('name'); ?>" >
              </div>
          </div>
          <div class="control-group ">
              <label for="" class="control-label">Slug</label>
              <div class="controls">
                  <input type="text" id="" name="slug" class='form-control' value="<?php echo set_value('slug'); ?>" >
              </div>
          </div>
          <div class="form-actions">
              <button class="btn btn-primary" type="submit">Save changes</button>
              <button class="btn" type="reset">Cancel</button>
          </div>
      </fieldset>

    <?php echo form_close(); ?>

</div>
