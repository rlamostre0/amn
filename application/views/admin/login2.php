<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/assets/bootstrap3/css/style.css" rel="stylesheet" type="text/css">
<link href="/assets/bootstrap3/css/bootstrap.css" rel="stylesheet" type="text/css">

</head>
<body>
<div class='wrapper'>
	<div class='container'>
		<div class='col-sm-7 col-sm-offset-2 login'>
				<div class='col-sm-12'>
					<div class='col-sm-6 col-sm-offset-3'>
						<h3>Members Login</h3>
						<p>*Login to confirm your scheduled appointment</p>
					</div>
					<div>
					<?php $attributes = array(
						'class' => 'form-signin',
						'style'=>'width:65%;margin:auto;padding:10%;'
					); ?>
					<?php echo form_open('admin/login/validate_credentials', $attributes); ?>
					<div class='form-cont'>
						<div class='form-group' >
							<input name="user_name" class='form-control form-username' placeholder='Username' />
						</div>
						<div class='form-group'>
							<input name="password" class='form-control form-password' placeholder='Password' type='password' />
						</div>
						<?php
							if (isset($message_error) && $message_error) {
								echo '<div class="alert alert-error">';
								echo '<a class="close" data-dismiss="alert">×</a>';
								echo '<strong>Oh snap!</strong> Change a few things up and try submitting again.';
								echo '</div>';
							}
						?>
						<?php echo form_submit('submit', 'Login', 'class="btn btn-default col-sm-offset-4"'); ?>
						</div>
					<?php echo form_close(); ?>
					<div class='text-center'>
						<div class='Days'></div>
						<h4>Not a member? Sign up for a <b>Membership.</b></h4>
						<a href='#' class='btn btn-default btn-join'>Join Now!</a>
						<br><br>
						<h4>No, I want to pay the still competitive full <b>Non-Membership</b> rate.</h4>
						<a href='#' class='btn btn-default btn-medic'>Book Now!</a>
						<br><br>
						<h4>Family and Friend's Guest Membership</h4>
						<a href='#' class='btn btn-default btn-join'>Join Now!</a>
					</div>
				</div>
		</div>
	</div>
</div>
        <script src="/assets/bootstrap3/js/jquery.js"></script>
        <script src="/assets/bootstrap3/js/bootstrap.js"></script>
</body>
</html>