<script type="text/javascript">
    $( "body" ).on( "click", ".checkTherapist", function(e) {
        
        e.preventDefault();
        link=$(this).attr('href');
        clinic_id=$(this).attr('data-clinicid');
        $.ajax({
            type: "POST",
            url: "/admin/clinics/has_therapist/",
            data: { clinic_id: clinic_id }
        })
        .done(function( result_parse ) {
            result=jQuery.parseJSON(result_parse);
            //echo json_encode($num_of_therapists);
            if(result==0){
                $("#checkTherapist").modal('show');
            }else{
                window.location=link;
            }
        });
    });
</script>

<div class="container top">

    <ul class="breadcrumb">
        <li>
            <a href="<?php echo site_url("admin"); ?>">
                <?php echo ucfirst($this->uri->segment(1)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
        <li class="active">
            <?php echo ucfirst($this->uri->segment(2)); ?>
        </li>
    </ul>

    <div class="page-header users-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?> 
            <a  href="<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>/add" class="btn btn-success">Add a new</a>
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">
            <div class="well">

                <?php
                $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');

                //save the columns names in a array that we will use as filter         
                $options_clinics = array();
                foreach ($clinics as $array) {
                    foreach ($array as $key => $value) {
                        $options_clinics[$key] = ucwords(str_replace("_", " ", $key));
                    }
                    break;
                }

                echo form_open('admin/clinics', $attributes);

                echo form_label('Search:', 'search_string');
                echo form_input('search_string', $search_string_selected);

                echo form_label('Order by:', 'order');
                echo form_dropdown('order', $options_clinics, $order, 'class="span2"');

                $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

                $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
                echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1"');

                echo form_submit($data_submit);

                echo form_close();
                ?>

            </div>

            <table class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th class="header">#</th>
                        <th class="yellow header headerSortDown">Name</th>
                        <th class="yellow header headerSortDown" style="width: 22em;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($clinics as $row) {
                        echo '<tr>';
                        echo '<td>' . $row['id'] . '</td>';
                        echo '<td>' . $row['name'] . '</td>';
                        echo '<td class="crud-actions">
                  <a data-clinicid="'.$row['id'].'" href="' . site_url("admin") . '/scheduler/' . $row['id'] . '" class="btn btn-success checkTherapist">view schedules</a>  
                  <a href="' . site_url("admin") . '/clinics/update/' . $row['id'] . '" class="btn btn-info">view & edit</a>  
                  <a href="' . site_url("admin") . '/clinics/delete/' . $row['id'] . '" class="btn btn-danger">delete</a>
                </td>';
                        echo '</tr>';
                    }
                    ?>      
                </tbody>
            </table>

            <?php echo '<div class="pagination">' . $this->pagination->create_links() . '</div>'; ?>

        </div>
    </div>

<!-- Modal -->
<form id="checkTherapist" method="post" action="/admin/clinics/has_therapist/">
    <div id="checkTherapist" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Information</h3>
        </div>
        <div class="modal-body">
            <h4 style="color: red">
                There are no therapists available for this clinic.
            </h4>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Okay</button>
            <input class="client_intakeId" type="hidden" name="id" value="1">
            <input class="client_start" type="hidden" name="client_start" value="1">
            <input class="client_end" type="hidden" name="client_end" value="1">
        </div>
    </div>
</form>