
<div class="container top">
    
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo site_url("admin"); ?>">
                <?php echo ucfirst($this->uri->segment(1)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
        <li>
            <a href="<?php echo site_url("admin") . '/' . $this->uri->segment(2); ?>">
                <?php echo ucfirst($this->uri->segment(2)); ?>
            </a> 
            <span class="divider">/</span>
        </li>
        <li class="active">
            <a href="#">Update</a>
        </li>
    </ul>

    <div class="page-header">
        <h2>
            Updating <?php echo ucfirst($this->uri->segment(2)); ?>
        </h2>
    </div>

    <div class="span12">
          <div class="row-fluid">
            <div class="span6">
                <h3>Clinic Information</h3><br>
                <?php
                //flash messages
                if ($this->session->flashdata('flash_message')) {
                    if ($this->session->flashdata('flash_message') == 'updated') {
                        echo '<div class="alert alert-success">';
                        echo '<a class="close" data-dismiss="alert">×</a>';
                        echo '<strong>Well done!</strong> manufacturer updated with success.';
                        echo '</div>';
                    } else {
                        echo '<div class="alert alert-error">';
                        echo '<a class="close" data-dismiss="alert">×</a>';
                        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
                        echo '</div>';
                    }
                }
                ?>

                <?php
                //form data
                $attributes = array('class' => 'form-horizontal', 'id' => '');

                //form validation
                echo validation_errors();

                echo form_open('admin/clinics/update/' . $this->uri->segment(4) . '', $attributes);
                ?>
                <fieldset>
                    <div class="control-group">
                        <label for="inputError" class="control-label">Name</label>
                        <div class="controls">
                            <input type="text" id="" name="name" value="<?php echo $clinic[0]['name']; ?>" >
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputError" class="control-label">Street Address</label>
                        <div class="controls">
                            <input type="text" id="" name="address_street" value="<?php echo $clinic[0]['address_street']; ?>" >
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputError" class="control-label">City</label>
                        <div class="controls">
                            <input type="text" id="" name="address_city" value="<?php echo $clinic[0]['address_city']; ?>" >
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputError" class="control-label">State</label>
                        <div class="controls">
                            <input type="text" id="" name="address_state" value="<?php echo $clinic[0]['address_state']; ?>" >
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputError" class="control-label">Zip Code</label>
                        <div class="controls">
                            <input type="text" id="" name="address_zipcode" value="<?php echo $clinic[0]['address_zipcode']; ?>" >
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputError" class="control-label">Schedule</label>
                        <div class="controls">
                            <textarea name="schedule"  ><?php echo $clinic[0]['schedule'] ?></textarea>
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputError" class="control-label">Map Coordinates<br>(Long, Lat)</label>
                        <div class="controls">
                            <input type="text" id="" name="coords_long" style="width: 12%" placeholder="Longitude" value="<?php echo $clinic[0]['coords_long']; ?>">&nbsp;&nbsp;
                            <input type="text" id="" name="coords_lat" style="width: 12%" placeholder="Latitude" value="<?php echo $clinic[0]['coords_lat']; ?>">
                            <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                        <div class="controls" style="margin-top: 0.5em;">
                            <button class="btn" type="button">Generate Coordinates</button>
                        </div>
                    </div>
                    <?php if (!empty($clinic_services)) { ?>
                    <div class="control-group">
                        <label for="inputError" class="control-label">Services</label>
                        <div class="controls">
                            <ul id="services">
                                <?php
                                foreach ($services as $service):
                                    
                                        $checked = in_array($service->id, $clinic_services) ? "checked" : NULL;
                                    ?>
                                    <li><input type="checkbox" name="services[]"  <?php echo $checked ?>  value="<?php echo $service->id ?>" /><?php echo $service->service ?></li>
                                <?php
                                endforeach; ?> 
                            </ul>
                          <!--<span class="help-inline">Woohoo!</span>-->
                        </div>
                    </div>
                    </fieldset>
                          
                    <?php } ?>
                    <div class="control-group">
                        <hr>
                        <div class="controls">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                            <button class="btn" type="reset">Cancel</button>
                        </div>
                    </div>

                    <?php echo form_close(); ?>
            </div><!--/span-->
            <div class="span5">
                <h3>Therapists</h3>
                <?php
                    $message = $this->session->flashdata('message');
                    if (!empty($message)) {
                ?>
                    <div class="alert alert-success span9">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $message; ?>
                    </div>
                <?php       
                    } 
                ?>
                <table class="table table-bordered">
                   
                    <?php
                        if (!empty($therapists)) { ?>
                             <thead>
                                <th>Name</th>
                                <th style="width: 40%">Action</th>
                            </thead>
                    <?php
                            foreach($therapists as $t) {
                                $link = base_url('admin')."/scheduler/".$this->uri->segment(3)."/".$t['id'];
                                echo "<tr>";
                                echo "<td>".$t['firstname']." ".$t['lastname']."</td>";
                                echo '<td><a class="btn btn-info" role="button">Manage</a>&nbsp;<a class="btn btn-danger" role="button">Delete</a></td>';
                                echo "</tr>";
                            } 
                        } else { ?>
                            <div class="alert alert-warning span9">
                                There are currently no therapists assigned in this clinic.
                            </div>
                            </table>
                    <?php
                        }
                    ?>
                </table>
                <p><a class="btn btn-primary pull-right therapist_add_new" role="button" data-clinicId="<?php echo $clinic_id; ?>" data-toggle="modal" style="margin-right: 1.5em;" href="#myModal"><i class="fa fa-plus"></i> Add a Therapist</a></p>
            </div><!--/span-->
          </div><!--/row-->
    </div>

</div>

<!-- Modal -->
<form method="post" action="/admin/clinics/add_therapist" class="therapist_form">
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Add a Therapist</h3>
        </div>
        <div class="modal-body">
                <fieldset>
                    <div class="span6" style="margin-left: 6.5em">
                        <h3 class="text-success">Add Therapist Login Details</h3>
                        <p>This is used by the therapist to sign in to his/her account.</p>

                        <div class="input-prepend">
                            <span class="add-on"><i class="fa fa-user"></i></span>
                            <input type="text" style="width: 70%" name="username" placeholder="Username" required>
                        </div>
                        
                        <div class="input-prepend">
                            <span class="add-on"><i class="fa fa-key"></i></span>
                            <input type="password" style="width: 70%" name="password" placeholder="Password" required>
                        </div>

                        <div class="input-prepend">
                        <span class="add-on"><i class="fa fa-envelope-o"></i></span>
                        <input type="email" style="width: 70%" name="email" placeholder="E-mail Address" required>
                        </div>

                    </div>
                    <input type="hidden" class="the_clinic_id" name="clinic_id" value="">
                    
                </fieldset>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-warning" type="submit" name="submit" value="submit">Save changes</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    $( "body" ).on("click", ".therapist_add_new", function() {
        $('.the_clinic_id').val($(this).attr('data-clinicId'));
    });
</script>