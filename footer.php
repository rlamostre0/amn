
</div>
<div id="footer">
    <div class="row">
        <div class="divider no-border"></div>
    </div>
    <div class="row">
        <div class="boxes">
            <div class="small-3  columns">  
                <div class="box military">
                    <h5 class="center">Military Discount</h5>     
                </div>
            </div>
            <div class="small-3  columns">  
                <div class="box senior">
                    <h5 class="center">Senior Citizens Discout</h5>     
                </div>
            </div>
            <div class="small-3  columns">  
                <div class="box healthcare">
                    <h5 class="center">Healthcare Prac. Discount</h5>     
                </div>
            </div>
            <div class="small-3  columns">  
                <div class="box blog">
                    <h5 class="center">Check on our blog!</h5>     
                </div>
            </div>
        </div>
        <div class="divider no-border"></div>
        <div class="quick-links">
            <div class="large-12 columns">
                <h3 class="italic"> Quick Links</h3>
                <div class="small-2  columns">  
                    <div class="box military">
                        <h5>Military Discount</h5>     
                        <ul>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-2  columns">  
                    <div class="box military">
                        <h5>Military Discount</h5>     
                        <ul>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-2  columns">  
                    <div class="box military">
                        <h5>Military Discount</h5>     
                        <ul>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                            <li><a href="#"> Link </a></li>
                        </ul>     
                    </div>
                </div>
                <div class="small-6  columns">  
                    <div class="box military">
                        <img src="http://placehold.it/490x150" />   
                    </div>
                </div>
            </div>
        </div>
        <div class="divider no-border"></div>
        <div class="large-12 columns grey  search">
            <div class="right"> <input type="text" placeholder="Search" class="rounded" /></div>
        </div>
        <div class="divider no-border"></div>
        <div class="large-12 columns grey  menu center">
            <ul>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
                <li><a href="#"> Link </a></li>
            </ul>
        </div>
        <div class="divider no-border"></div>
        <div class="large-12 columns copyright">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
        </div>
        <div class="divider no-border"></div>
    </div>   
</div>   
<script src="/assets/js/vendor/jquery.js"></script>
<script src="/assets/js/foundation.min.js"></script>
<script src="/assets/js/foundation/foundation.orbit.js"></script>
<script src="/assets/js/vendor/modernizr.js"></script>
<?php if (isset($js) && is_array($js)): ?>
    <?php foreach ($js as $index => $file): ?>
        <script src="/assets/js/<?php echo $file ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>
<script>
    $(document).foundation();
</script>
</body>
</html>
