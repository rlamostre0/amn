<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php if (isset($meta_title)) echo $meta_title . " | "; ?>Advanced Massage Network </title>
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="description" content="<?php echo $meta_description ?>">
        <meta name="robots" content="index,follow">
        <link rel="stylesheet" href="/assets/css/foundation.css" />
        <link rel="stylesheet" href="/assets/css/mystyle.css" />test
        <?php if (isset($css) && is_array($css)): ?>
            <?php foreach ($css as $index => $file): ?>
                <link rel="stylesheet" href="/assets/css/<?php echo $file ?>" />
            <?php endforeach; ?>
        <?php endif; ?>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>      
        <script src="http://www.mapquestapi.com/sdk/js/v7.0.s/mqa.toolkit.js?key=Fmjtd%7Cluur2g6121%2Crn%3Do5-9az2lr"></script>        
    </head>

    <body>
        <div class="row" id="header">

            <div class="large-12 columns" id="top-bar">

                <div class="util-bar left-col logo">

                    <a href="<?php echo site_url() ?>pages/home"><img src="/assets/img/logo2.png" alt="logo" title="Advance Massage Network"></a>   

                </div>

                <div class="util-bar right-col">
                    <div class="utilities">
                        <div class="social-icons">
                            <ul>
                                <li><img src="http://placehold.it/24x24"></li>
                                <li><img src="http://placehold.it/24x24"></li>
                                <li><img src="http://placehold.it/24x24"></li>
                                <li><img src="http://placehold.it/24x24"></li>
                                <li><img src="http://placehold.it/24x24"></li>
                                <li><img src="http://placehold.it/24x24"></li>
                                <li><img src="http://placehold.it/24x24"></li>
                            </ul>
                        </div>
                        <div class="links">
                            <nav data-topbar>
                                <section class="top-bar-section">
                                    <ul>
                                        <li class="has-dropdown"><a href="#">Own a Franchise for free!</a>
                                            <ul class="dropdown">
                                                <li><a href="#">Test</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Membership Sign-in</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="#">Site Map</a></li>
                                    </ul>
                                </section>
                            </nav>
                        </div>
                    </div>
                    <div class="phone">
                        <h3>1-884-ADV-MASS</h3>
                        <span>238777</span>
                    </div>
                </div>

            </div>
            <!--
            <div class="large-3 columns left" id="modal-form">
    
                
                    <div class="form">
                    <h4 class="center">Find a Clinic</h4>
                    <h5 class="center">Atlanta</h5>
                    <div class="divider no-border"></div>
                    <div class="results">
                            <div class="entries">
                            <div class="name">Name</div>
                            <div class="miles">1.0mi</div>
                        </div>
                            <div class="entries">
                            <div class="name">Name</div>
                            <div class="miles">1.0mi</div>
                        </div>
                            <div class="entries">
                            <div class="name">Name</div>
                            <div class="miles">1.0mi</div>
                        </div>
                            <div class="entries">
                            <div class="name">Name</div>
                            <div class="miles">1.0mi</div>
                        </div>
                    </div>
                    <div class="divider no-border"></div>
                    <p class="italic">Search by city,state and zip code</p>
                    <div class="row">
                            <div class="small-7 column">
                            <input type="text" name="search" class="input text">
                        </div>
                        <div class="small-5 column">
                            <input type="button" class="button" value="Search">
                        </div>
                    </div>
                </div>
    
      
            </div>
    
            -->
            <div class="large-12 columns" id="slider">

                <ul class="example-orbit" data-orbit> 

                    <li> 
                        <img src="/assets/img/slide1.png" alt="slide 1" />
                    </li> 
                    <li> 
                        <img src="/assets/img/slide2.png" alt="slide 2" />  
                    </li> 
                    <li> 
                        <img src="/assets/img/slide1.png" alt="slide 1" /> 
                    </li> 
                </ul>

            </div>


            <div class="large-12 columns">
                <nav class="top-bar" data-topbar>

                    <section class="top-bar-section">
                        <ul class="left">
                            <li class="has-dropdown"><a href="<?php echo site_url() ?>pages/home">Welcome <img src="http://placehold.it/24x24"></a>
                                <ul class="dropdown">
                                    <li><a href="#">YOUR FIRST VISIT</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">SIGN UP FOR A MEMBERSHIP</a></li>
                                    <li><a href="#">FAQs</a></li>
                                    <li><a href="#">Blog</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo site_url() ?>pages/pricing">Pricing &amp; Memberships <img src="http://placehold.it/24x24"></a>

                            </li>
                            <li class="has-dropdown"><a href="<?php echo site_url() ?>pages/modalities">Modalities <img src="/assets/img/eye-icon.png"></a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo site_url() ?>pages/intakeform">Intake Form</a></li>
                                    <li><a href="<?php echo site_url() ?>pages/diagnosis">Diagnosis </a></li>                        
                                </ul>
                            </li>
                            <li><a href="<?php echo site_url() ?>pages/locations">Locations <img src="http://placehold.it/24x24"></a>
                                <!--ul class="dropdown">
                                    <li><a href="#">Item 1 </a></li>
                                    <li><a href="#">Item 2</a></li>
                                </ul-->
                            </li>
                            <li><a href="<?php echo site_url() ?>pages/gift">Gift Certificates <img src="http://placehold.it/24x24"></a>

                            </li>
                        </ul>

                        <ul class="right">
                            <li ><a href="<?php echo site_url() ?>pages/book">Book Now</a>

                            </li>
                            <li><a href="<?php echo site_url() ?>pages/join">Join Network</a>

                            </li>
                            <li class="has-dropdown"><a href="<?php echo site_url() ?>pages/contact">Info &amp; Contact</a>
                                <ul class="dropdown">
                                    <li><a href="#">Contact Us </a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                            </li>
                        </ul>
                    </section>
                </nav>

            </div>

        </div>


        <div  id="content">
            <div class="row">
                <div class="divider no-border"></div>
            </div>
        </div>
